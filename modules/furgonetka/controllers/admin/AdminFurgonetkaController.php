<?php
class AdminFurgonetkaController extends ModuleAdminController
{
    public function displayAjaxGetOrderIframe()
    {
        $orderId = Tools::getValue('id_order');
        $furgonetka = new FurgonetkaDelivery(FurgonetkaAuth::getEnvironment());
        try{
            $link = $furgonetka->getPackageFormUrl($orderId);
        }catch(Exception $e){
            die(json_encode(array('status' => 'error', 'error' => $e->getMessage())));
        }
        $this->context->smarty->assign(array('url' => $link));
        $html = $this->context->smarty->fetch(__DIR__.'/../../views/templates/admin/furgonetka-iframe.tpl');

        die(json_encode(array('status' => 'ok', 'html' => $html)));
    }
}
