<?php

/**
 * Cart details gate.
 */
class FurgonetkaCartDetailsGateModuleFrontController extends ModuleFrontController
{
    /**
     * Processes POST request.
     */
    public function postProcess()
    {
        try {
            // Get webservice access key.
            $accessKey = Tools::getValue('accessKey');

            // Check if access key is valid and active.
            if (!WebserviceKey::isKeyActive($accessKey)) {
                $this->generateResponse(401);
                die(0);
            }

            $cartIds = Tools::getValue('cartIds');

            // Check if cartIds param was passed and has valid format.
            if (empty($cartIds) || !\is_array($cartIds)) {
                $this->generateResponse(400);
                die(0);
            }

            $cartIdsFiltered = array();

            // Filter cart ids and remove duplicates.
            foreach ($cartIds as $cartId) {

                $cartIdFiltered = \intval($cartId);

                if (!empty($cartIdFiltered) && !\in_array($cartIdFiltered, $cartIdsFiltered)) {
                    $cartIdsFiltered[] = pSQL($cartIdFiltered);
                }
            }

            // Check if there are any cart ids after filtering.
            if (empty($cartIdsFiltered)) {
                $this->generateResponse(400);
                die(0);
            }

            // Prepare SQL select and get result.
            $select = new DbQuery();
            $select->select('id_cart,machine_code,machine_type');
            $select->from('furgonetka_cart_delivery_machine');
            $select->where('id_cart IN (' . \implode(',', $cartIdsFiltered) . ')');

            $queryResult = Db::getInstance()->executeS($select);

            $responseData = array();

            // Prepare response data.
            foreach ($queryResult as $row) {
                $responseData[$row['id_cart']] = array(
                    'code' => $row['machine_code'],
                    'type' => $row['machine_type'],
                );
            }

            $this->generateResponse(200, $responseData);
            die(0);
        } catch (\Exception $e) {
            $this->generateResponse(500);
        }
    }

    /**
     * Generates response.
     *
     * @param int  $httpCode
     * @param null $data
     */
    private function generateResponse($httpCode, $data = null)
    {
        $httpStatus = 'OK';

        switch ($httpCode) {
            case 400:
                $httpStatus = 'Bad Request';
                break;
            case 401:
                $httpStatus = 'Unauthorized';
                break;
            case 500:
                $httpStatus = 'Internal Server Error';
                break;
        }

        if (!empty($data)) {
            echo \json_encode($data);
        }

        \header('HTTP/1.1 ' . $httpCode . ' ' . $httpStatus, true, $httpCode);
    }
}
