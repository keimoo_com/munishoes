{*
* 2012-2017 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - Facebook login Pro © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek PrestaDev.pl <info@prestadev.pl>
* @copyright 2012-2017 Patryk Marek - PrestaDev.pl
* @license   License is for use in domain / or one multistore enviroment (do not modify or reuse this code or part of it) if you want any changes please contact with me at info@prestadev.pl
* @link      http://prestadev.pl
* @package   Facebook login Pro - PrestaShop 1.5.x and 1.6.x Module
* @version   1.1.0
* @date      10-06-2016
*}
<div class="pdfbloginpro">
	<a onclick="fb_login();" class="sb gradient glossy blue facebook" href="#" title="{l s='Login with Facebook account' mod='pdfbloginpro'}">
		<span>{l s='Login with Facebook' mod='pdfbloginpro'}</span>
	</a>
</div>