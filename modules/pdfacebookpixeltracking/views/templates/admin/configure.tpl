{*
* 2012-2016 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Facebook Pixel Tracking © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek PrestaDev.pl <info@prestadev.pl>
* @copyright 2012-2016 Patryk Marek - PrestaDev.pl
* @link		 http://prestadev.pl
* @package	 PD Facebook Pixel Tracking PrestaShop 1.5.x and 1.6.x Module
* @version	 1.1.0
* @license   License is for use in domain / or one multistore enviroment (do not modify or reuse this code or part of it) if you want any changes please contact with me at info@prestadev.pl
* @date		 24-05-2016
*}

<div class="panel">
    <h2>{l s='How this module works?' mod='pdfacebookpixeltracking'}</h2>
    <p>{l s='To start tracking conversions and start growing your custom audience you just have to follow one of those steps' mod='pdfacebookpixeltracking'}</p>
    <br />
    <h4>{l s='1. On Facebook Ads Manager' mod='pdfacebookpixeltracking'}</h4>
    <p>{l s='1.1 To get it go to' mod='pdfacebookpixeltracking'} <a href="https://www.facebook.com/ads/manager/pixel/facebook_pixel/" title="{l s='Get Facebook Pixel\'s ID' mod='pdfacebookpixeltracking'}"> {l s='Facebook\'s Pixel Manager' mod='pdfacebookpixeltracking'}</a> {l s='and find the pixel id on the right sidebar of the screen, below your account name.' mod='pdfacebookpixeltracking'}</p>
    <p>{l s='1.2 Copy and paste the code on the form below and save' mod='pdfacebookpixeltracking'}</p>
    
    <br />
    <h4>{l s='2. On Facebook Power Editor' mod='pdfacebookpixeltracking'}</h4>
    <p>{l s='2.1 Go to' mod='pdfacebookpixeltracking'} <a href="https://www.facebook.com/ads/manage/powereditor"> {l s='Facebook\'s Power Editor' mod='pdfacebookpixeltracking'}</a></p>
    <p>{l s='2.2 Click on Tools > Pixels.' mod='pdfacebookpixeltracking'}</p>
    <p>{l s='2.3 Find the pixel id on the right sidebar of the screen, below your account name.' mod='pdfacebookpixeltracking'}</p>
    <p>{l s='2.4 Copy and paste the code on the form below and save' mod='pdfacebookpixeltracking'}</p>
</div>

