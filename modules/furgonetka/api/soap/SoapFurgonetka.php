<?php
require_once 'SoapRequest.php';
require_once 'SoapClasess.php';
class SoapFurgonetka extends SoapRequest
{
    const WSDL = 'https://biznes.furgonetka.pl/api/soap/v2?wsdl';
    const WSDL_TEST = 'https://biznes-test.furgonetka.pl/api/soap/v2?wsdl';

    public function __construct($test)
    {
        $this->soap = self::getSoap((bool)$test);
    }

    public function createIntegrationOAuthApplication($url, $data_1, $data_2, $integration_type = 'prestashop')
    {
        $params = new CreateIntegrationOAuthApplicationParams($integration_type, $url, $data_1, $data_2);
        return
            new CreateIntegrationOAuthApplicationResult(
                $this->execSoap('createIntegrationOAuthApplication', array('data' => $params))
            );

    }

    public function getPackageFormUrl(
        FurgonetkaSoapAuth $auth,
        FurgonetkaSoapSender $sender,
        FurgonetkaSoapReceiver $receiver,
        $partner_reference_number,
        $sale_source_id,
        FurgonetkaSoapParcel $parcel,
        FurgonetkaSoapServices $services
    )
    {
        $params = new
        GetPackageFormUrlParams(
            $auth,
            $sender,
            $receiver,
            $partner_reference_number,
            $receiver->type_point,
            $sale_source_id,
            null,
            [$parcel],
            $services
        );
        return  new GetPackageFormUrlResult($this->execSoap('getPackageFormUrl', array('data' => $params)));
    }

    public function addIntegrationSource(FurgonetkaSoapAuth $auth, $data_1, $data_2, $data_3, $module_version = null, $integration_type = 'prestashop')
    {
        if(empty($module_version)){
            $module = Module::getInstanceByName('furgonetka');
            $module_version = $module->version;
        }

        $params = new AddIntegrationSourceParams($auth, $integration_type, $module_version, $data_1, $data_2, $data_3);
        return new AddIntegrationSourceResult($this->execSoap('addIntegrationSource', array('data' => $params)));
    }
    
    public static function getSoap($test)
    {
        $url = $test? self::WSDL_TEST : self::WSDL;
        return new SoapClient($url, array('trace' => 1, 'user_agent' => self::getUserAgent()));
    }

    private static function getUserAgent()
    {
        $module = Module::getInstanceByName('furgonetka');

        return 'prestashop_'._PS_VERSION_.'_plugin_'. $module->version;
    }
}
