<?php
/**
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analytics Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analytics Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*/

require_once(dirname(__FILE__).'/vendor/autoload.php');
require_once(dirname(__FILE__).'/models/PdGAEEPModel.php');

use TheIconic\Tracking\GoogleAnalytics\Analytics;

class PdGoogleAnalytycEnhancedEcommercePro extends Module
{
    private $html = '';
    private $errors = array();

    public $pd_google_tag_menager_id = '';
    public $pd_google_analitycs_id = '';
    public $pd_google_analitycs4_id = '';
    public $pd_google_dynamic_remarketing;

    private $product_ids_type;
    private $send_page_view;
    public $http_referer = '';

    public $os_statuses_send;
    public $os_statuses_refund;

    public static $products_hook_exec;
    private static $order_confirmation_exec = false;

    public function __construct()
    {
        $this->name = 'pdgoogleanalytycenhancedecommercepro';
        $this->tab = 'advertising_marketing';
        $this->version = '1.0.9';
        $this->author = 'PrestaDev.pl';
        $this->bootstrap = true;
        $this->need_instance = 0;
        $this->module_key = '1256d000ea3dfg12d6b2i33cr5ca32e4d';

        parent::__construct();

        $this->displayName = $this->l('PD Google Analytics Enhanced Ecommerce Pro');
        $this->description = $this->l('Module that adds Google Analytics Enhanced Ecommerce code to Your store trought Tag Meanager > DataLayer');

        $this->secure_key = Tools::encrypt($this->name);
        
        $this->pd_google_tag_menager_id = htmlspecialchars_decode(Configuration::get('PD_GAEEP_GOOGLE_TAG_MANAGER'));
        $this->pd_google_analitycs_id = htmlspecialchars_decode(Configuration::get('PD_GAEEP_GOOGLE_ANAL_ID'));
        $this->pd_google_analitycs4_id = htmlspecialchars_decode(Configuration::get('PD_GAEEP_GOOGLE_ANAL4_ID'));
        $this->pd_google_dynamic_remarketing_id = htmlspecialchars_decode(Configuration::get('PD_GA4P_GOOGLE_ADWORDS_CONVERSION'));
        $this->pd_google_dynamic_remarketing = Configuration::get('PD_GAEEP_GOOGLE_DR_ACTIVE');

        $this->product_ids_type = Configuration::get('PD_GAEEP_PRODUCT_ID_TYPE');
        $this->send_page_view = Configuration::get('PD_GAEEP_GOOGLE_ANAL_SPV');

        $this->os_statuses_send = explode(',', Configuration::get('PD_GAEEP_OS_SEND_ORDER'));
        $this->os_statuses_refund = explode(',', Configuration::get('PD_GAEEP_OS_REFUND_ORDER'));

        $this->ps_version_1770_lte = (version_compare(Tools::substr(_PS_VERSION_, 0, 7), '1.7.7.0', '<')) ? true : false;

        if (isset($this->context->cookie->id_connections)) {
            $this->getRefererFromIdConnection($this->context->cookie->id_connections);
        } 

    }

    public function install()
    {
        if (!parent::install()
            
            || !$this->registerHook('actionProductSearchAfter')
            || !$this->registerHook('actionOrderStatusPostUpdate')
            || !$this->registerHook('actionObjectOrderAddAfter')
            || !$this->registerHook('displayOrderConfirmation')
            || !$this->registerHook('displayBackOfficeHeader')
            || !$this->registerHook('displayAdminOrderSide')
            || !$this->registerHook('displayAdminOrder')
            || !$this->registerHook('displayAfterBodyOpeningTag') 
            || !$this->registerHook('displayBeforeBodyClosingTag') 
            || !$this->registerHook('displayHeader') 
            || !$this->registerHook('displayFooter') 
            || !Configuration::updateValue('PD_GAEEP_GOOGLE_ANAL_SPV', 1) 
            || !Configuration::updateValue('PD_GAEEP_PRODUCT_ID_TYPE', 0) 
            || !Configuration::updateValue('PD_GAEEP_GOOGLE_ANAL_ID', '') 
            || !Configuration::updateValue('PD_GAEEP_GOOGLE_ANAL4_ID', '') 
            || !Configuration::updateValue('PD_GA4P_GOOGLE_ADWORDS_CONVERSION', '') 
            || !Configuration::updateValue('PD_GAEEP_OS_SEND_ORDER', '5,11,2') 
            || !Configuration::updateValue('PD_GAEEP_OS_REFUND_ORDER', 7)
            || !Configuration::updateValue('PD_GAEEP_GOOGLE_TAG_MANAGER', '')
            || !Configuration::updateValue('PD_GAEEP_GOOGLE_DR_ACTIVE', 0)
            || !PdGAEEPModel::installDB()
            || !$this->installModuleTabs()) {
            return false;
        }
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()
            || !PdGAEEPModel::uninstallDB()
            || !$this->uninstallModuleTab('AdminPdGoogleAnalytycEnhancedEcommercePro')
            || !Configuration::deleteByName('PD_GAEEP_GOOGLE_ANAL_SPV') 
            || !Configuration::deleteByName('PD_GAEEP_PRODUCT_ID_TYPE') 
            || !Configuration::deleteByName('PD_GAEEP_GOOGLE_ANAL_ID') 
            || !Configuration::deleteByName('PD_GAEEP_GOOGLE_ANAL4_ID') 
            || !Configuration::deleteByName('PD_GA4P_GOOGLE_ADWORDS_CONVERSION') 
            || !Configuration::deleteByName('PD_GAEEP_OS_SEND_ORDER') 
            || !Configuration::deleteByName('PD_GAEEP_OS_REFUND_ORDER')
            || !Configuration::deleteByName('PD_GAEEP_GOOGLE_DR_ACTIVE')
            || !Configuration::deleteByName('PD_GAEEP_GOOGLE_TAG_MANAGER')) {
            return false;
        }
        return true;
    }

    private function installModuleTabs()
    {
        $languages = Language::getLanguages();
        $tabs = array(
            'AdminPdGoogleAnalytycEnhancedEcommercePro' => array(
                'en' => 'Google Analytics orders',
                'pl' => 'Google Analytics zamówienia')
        );
        $main_tab_id = Tab::getIdFromClassName('AdminParentOrders');

        if ($main_tab_id) {
            foreach ($tabs as $class => $tab) {
                $tab_names_array = array();
                foreach ($tab as $tab_iso => $tab_name) {
                    foreach ($languages as $language) {
                        if ($language['iso_code'] == $tab_iso) {
                            $tab_names_array[$language['id_lang']] = $tab_name;
                        } else {
                            $tab_names_array[$language['id_lang']] = $this->l('Google Analytics orders');
                        }
                    }
                }
                $this->installModuleTab($class, $tab_names_array, $main_tab_id);
            }
        }
        return true;
    }

    private function installModuleTab($tabClass, $tab_name, $id_tab_parent)
    {
        file_put_contents('../img/t/'.$tabClass.'.gif', Tools::file_get_contents('logo.gif'));

        $tab = new Tab();
        $tab->name = $tab_name;
        $tab->class_name = $tabClass;
        $tab->module = $this->name;
        $tab->id_parent = $id_tab_parent;

        if (!$tab->save()) {
            return false;
        }

        return true;
    }

    private function uninstallModuleTab($tabClass)
    {
        $id_tab = Tab::getIdFromClassName($tabClass);
        if ($id_tab != 0) {
            $tab = new Tab($id_tab);
            $tab->delete();
            return true;
        }
        return false;
    }

    public function getContent()
    {
        if (Tools::isSubmit('btnSubmit')) {
            $this->postValidation();
            if (!count($this->errors)) {
                $this->postProcess();
            } else {
                foreach ($this->errors as $err) {
                    $this->html .= $this->displayError($err);
                }
            }
        } else {
            $this->html .= '<br />';
        }

        $this->html .= '<h2>'.$this->displayName.' (v'.$this->version.')</h2><p>'.$this->description.'</p>';
        $this->html .= $this->renderForm();
        $this->html .= '<br />';

        return $this->html;
    }

    private function postValidation()
    {
        $this->context->controller->getLanguages();

        if (Tools::isSubmit('btnSubmit')) {
            if (!Tools::getValue('PD_GAEEP_GOOGLE_ANAL_ID')) {
                $this->errors[] = $this->l('You must enter Your Google Analytics ID.');
            }
        }
    }

    private function postProcess()
    {
        if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('PD_GAEEP_GOOGLE_ANAL_ID', trim(Tools::getValue('PD_GAEEP_GOOGLE_ANAL_ID')));
            Configuration::updateValue('PD_GAEEP_GOOGLE_TAG_MANAGER', trim(Tools::getValue('PD_GAEEP_GOOGLE_TAG_MANAGER')));
            Configuration::updateValue('PD_GA4P_GOOGLE_ADWORDS_CONVERSION', trim(Tools::getValue('PD_GA4P_GOOGLE_ADWORDS_CONVERSION')));
            Configuration::updateValue('PD_GAEEP_GOOGLE_ANAL4_ID', trim(Tools::getValue('PD_GAEEP_GOOGLE_ANAL4_ID')));
            Configuration::updateValue('PD_GAEEP_PRODUCT_ID_TYPE', Tools::getValue('PD_GAEEP_PRODUCT_ID_TYPE'));
            Configuration::updateValue('PD_GAEEP_GOOGLE_ANAL_SPV', Tools::getValue('PD_GAEEP_GOOGLE_ANAL_SPV'));
            Configuration::updateValue('PD_GAEEP_OS_SEND_ORDER', implode(',', Tools::getValue('PD_GAEEP_OS_SEND_ORDER')));
            Configuration::updateValue('PD_GAEEP_OS_REFUND_ORDER', implode(',', Tools::getValue('PD_GAEEP_OS_REFUND_ORDER')));
            Configuration::updateValue('PD_GAEEP_GOOGLE_DR_ACTIVE', Tools::getValue('PD_GAEEP_GOOGLE_DR_ACTIVE'));

            $this->pd_google_tag_menager_id = htmlspecialchars_decode(Configuration::get('PD_GAEEP_GOOGLE_TAG_MANAGER'));
            $this->pd_google_analitycs_id = htmlspecialchars_decode(Configuration::get('PD_GAEEP_GOOGLE_ANAL_ID'));
            $this->pd_google_analitycs4_id = htmlspecialchars_decode(Configuration::get('PD_GAEEP_GOOGLE_ANAL4_ID'));
            $this->pd_google_dynamic_remarketing_id = htmlspecialchars_decode(Configuration::get('PD_GA4P_GOOGLE_ADWORDS_CONVERSION'));
            $this->pd_google_dynamic_remarketing = Configuration::get('PD_GAEEP_GOOGLE_DR_ACTIVE');
            $this->product_ids_type = Configuration::get('PD_GAEEP_PRODUCT_ID_TYPE');
            $this->send_page_view = Configuration::get('PD_GAEEP_GOOGLE_ANAL_SPV');
            $this->os_statuses_send = explode(',', Configuration::get('PD_GAEEP_OS_SEND_ORDER'));
        }
        $this->html .= $this->displayConfirmation($this->l('Settings updated'));
    }

    public function renderForm()
    {
        $switch = version_compare(_PS_VERSION_, '1.6.0', '>=') ? 'switch' : 'radio';
        $order_states = OrderState::getOrderStates($this->context->language->id);
        $url_part = $this->context->link->getAdminLink('AdminPdGoogleAnalytycEnhancedEcommercePro');
        $this->context->smarty->assign(array(
            'gtm_link' => $url_part.'&downloadGtmConfig=1',

        ));

        $fields_form_1 = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Module configuration'),
                    'icon' => 'icon-cogs'
                ),
                'tabs' => array(
                    'configAnalitycs' => $this->l('Google Analytics'),
                    'configTagManager' => $this->l('Google Tag Manager'),
                    'configGdr' => $this->l('Google Dynamic remarketing'),
                    'configOtherOptions' => $this->l('Other options'),
                    'configInstructions' => $this->l('Instructions'),
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Google Analytics id'),
                        'name' => 'PD_GAEEP_GOOGLE_ANAL_ID',
                        'desc' => $this->l('Please enter Google Analytics to get Your ID go to page: ').'<a href="https://analytics.google.com/analytics/web/#management/Settings/">'.$this->l('Google Analytics').'</a>'.$this->l(' after that open section Administration > Service settings > Tracking identifier '),
                        'placeholder' => 'UA-00000000-0',
                        'required' => true,
                        'tab' => 'configAnalitycs',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Google Analytics 4 id'),
                        'name' => 'PD_GAEEP_GOOGLE_ANAL4_ID',
                        'desc' => $this->l('Please enter Google Analytics 4 id, at the moment offer only basic tracking with automaticly captured events'),
                        'placeholder' => 'G-XXXXXXXX',
                        'required' => false,
                        'tab' => 'configAnalitycs',
                    ),
                    array(
                        'type' => $switch,
                        'label' => $this->l('Send page view'),
                        'class' => 't',
                        'name' => 'PD_GAEEP_GOOGLE_ANAL_SPV',
                        'desc' => $this->l('Send page view to Google Analytics? if you don’t want the snippet to send a pageview hit to Google Analytics please disable this option'),
                        'tab' => 'configAnalitycs',
                        'values' => array(
                            array(
                                'id' => 'yes',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'no',
                                'value' => 0,
                                'label' => $this->l('No')
                            ),
                        )
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Google Tag Manager id'),
                        'name' => 'PD_GAEEP_GOOGLE_TAG_MANAGER',
                        'desc' => $this->l('Please enter Google Tag Manager id, you can get Your id from ').'<a href="https://www.google.com/analytics/tag-manager/">'.$this->l('Tag manager page').'</a>',
                        'placeholder' => 'GTM-XXXXXXX',
                        'tab' => 'configTagManager',
                        'required' => true
                    ),

                    array(
                        'type' => $switch,
                        'label' => $this->l('Google Dynamic Remarketing'),
                        'class' => 't',
                        'name' => 'PD_GAEEP_GOOGLE_DR_ACTIVE',
                        'desc' => $this->l('Please select if module should implement Google Dynamic Remarketing trought DataLayer'),
                        'tab' => 'configGdr',
                        'values' => array(
                            array(
                                'id' => 'yes',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'no',
                                'value' => 0,
                                'label' => $this->l('No')
                            ),
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Google Coversion ID'),
                        'name' => 'PD_GA4P_GOOGLE_ADWORDS_CONVERSION',
                        'desc' => $this->l('Please enter Google Dynamic Remarketing conversion id, this field is optional use when needed'),
                        'required' => false,
                        'placeholder' => 'AW-XXXXXXXXXX',
                        'tab' => 'configGdr',
                    ),

                    array(
                        'type' => 'select',
                        'label' => $this->l('Product identifier'),
                        'name' => 'PD_GAEEP_PRODUCT_ID_TYPE',
                        'width' => 300,
                        'required' => true,
                        'class' => 'fixed-width-ld',
                        'tab' => 'configOtherOptions',
                        'desc' => $this->l('You can choose which product identifier we want to pass as a id to Google Analytics, if must match Your feed products identifiers'),
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => '0',
                                    'name' => $this->l('Id product (default)')
                                ),
                                array(
                                    'id' => '1',
                                    'name' => $this->l('Id product-id product attribute')
                                ),
                                array(
                                    'id' => '2',
                                    'name' => $this->l('Id product_id product attribute')
                                ),
                                array(
                                    'id' => '3',
                                    'name' => $this->l('Product reference')
                                ),
                                array(
                                    'id' => '4',
                                    'name' => $this->l('Product EAN')
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name',
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Order statuses to send order'),
                        'desc' => $this->l('Order statuses to send order to Google Analytics on order status change (orders are send in backend)'),
                        'name' => 'PD_GAEEP_OS_SEND_ORDER',
                        'tab' => 'configOtherOptions',
                        'multiple' => true,
                        'required' => true,
                        'options' => array(
                            'query' => $order_states,
                            'id' => 'id_order_state',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Order statuses for refund order'),
                        'desc' => $this->l('Order statuses for refund order in Google Analytics on order status change (orders are send in backend)'),
                        'name' => 'PD_GAEEP_OS_REFUND_ORDER',
                        'tab' => 'configOtherOptions',
                        'required' => true,
                        'multiple' => true,
                        'options' => array(
                            'query' => $order_states,
                            'id' => 'id_order_state',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'html',
                        'label' => $this->l('What to do at the begining'),
                        'tab' => 'configInstructions',
                        'name' => '',
                        'html_content' => $this->context->smarty->fetch($this->local_path.'views/templates/admin/instructions.tpl')
                    ),
                    array(
                        'type' => 'html',
                        'label' => $this->l('Automatic events configurations instructions'),
                        'tab' => 'configInstructions',
                        'name' => '',
                        'html_content' => $this->context->smarty->fetch($this->local_path.'views/templates/admin/instructions_auto.tpl')
                    ),
                    array(
                        'type' => 'html',
                        'label' => $this->l('Manual events configurations instructions'),
                        'tab' => 'configInstructions',
                        'name' => '',
                        'html_content' => $this->context->smarty->fetch($this->local_path.'views/templates/admin/instructions_manual.tpl')
                    )
                ),
                'submit' => array(
                    'name' => 'btnSubmit',
                    'title' => $this->l('Save settings'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'btnSubmit';
        $admin_link = $this->context->link->getAdminLink('AdminModules', false);
        $helper->currentIndex = $admin_link.'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form_1));
    }

    public function getConfigFieldsValues()
    {
        $return = array();
        $return['PD_GAEEP_GOOGLE_ANAL_ID'] = htmlspecialchars(Tools::getValue('PD_GAEEP_GOOGLE_ANAL_ID', Configuration::get('PD_GAEEP_GOOGLE_ANAL_ID')));
        $return['PD_GAEEP_GOOGLE_TAG_MANAGER'] = htmlspecialchars(Tools::getValue('PD_GAEEP_GOOGLE_TAG_MANAGER', Configuration::get('PD_GAEEP_GOOGLE_TAG_MANAGER')));
        $return['PD_GAEEP_GOOGLE_ANAL4_ID'] = htmlspecialchars(Tools::getValue('PD_GAEEP_GOOGLE_ANAL4_ID', Configuration::get('PD_GAEEP_GOOGLE_ANAL4_ID')));
        $return['PD_GA4P_GOOGLE_ADWORDS_CONVERSION'] = htmlspecialchars(Tools::getValue('PD_GA4P_GOOGLE_ADWORDS_CONVERSION', Configuration::get('PD_GA4P_GOOGLE_ADWORDS_CONVERSION')));
        $return['PD_GAEEP_PRODUCT_ID_TYPE'] = Tools::getValue('PD_GAEEP_PRODUCT_ID_TYPE', Configuration::get('PD_GAEEP_PRODUCT_ID_TYPE'));
        $return['PD_GAEEP_GOOGLE_ANAL_SPV'] = htmlspecialchars(Tools::getValue('PD_GAEEP_GOOGLE_ANAL_SPV', Configuration::get('PD_GAEEP_GOOGLE_ANAL_SPV')));
        $return['PD_GAEEP_OS_SEND_ORDER[]'] = Tools::getValue('PD_GAEEP_OS_SEND_ORDER', explode(',', Configuration::get('PD_GAEEP_OS_SEND_ORDER')));
        $return['PD_GAEEP_OS_REFUND_ORDER[]'] = Tools::getValue('PD_GAEEP_OS_REFUND_ORDER', explode(',', Configuration::get('PD_GAEEP_OS_REFUND_ORDER')));

        $return['PD_GAEEP_GOOGLE_DR_ACTIVE'] = htmlspecialchars(Tools::getValue('PD_GAEEP_GOOGLE_DR_ACTIVE', Configuration::get('PD_GAEEP_GOOGLE_DR_ACTIVE')));
        return $return;
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
        $newOrderStatus = $params['newOrderStatus'];
        $id_order_status_new = (int)$newOrderStatus->id;
        $id_order = (int)$params['id_order'];

        $user_id = '';
        // finaly if loged switch to // im not surre about that switching is ok
        if (isset($this->context->customer->id)) {
            $user_id = $this->context->customer->id;
        } 

        $analytics = new Analytics();
        $analytics->setProtocolVersion('1')
            ->setTrackingId($this->pd_google_analitycs_id)
            ->setClientId(PdGAEEPModel::getGAClientIdByIdOrder($id_order))
            ->setUserId($user_id);

        // order
        if ($id_order 
            && $id_order_status_new 
            && in_array($id_order_status_new, $this->os_statuses_send)
        ) {

            $obj = new PdGAEEPModel($id_order);
            if (isset($obj->id_order) && !$obj->order_send) {
                $order = new Order($id_order);
                if (!(Validate::isLoadedObject($order))) {
                    return;
                }

                $order_value = $order->total_paid;
                $order_tax = $order->total_paid_tax_incl - $order->total_paid_tax_excl;
                $shipping_cost = $order->total_shipping_tax_incl;

                $id_lang = (int)$this->context->language->id;
                $id_shop = (int)$this->context->shop->id;
                $id_currency = $order->id_currency;
                $currency = new Currency($id_currency);
                $currency_iso = $currency->iso_code;
                $order_products = $order->getProducts();
                $cart_rules = $this->getCartRuleWithCoupon($order);
                
                $analytics->setTransactionId($order->reference)
                    ->setAffiliation($this->http_referer)
                    ->setRevenue(Tools::ps_round($order_value, 2))
                    ->setTax(Tools::ps_round($order_tax, 2))
                    ->setShipping(Tools::ps_round($shipping_cost, 2))
                    ->setCouponCode(sizeof($cart_rules) ? $cart_rules['name'].' - '.$cart_rules['code'] : '');

                //dump($order_products);
                foreach ($order_products as &$op) {
                    $category = new Category($op['id_category_default'], $id_lang, $id_shop);
                    $op['category_name'] = addslashes($category->name);
                    $op['content_ids'] = $this->getProductIdStringByType($op);
                    $op['price'] = Tools::ps_round($op['unit_price_tax_incl'], 2);
                    $op['manufacturer'] = $op['id_manufacturer'] ? addslashes(Manufacturer::getNameById($op['id_manufacturer'])) : '';
                    $product = new Product($op['id_product'], false, $id_lang);
                    $attribute_combination_resume = $product->getAttributeCombinationsById($op['product_attribute_id'], $id_lang, true);

                    $op['variant'] = '';
                    if ($attribute_combination_resume) {
                        foreach ($attribute_combination_resume as  $acr) {
                            $op['variant']  .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                        }
                        $op['variant'] = mb_substr($op['variant'], 0, -3);
                    }

                    if (!empty($op['variant'])) {
                        $op['product_name'] = addslashes(Tools::replaceAccentedChars($product->name.' ('.$op['variant'].')'));
                    } else {
                        $op['product_name'] = addslashes($product->name);
                    }
                }

                //dump($order_products);
                //die();

                $productData = array();
                $pos = 0;
                foreach ($order_products as $o) {
                    $pos++;
                    $productData = array(
                        'sku' => $o['content_ids'],
                        'name' => $o['product_name'],
                        'brand' => $o['manufacturer'],
                        'category' => $o['category_name'],
                        'variant' => $o['variant'],
                        'price' => $o['price'],
                        'quantity' => $o['product_quantity'],
                        'coupon_code' => sizeof($cart_rules) ? $cart_rules['name'].' - '.$cart_rules['code'] : '',
                        'position' => $pos
                    );
                    $analytics->addProduct($productData);
                }

                $analytics->setProductActionToPurchase();
                $analytics->setEventCategory('Checkout')
                    ->setEventAction('purchase')
                    ->sendEvent();

                $obj->order_send = 1;
                $obj->date_upd = date('Y-m-d H:i:s');
                $obj->update();
            }
        }
        // refund
        if ($id_order 
            && $id_order_status_new 
            && in_array($id_order_status_new, $this->os_statuses_refund)
        ) {
            $obj = new PdGAEEPModel($id_order);
            if (isset($obj->id_order) && !$obj->refund_send) {
                $order = new Order($id_order);
                if (!(Validate::isLoadedObject($order))) {
                    return;
                }

                $analytics->setTransactionId($order->reference);

                $id_lang = (int)$this->context->language->id;
                $id_shop = (int)$this->context->shop->id;
                $id_currency = $order->id_currency;
                $currency = new Currency($id_currency);
                $currency_iso = $currency->iso_code;
                $order_products = $order->getProducts();
                $cart_rules = $this->getCartRuleWithCoupon($order);

                foreach ($order_products as &$op) {
                    $category = new Category($op['id_category_default'], $id_lang, $id_shop);
                    $op['category_name'] = addslashes($category->name);
                    $op['content_ids'] = $this->getProductIdStringByType($op);
                    $op['price'] = Tools::ps_round($op['unit_price_tax_incl'], 2);
                    $op['manufacturer'] = $op['id_manufacturer'] ? addslashes(Manufacturer::getNameById($op['id_manufacturer'])) : '';
                    $product = new Product($op['id_product'], false, $id_lang);
                    $attribute_combination_resume = $product->getAttributeCombinationsById($op['product_attribute_id'], $id_lang, true);
                    if ($attribute_combination_resume) {
                        $op['variant'] = '';
                        foreach ($attribute_combination_resume as  $acr) {
                            $op['variant']  .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                        }
                        $op['variant'] = mb_substr($op['variant'], 0, -3);
                    }

                    if (!empty($op['variant'])) {
                        $op['product_name'] = addslashes(Tools::replaceAccentedChars($product->name.' ('.$op['variant'].')'));
                    } else {
                        $op['product_name'] = addslashes($product->name);
                    }
                }

                $productData = array();
                $pos = 0;
                foreach ($order_products as $o) {
                    $pos++;
                    $productData = array(
                        'sku' => $o['content_ids'],
                        'name' => $o['product_name'],
                        'brand' => $o['manufacturer'],
                        'category' => $o['category_name'],
                        'variant' => $o['variant'],
                        'price' => $o['price'],
                        'quantity' => $o['product_quantity'],
                        'coupon_code' => sizeof($cart_rules) ? $cart_rules['name'].' - '.$cart_rules['code'] : '',
                        'position' => $pos
                    );
                    $analytics->addProduct($productData);
                }

                $analytics->setProductActionToRefund();
                $analytics->setEventCategory('Ecommerce')
                    ->setEventAction('refund')
                    ->sendEvent();

                $obj->refund_send = 1;
                $obj->date_upd = date('Y-m-d H:i:s');
                $obj->update();
            }
        }
    }

    public function hookActionObjectOrderAddAfter($params)
    {
        $order = isset($params['object']) ? $params['object'] : false;
        if ($order && Validate::isLoadedObject($order)) {
            $obj = new PdGAEEPModel();
            $obj->id_order = (int)$order->id;
            $obj->order_send = 0;
            $obj->refund_send = 0;
            $obj->client_id = $this->getGAUserId();
            $obj->date_add = date('Y-m-d H:i:s');
            $obj->date_upd = '0000-00-00 00:00:00';
            $obj->add();
        }
    }

    public function hookDisplayBackOfficeHeader($params)
    {
        $controller = Tools::getValue('controller');
        if ($controller == 'AdminOrders') {
            Media::addJsDef(array(
                'pdgoogleanalytycenhancedecommercepro_ajax_url' => $this->context->link->getAdminLink(
                    'AdminPdGoogleAnalytycEnhancedEcommercePro'
                ),
                'pdgoogleanalytycenhancedecommercepro_token' => Tools::getAdminTokenLite(
                    'AdminPdCeneoBasketServiceProOrdersList'
                ),
                'pdgoogleanalytycenhancedecommercepro_os_send' => $this->os_statuses_send,
                'pdgoogleanalytycenhancedecommercepro_os_refund' => $this->os_statuses_refund,
            ));
            $this->context->controller->addJquery();
            $this->context->controller->addJS($this->_path.'views/js/back.js');

            $id_currency = (int)$this->context->currency->id;
            $country = $this->context->country->iso_code;
            $currency = new Currency($id_currency);
            $currency_iso = $currency->iso_code;
            $id_lang = (int)$this->context->language->id;

            $this->smarty->assign(array(
                'pd_gaeep_tm_id' => $this->pd_google_tag_menager_id,
                'pd_gaeep_a_id' => $this->pd_google_analitycs_id,
                'pd_gaeep_a4_id' => $this->pd_google_analitycs4_id,
                'pd_gaeep_dr_conversion_id' => $this->pd_google_dynamic_remarketing_id,
                'pd_gaeep_currency' => $currency_iso,
                'pd_gaeep_country' => $country,
                'pd_gaeep_spv' => $this->send_page_view ? 'true' : 'false',
            ));

            return $this->display(__FILE__, 'displayBackOfficeHeader.tpl');
        }
    }

    public function hookDisplayAdminOrder($params)
    {
        if (!isset($params['id_order']) 
            || !$this->ps_version_1770_lte) {
            return;
        }

        $id_order = (int)$params['id_order'];
        $data = new PdGAEEPModel($id_order);
        if ($data && isset($data->id_order)) {
            $this->smarty->assign(array(
                'data' => $data,
                'id_order' => $id_order
            ));
            return $this->display(__FILE__, 'displayAdminOrder.tpl');
        }
    }

    public function hookDisplayAdminOrderSide($params)
    {
        if (!isset($params['id_order'])) {
            return;
        }

        $id_order = $params['id_order'];
        $data = new PdGAEEPModel($id_order);
        if ($data && isset($data->id_order)) {
            $this->smarty->assign(array(
                'data' => $data,
                'id_order' => $id_order
            ));
            return $this->display(__FILE__, 'displayAdminOrderSide.tpl');
        }
    }

    public function hookDisplayHeader($params)
    {
        if (!empty($this->pd_google_tag_menager_id) && !empty($this->pd_google_analitycs_id)) {
            
            Media::addJsDef(array(
                'pdgoogleanalytycenhancedecommercepro_secure_key' => $this->secure_key,
                'pdgoogleanalytycenhancedecommercepro_gdr_enabled' => $this->pd_google_dynamic_remarketing,
                'pdgoogleanalytycenhancedecommercepro_ajax_link' => $this->context->link->getModuleLink(
                    'pdgoogleanalytycenhancedecommercepro',
                    'ajax', 
                    array()
                 ),
            ));

            $this->context->controller->registerJavascript(
                'modules-pdgoogleanalytycenhancedecommercepro-front',
                'modules/'.$this->name.'/views/js/front.js',
                array('position' => 'bottom', 'priority' => 1)
            );

            $id_currency = (int)$this->context->currency->id;
            $country = $this->context->country->iso_code;
            $currency = new Currency($id_currency);
            $currency_iso = $currency->iso_code;
            $id_lang = (int)$this->context->language->id;

            $user_id = '';
            // finaly if loged switch to 
            if (isset($this->context->customer->id)) {
                $user_id = $this->context->customer->id;
            }

            $this->smarty->assign(array(
                'pd_gaeep_tm_id' => $this->pd_google_tag_menager_id,
                'pd_gaeep_a_id' => $this->pd_google_analitycs_id,
                'pd_gaeep_a4_id' => $this->pd_google_analitycs4_id,
                'pd_gaeep_dr_conversion' => $this->pd_google_dynamic_remarketing_id,
                'pd_gaeep_currency' => $currency_iso,
                'pd_gaeep_country' => $country,
                'pd_gaeep_spv' => $this->send_page_view ? 'true' : 'false',
                'pd_gaeep_gdr_enabled' => $this->pd_google_dynamic_remarketing,
                'pd_gaeep_customer_id' => $user_id,
            ));

            return $this->display(__FILE__, 'displayHeader.tpl');
        }
    }

    public function hookDisplayAfterBodyOpeningTag($params)
    {
        if (!empty($this->pd_google_tag_menager_id)) {
            $this->smarty->assign(array(
                'pd_gaeep_tm_id' => $this->pd_google_tag_menager_id
            ));
            return $this->display(__FILE__, 'displayAfterBodyOpeningTag.tpl');
        }
    }

    public function hookDisplayBeforeBodyClosingTag($params)
    {
        if (empty($this->pd_google_tag_menager_id) 
            || empty($this->pd_google_analitycs_id)) {
            return;
        }

        $cn = self::getControlerName();
      
        $currency_iso = (string)$this->context->currency->iso_code;
        $id_lang = (int)$this->context->language->id;
        $id_shop = (int)$this->context->shop->id;

        switch ($cn) {

            case 'order':
                
                $cart = Context::getContext()->cart;
                if (!($cart instanceof Cart)) {
                    return;
                }
                if (isset($cart->id)) {
                    $value = $cart->getOrderTotal(true);
                    $cart_products = $cart->getProducts();
                    
                    foreach ($cart_products as &$cp) {
                        $category = new Category($cp['id_category_default'], $id_lang, $id_shop);
                        $cp['category_name'] = addslashes($category->name);
                        $cp['content_ids'] = $this->getProductIdStringByType($cp);
                        $product = new Product($cp['id_product'], false, $id_lang);
                        $cp['manufacturer'] = $product->id_manufacturer ? addslashes(Manufacturer::getNameById($product->id_manufacturer)) : '';
                        $attribute_combination_resume = $product->getAttributeCombinationsById($cp['id_product_attribute'], $id_lang, true);
                        if ($attribute_combination_resume) {
                            $cp['variant'] = '';
                            foreach ($attribute_combination_resume as  $acr) {
                                $cp['variant']  .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                            }
                            $cp['variant'] = mb_substr($cp['variant'], 0, -3);
                        }

                        if (!empty($cp['variant'])) {
                           $cp['name'] = addslashes($product->name.' ('.$cp['variant'].')');
                        } else {
							$cp['name'] = addslashes($product->name);
						}
						
                    }

                    $this->smarty->assign(array(
                        'content_products' => $cart_products,
                        'content_value' => Tools::ps_round($value, 2),
                        'currency' => $currency_iso,
                        'tagType' => 'order',
                    ));
                    return $this->display(__FILE__, 'displayFooter.tpl');
                }
                break;

            case 'order-opc':
                
                $cart = Context::getContext()->cart;
                if (!($cart instanceof Cart)) {
                    return;
                }
                if (isset($cart->id)) {
                    $value = $cart->getOrderTotal(true);
                    $cart_products = $cart->getProducts();
                    
                    foreach ($cart_products as &$cp) {
                        $category = new Category($cp['id_category_default'], $id_lang, $id_shop);
                        $cp['category_name'] = addslashes($category->name);
                        $cp['content_ids'] = $this->getProductIdStringByType($cp);
                        $product = new Product($cp['id_product'], false, $id_lang);
                        $cp['manufacturer'] = $product->id_manufacturer ? addslashes(Manufacturer::getNameById($product->id_manufacturer)) : '';
                        $attribute_combination_resume = $product->getAttributeCombinationsById($cp['id_product_attribute'], $id_lang, true);
                        if ($attribute_combination_resume) {
                            $cp['variant'] = '';
                            foreach ($attribute_combination_resume as  $acr) {
                                $cp['variant']  .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                            }
                            $cp['variant'] = mb_substr($cp['variant'], 0, -3);
                        }


                        if (!empty($cp['variant'])) {
                           $cp['name'] = addslashes($product->name.' ('.$cp['variant'].')');
                        } else {
							$cp['name'] = addslashes($product->name);
						}
                    }

                    $this->smarty->assign(array(
                        'content_products' => $cart_products,
                        'content_value' => Tools::ps_round($value, 2),
                        'currency' => $currency_iso,
                        'tagType' => 'order',
                    ));
                    return $this->display(__FILE__, 'displayFooter.tpl');
                }
                break;

            case 'onepagecheckout':
                
                $cart = Context::getContext()->cart;
                if (!($cart instanceof Cart)) {
                    return;
                }
                if (isset($cart->id)) {
                    $value = $cart->getOrderTotal(true);
                    $cart_products = $cart->getProducts();
                    
                    foreach ($cart_products as &$cp) {
                        $category = new Category($cp['id_category_default'], $id_lang, $id_shop);
                        $cp['category_name'] = addslashes($category->name);
                        $cp['content_ids'] = $this->getProductIdStringByType($cp);
                        $product = new Product($cp['id_product'], false, $id_lang);
                        $cp['manufacturer'] = $product->id_manufacturer ? addslashes(Manufacturer::getNameById($product->id_manufacturer)) : '';
                        $attribute_combination_resume = $product->getAttributeCombinationsById($cp['id_product_attribute'], $id_lang, true);
                        if ($attribute_combination_resume) {
                            $cp['variant'] = '';
                            foreach ($attribute_combination_resume as  $acr) {
                                $cp['variant']  .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                            }
                            $cp['variant'] = mb_substr($cp['variant'], 0, -3);
                        }

                        if (!empty($cp['variant'])) {
                           $cp['name'] = addslashes($product->name.' ('.$cp['variant'].')');
                        } else {
						   $cp['name'] = addslashes($product->name);
						}
                    }

                    $this->smarty->assign(array(
                        'content_products' => $cart_products,
                        'content_value' => Tools::ps_round($value, 2),
                        'currency' => $currency_iso,
                        'tagType' => 'order',
                    ));
                    return $this->display(__FILE__, 'displayFooter.tpl');
                }
                break;

            case 'onepagecheckoutps':
                
                $cart = Context::getContext()->cart;
                if (!($cart instanceof Cart)) {
                    return;
                }
                if (isset($cart->id)) {
                    $value = $cart->getOrderTotal(true);
                    $cart_products = $cart->getProducts();
                    
                    foreach ($cart_products as &$cp) {
                        $category = new Category($cp['id_category_default'], $id_lang, $id_shop);
                        $cp['category_name'] = addslashes($category->name);
                        $cp['content_ids'] = $this->getProductIdStringByType($cp);
                        $product = new Product($cp['id_product'], false, $id_lang);
                        $cp['manufacturer'] = $product->id_manufacturer ? addslashes(Manufacturer::getNameById($product->id_manufacturer)) : '';
                        $attribute_combination_resume = $product->getAttributeCombinationsById($cp['id_product_attribute'], $id_lang, true);
                        if ($attribute_combination_resume) {
                            $cp['variant'] = '';
                            foreach ($attribute_combination_resume as  $acr) {
                                $cp['variant']  .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                            }
                            $cp['variant'] = mb_substr($cp['variant'], 0, -3);
                        }

                        if (!empty($cp['variant'])) {
                           $cp['name'] = addslashes($product->name.' ('.$cp['variant'].')');
                        } else {
						   $cp['name'] = addslashes($product->name);
						}
                    }

                    $this->smarty->assign(array(
                        'content_products' => $cart_products,
                        'content_value' => Tools::ps_round($value, 2),
                        'currency' => $currency_iso,
                        'tagType' => 'order',
                    ));
                    return $this->display(__FILE__, 'displayFooter.tpl');
                }
                break;

            case 'thecheckout':
                
                $cart = Context::getContext()->cart;
                if (!($cart instanceof Cart)) {
                    return;
                }
                if (isset($cart->id)) {
                    $value = $cart->getOrderTotal(true);
                    $cart_products = $cart->getProducts();
                    
                    foreach ($cart_products as &$cp) {
                        $category = new Category($cp['id_category_default'], $id_lang, $id_shop);
                        $cp['category_name'] = addslashes($category->name);
                        $cp['content_ids'] = $this->getProductIdStringByType($cp);
                        $product = new Product($cp['id_product'], false, $id_lang);
                        $cp['manufacturer'] = $product->id_manufacturer ? addslashes(Manufacturer::getNameById($product->id_manufacturer)) : '';
                        $attribute_combination_resume = $product->getAttributeCombinationsById($cp['id_product_attribute'], $id_lang, true);
                        if ($attribute_combination_resume) {
                            $cp['variant'] = '';
                            foreach ($attribute_combination_resume as  $acr) {
                                $cp['variant']  .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                            }
                            $cp['variant'] = mb_substr($cp['variant'], 0, -3);
                        }
                        if (!empty($cp['variant'])) {
                           $cp['name'] = addslashes($product->name.' ('.$cp['variant'].')');
                        } else {
							$cp['name'] = addslashes($product->name);
						}
                    }

                    $this->smarty->assign(array(
                        'content_products' => $cart_products,
                        'content_value' => Tools::ps_round($value, 2),
                        'currency' => $currency_iso,
                        'tagType' => 'order',
                    ));
                    return $this->display(__FILE__, 'displayFooter.tpl');
                }
                break;

            case 'supercheckout':
                
                $cart = Context::getContext()->cart;
                if (!($cart instanceof Cart)) {
                    return;
                }
                if (isset($cart->id)) {
                    $value = $cart->getOrderTotal(true);
                    $cart_products = $cart->getProducts();
                    
                    foreach ($cart_products as &$cp) {
                        $category = new Category($cp['id_category_default'], $id_lang, $id_shop);
                        $cp['category_name'] = addslashes($category->name);
                        $cp['content_ids'] = $this->getProductIdStringByType($cp);
                        $product = new Product($cp['id_product'], false, $id_lang);
                        $cp['manufacturer'] = $product->id_manufacturer ? addslashes(Manufacturer::getNameById($product->id_manufacturer)) : '';
                        $attribute_combination_resume = $product->getAttributeCombinationsById($cp['id_product_attribute'], $id_lang, true);
                        if ($attribute_combination_resume) {
                            $cp['variant'] = '';
                            foreach ($attribute_combination_resume as  $acr) {
                                $cp['variant']  .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                            }
                            $cp['variant'] = mb_substr($cp['variant'], 0, -3);
                        }
                        if (!empty($cp['variant'])) {
                           $cp['name'] = addslashes($product->name.' ('.$cp['variant'].')');
                        } else {
						   $cp['name'] = addslashes($product->name);
						}
                    }

                    $this->smarty->assign(array(
                        'content_products' => $cart_products,
                        'content_value' => Tools::ps_round($value, 2),
                        'currency' => $currency_iso,
                        'tagType' => 'order',
                    ));
                    return $this->display(__FILE__, 'displayFooter.tpl');
                }
                break;

            default:
                break;
        }
    }

    public function hookDisplayOrderConfirmation($params)
    {
        return $this->display(__FILE__, 'displayOrderConfirmation.tpl');
    }


    public function hookActionProductSearchAfter($params)
    {
      
        if (isset($params['products'])) {
            self::$products_hook_exec = $params['products'];
        }
    }

    public function hookDisplayFooter($params)
    {

        if (empty($this->pd_google_tag_menager_id) 
            || empty($this->pd_google_analitycs_id)) {
            return;
        }

        $cn = self::getControlerName();

      
        $currency_iso = (string)$this->context->currency->iso_code;
        $id_lang = (int)$this->context->language->id;
        $id_shop = (int)$this->context->shop->id;

        switch ($cn) {

            case 'category':

                $id_category = (int)Tools::getValue('id_category');
                $category = new Category($id_category, $id_lang, $id_shop);
                $total_value = 0;
                $products = self::$products_hook_exec;
                foreach ($products as &$cp) {

                    $cp['content_ids'] = $this->getProductIdStringByType($cp);
                    $product = new Product($cp->id_product, false, $id_lang);
                    $cp['manufacturer'] = Manufacturer::getNameById($cp->id_manufacturer) ? addslashes(Manufacturer::getNameById($cp->id_manufacturer)) : '';
                    $attribute_combination_resume = $product->getAttributeCombinationsById($cp->cache_default_attribute, $id_lang, true);
                    if ($attribute_combination_resume) {
                        $cp['variant'] = '';
                        foreach ($attribute_combination_resume as  $acr) {
                            $cp['variant']  .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                        }
                        $cp['variant'] = mb_substr($cp['variant'], 0, -3);
                    }
                    $total_value += $cp['price_amount'];

                    if (!empty($cp['variant'])) {
                        $cp['name'] = addslashes($product->name.' ('.$cp['variant'].')');
                    } else {
						$cp['name'] = addslashes($product->name);
					}
                }

                if (isset($category->id)) {
                    $this->smarty->assign(array(
                        'total_value' => $total_value,
                        'content_name' => 'Category page: '.addslashes($category->name),
                        'content_products' => $products,
                        'currency' => $currency_iso,
                        'tagType' => 'category',
                        'pd_gaeep_gdr_enabled' => $this->pd_google_dynamic_remarketing,
                    ));
                    return $this->display(__FILE__, 'displayFooter.tpl');
                }
                break;


            case 'prices-drop':
                
                $products = self::$products_hook_exec;
                $total_value = 0;
                foreach ($products as &$cp) {
                    $cp['content_ids'] = $this->getProductIdStringByType($cp);
                    $product = new Product($cp->id_product, false, $id_lang);
                    $cp['manufacturer'] = Manufacturer::getNameById($cp->id_manufacturer) ? addslashes(Manufacturer::getNameById($cp->id_manufacturer)) : '';
                    $attribute_combination_resume = $product->getAttributeCombinationsById($cp->cache_default_attribute, $id_lang, true);
                    if ($attribute_combination_resume) {
                        $cp['variant'] = '';
                        foreach ($attribute_combination_resume as  $acr) {
                            $cp['variant']  .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                        }
                        $cp['variant'] = mb_substr($cp['variant'], 0, -3);
                    }
                    $total_value += $cp['price_amount'];
                    if (!empty($cp['variant'])) {
                        $cp['name'] = addslashes($product->name.' ('.$cp['variant'].')');
                    } else {
						$cp['name'] = addslashes($product->name);
					}
                }

                if (is_array($products) && sizeof($products)) {
                    $this->smarty->assign(array(
                        'total_value' => $total_value,
                        'content_name' => 'Prices drop page',
                        'content_products' => $products,
                        'currency' => $currency_iso,
                        'tagType' => 'prices-drop',
                        'pd_gaeep_gdr_enabled' => $this->pd_google_dynamic_remarketing,
                    ));
                    return $this->display(__FILE__, 'displayFooter.tpl');
                }
                break;
            

            case 'new-products':
                
                $products = self::$products_hook_exec;
                $total_value = 0;
                foreach ($products as &$cp) {
                    $cp['content_ids'] = $this->getProductIdStringByType($cp);
                    $product = new Product($cp->id_product, false, $id_lang);
                    $cp['manufacturer'] = Manufacturer::getNameById($cp->id_manufacturer) ? addslashes(Manufacturer::getNameById($cp->id_manufacturer)) : '';
                    $attribute_combination_resume = $product->getAttributeCombinationsById($cp->cache_default_attribute, $id_lang, true);
                    if ($attribute_combination_resume) {
                        $cp['variant'] = '';
                        foreach ($attribute_combination_resume as  $acr) {
                            $cp['variant']  .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                        }
                        $cp['variant'] = mb_substr($cp['variant'], 0, -3);
                    }
                    $total_value += $cp['price_amount'];
                    if (!empty($cp['variant'])) {
                        $cp['name'] = addslashes($product->name.' ('.$cp['variant'].')');
                    } else {
                        $cp['name'] = addslashes($product->name);
                    }
                }

                if (is_array($products) && sizeof($products)) {
                    $this->smarty->assign(array(
                        'total_value' => $total_value,
                        'content_name' => 'New products page',
                        'content_products' => $products,
                        'currency' => $currency_iso,
                        'tagType' => 'new-products',
                        'pd_gaeep_gdr_enabled' => $this->pd_google_dynamic_remarketing,
                    ));
                    return $this->display(__FILE__, 'displayFooter.tpl');
                }
                break;

            case 'best-sales':
                
                $products = self::$products_hook_exec;
                $total_value = 0;
                foreach ($products as &$cp) {
                    $cp['content_ids'] = $this->getProductIdStringByType($cp);
                    $product = new Product($cp->id_product, false, $id_lang);
                    $cp['manufacturer'] = Manufacturer::getNameById($cp->id_manufacturer) ? addslashes(Manufacturer::getNameById($cp->id_manufacturer)) : '';
                    $attribute_combination_resume = $product->getAttributeCombinationsById($cp->cache_default_attribute, $id_lang, true);
                    if ($attribute_combination_resume) {
                        $cp['variant'] = '';
                        foreach ($attribute_combination_resume as  $acr) {
                            $cp['variant']  .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                        }
                        $cp['variant'] = mb_substr($cp['variant'], 0, -3);
                    }
                    $total_value += $cp['price_amount'];
                    if (!empty($cp['variant'])) {
                        $cp['name'] = addslashes($product->name.' ('.$cp['variant'].')');
                    } else {
						$cp['name'] = addslashes($product->name);
					}
                }

                if (is_array($products) && sizeof($products)) {
                    $this->smarty->assign(array(
                        'total_value' => $total_value,
                        'content_name' => 'Best sales page',
                        'content_products' => $products,
                        'currency' => $currency_iso,
                        'tagType' => 'best-sales',
                        'pd_gaeep_gdr_enabled' => $this->pd_google_dynamic_remarketing,
                    ));
                    return $this->display(__FILE__, 'displayFooter.tpl');
                }
                break;


            case 'product':

                $product = new Product((int)Tools::getValue('id_product'), false, $id_lang);
                $id_product_attribute = (int)Tools::getValue('id_product_attribute');
                $price = Product::getPriceStatic($product->id, true, $id_product_attribute, 6, null, false, true);
                $variant = '';
                if ($id_product_attribute) {
                    $attribute_combination_resume = $product->getAttributeCombinationsById($id_product_attribute, $id_lang, true);
                    if ($attribute_combination_resume) {
                        foreach ($attribute_combination_resume as  $acr) {
                            $variant .= $acr['group_name'].': '.$acr['attribute_name'].' - ';
                        }
                        $variant = mb_substr($variant, 0, -3);
                    } 
                }

                if (!empty($variant)) {
                    $product_name = addslashes($product->name.' ('.$variant.')');
                } else {
                    $product_name = addslashes($product->name);
                }


                $this->smarty->assign(array(
                    'content_manufacturer' => $product->id_manufacturer ? addslashes(Manufacturer::getNameById($product->id_manufacturer)) : '',
                    'content_ids' =>  $this->getProductIdStringByType($product, $id_product_attribute),
                    'content_name' =>  $product_name,
                    'content_category' => ucfirst(addslashes($product->category)),
                    'content_variant' => $variant,
                    'content_value' => Tools::ps_round($price, 2),
                    'currency' => $currency_iso,
                    'tagType' => 'product',
                    'pd_gaeep_gdr_enabled' => $this->pd_google_dynamic_remarketing,
                ));

                return $this->display(__FILE__, 'displayFooter.tpl');
            
            case 'cart':
                
                $cart = Context::getContext()->cart;
                if (!($cart instanceof Cart)) {
                    return;
                }
                if (isset($cart->id)) {
                    $value = $cart->getOrderTotal(true);
                    $cart_products = $cart->getProducts();

                    
                    foreach ($cart_products as &$cp) {
                        $category = new Category($cp['id_category_default'], $id_lang, $id_shop);
                        $cp['category_name'] = addslashes($category->name);
                        $cp['content_ids'] = $this->getProductIdStringByType($cp);
                        $product = new Product($cp['id_product'], false, $id_lang);
                        $cp['manufacturer'] = $product->id_manufacturer ? Manufacturer::getNameById($product->id_manufacturer) : '';
                        $attribute_combination_resume = $product->getAttributeCombinationsById($cp['id_product_attribute'], $id_lang, true);
                        if ($attribute_combination_resume) {
                            $cp['variant'] = '';
                            foreach ($attribute_combination_resume as  $acr) {
                                $cp['variant']  .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                            }
                            $cp['variant'] = mb_substr($cp['variant'], 0, -3);
                        }

                        if (!empty($cp['variant'])) {
                            $cp['name'] = addslashes($product->name.' ('.$cp['variant'].')');
                        } else {
							$cp['name'] = addslashes($product->name);
						}
                    }

                    $this->smarty->assign(array(
                        'content_products' => $cart_products,
                        'content_value' => Tools::ps_round($value, 2),
                        'currency' => $currency_iso,
                        'tagType' => 'cart',

                    ));
                    return $this->display(__FILE__, 'displayFooter.tpl');
                }
                break;
            
            case 'search':
                
                $search_string = Tools::getValue('s');
                $search_string = Tools::replaceAccentedChars(urldecode($search_string));

                $orderBy  = Tools::getProductsOrder('by');
                $orderWay = Tools::getProductsOrder('way');
                $number = (int) Tools::getValue('resultsPerPage');
                if ($number <= 0) {
                    $number = Configuration::get('PS_PRODUCTS_PER_PAGE');
                }
                $page = (int)Tools::getValue('page', 1);

                $products_partial = Search::find($this->context->language->id, $search_string, $page, $number, $orderBy, $orderWay);
                $search_products = Product::getProductsProperties($id_lang, $products_partial['result']);

                $total_value = 0;
                foreach ($search_products as &$sp) {
                    $sp['content_ids'] = $this->getProductIdStringByType($sp);
                    $total_value += $sp['price_amount'];
                }

                if (is_array($search_products) && sizeof($search_products)) {
                    $this->smarty->assign(array(
                        'total_value' => $total_value,
                        'content_products' => $search_products,
                        'tagType' => 'search',
                        'pd_gaeep_gdr_enabled' => $this->pd_google_dynamic_remarketing,
                    ));
                    return $this->display(__FILE__, 'displayFooter.tpl');
                }
                break;

            default:
                break;
        }
    }

    public static function getControlerName()
    {
        $page_name = 'index';
        if (!empty(Context::getContext()->controller->php_self)) {
            $page_name = (string)Context::getContext()->controller->php_self;
        } else {
            $page_name = (string)Tools::getValue('controller');
        }
        return $page_name;
    }
    
    public function getProductIdStringByType($product, $id_product_attribute = false)
    {
        $id_lang = (int)$this->context->language->id;
        $ids_type = $this->product_ids_type;

        if ($product instanceof Product) {

            if (isset($product->id)) {
                $id_product = (int)$product->id;
            } else {
                $id_product = (int)$product->id_product;
            }
            $ean13 = (string)$product->ean13;
            $reference = (string)$product->reference;
            if (!$id_product_attribute) {
                $id_product_attribute = (int)$product->cache_default_attribute;
            }

            if ($id_product_attribute) {
                $attribute_combination_resume = $product->getAttributeCombinationsById($id_product_attribute, $id_lang, true);
            
                if ($attribute_combination_resume && is_array($attribute_combination_resume) && sizeof($attribute_combination_resume)) {
                    if (isset($attribute_combination_resume['ean13'])) {
                        $ean13 = (string)$attribute_combination_resume['ean13'];
                    }
                    if (isset($attribute_combination_resume['reference'])) {
                        $reference = (string)$attribute_combination_resume['reference'];
                    }
                }
            }

        } elseif (is_array($product)) {

            $id_product = (int)$product['id_product'];
            $ean13 = (string)$product['ean13'];
            $reference = (string)$product['reference'];
            if (!$id_product_attribute && isset($product['id_product_attribute'])) {
                $id_product_attribute = (int)$product['id_product_attribute'];
            }

            if (!$id_product_attribute && isset($product['product_attribute_id'])) {
                $id_product_attribute = (int)$product['product_attribute_id'];
            }

            if ($id_product_attribute) {
                $product = new Product($id_product);
                $attribute_combination_resume = $product->getAttributeCombinationsById($id_product_attribute, $id_lang, true);
                if ($attribute_combination_resume && sizeof($attribute_combination_resume)) {
                    $ean13 = (string)$attribute_combination_resume[0]['ean13'];
                    $reference = (string)$attribute_combination_resume[0]['reference'];
                }
            }

        } else {

            if (isset($product->id)) {
                $id_product = (int)$product->id;
            } else {
                $id_product = (int)$product->id_product;
            }

            $ean13 = (string)$product->ean13;
            $reference = (string)$product->reference;
            if (isset($product->cache_default_attribute)) {
                $id_product_attribute = (int)$product->cache_default_attribute;
            }
        }

        $ecomm_prodid = '';
        switch ($ids_type) {
            case '0':
                $ecomm_prodid = $id_product;
                break;
            case '1':
                if ($id_product_attribute) {
                    $ecomm_prodid = $id_product.'-'.$id_product_attribute;
                }
                break;
            case '2':
                if ($id_product_attribute) {
                    $ecomm_prodid = $id_product.'_'.$id_product_attribute;
                }
                break;
            case '3':
                $ecomm_prodid = $reference;
                break;
            case '4':
                $ecomm_prodid = $ean13;
                break;
            default:
                $ecomm_prodid = $id_product;
                break;
        }
        return $ecomm_prodid;
    }

    public function getCartRuleWithCoupon($order)
    {
        if (isset($this->context->cart)) {
            $cart_rules = $this->context->cart->getCartRules(CartRule::FILTER_ACTION_ALL, false);
        } else {
            $cart = new Cart($order->id_cart);
            $cart_rules = $cart->getCartRules(CartRule::FILTER_ACTION_ALL, false);
        }
        $out = array();
        if ($cart_rules) {
            foreach ($cart_rules as $cr) {
                if (!empty($cr['code'])) {
                    $out['name'] = $cr['name'];
                    $out['code'] = $cr['code'];
                }
            }
        }
        return $out;
    }

    public function getCarriersArray()
    {
        $sql = '
        SELECT c.`id_carrier`, c.`name`
        FROM `' . _DB_PREFIX_ . 'carrier` c
        ' . Shop::addSqlAssociation('carrier', 'c') . '
        WHERE c.`deleted` = 0';

        $carriers = Db::getInstance()->executeS($sql);
        $out = array();
        foreach ($carriers as  $carrier) {
            if ($carrier['name'] == '0') {
                $out[$carrier['id_carrier']] = Carrier::getCarrierNameFromShopName();
            } else {
                $out[$carrier['id_carrier']] = $carrier['name'];
            }
        }
        
        return $out;
    }

    public function getRefererFromIdConnection($id_connections)
    {
        $sql = 'SELECT SQL_NO_CACHE `http_referer` FROM `'._DB_PREFIX_.'connections`
            WHERE id_connections = '.(int)$id_connections.'
            AND `date_add` > \''.pSQL(date('Y-m-d H:i:00', time() - 1800)).'\'
            '.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER).'
            ORDER BY `date_add` DESC';
        if ($referer = Db::getInstance()->getValue($sql, false)) {
            $this->http_referer = $referer;
        }
    }
    
    public function getGAUserId()
    {
        if (isset($_COOKIE['_ga'])) {
            $ga_user_id = $_COOKIE['_ga'];
            $lenght = strlen($ga_user_id);
            $ga_user_id = substr($ga_user_id, 6, $lenght);
            return $ga_user_id;
        } 
        return '';
    }
}
