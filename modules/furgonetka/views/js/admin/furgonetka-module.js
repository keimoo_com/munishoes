$(document).ready(function() {
    $(document).on('click', '.furgonetka-connect, .furgonetka-reset' , function(e) {
        e.preventDefault();
        var that = $(this);
        var href = that.attr('href');
        $.ajax({
            type: 'POST',
            async: true,
            dataType: 'json',
            url: href,
            success: function(response) {
                if(response.status == 'ok'){
                    window.location = window.location.href;
                }else if(response.status == 'iframe'){
                    openIframe(response.html);
                }else if(response.status == 'error'){
                    showErrorMessage(response.error)
                }
            }
        });
    });

    function openIframe(html){
        $('body').append(html);
        $('#furgonetka-iframe').fadeIn();
        $('#furgonetka-iframe iframe').on("load", function(e){
            try{
                var iframeContent = $(this).contents().find("#connect-status").html();
                if(iframeContent === 'ok'){
                        $('#furgonetka-iframe').remove();
                        $(document).trigger('furgonetka-iframe:close');
                }
            }
            catch(e){

            }
        });
    }

    $(document).on('furgonetka-iframe:close', function(e, data){
        window.location = window.location.href;
    });
});
