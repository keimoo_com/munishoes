<?php
require_once __DIR__.'/../api/soap/SoapClasess.php';
require_once __DIR__.'/../api/soap/SoapFurgonetka.php';

class FurgonetkaDelivery
{
    private $test;
    private $resetToken = false;

    public function __construct($test)
    {
        $this->test = $test;
    }

    public function getPackageFormUrl($idOrder)
    {
        $order = new Order($idOrder);

        if(!$order->id){
            return false;
        }

        try {
            $auth = FurgonetkaAuth::getAuth($this->test);
            $sender = $this->__getSender();
            $receiver = $this->__getReceiver($order);
            $parcel = $this->__getParcel($order);
            $source_id = FurgonetkaAuth::getSourceId($this->test);
            $services = new FurgonetkaSoapServices();

            // Prepare COD params.
            if (isset($order->module) && \in_array($order->module, array('cashondelivery', 'ps_cashondelivery', 'cashondeliveryplus'))) {
                $services->cod = array(
                    'amount' => isset($order->total_paid_tax_incl) ? $order->total_paid_tax_incl : $order->total_paid,
                    'iban' => Configuration::getGlobalValue('FURGONETKA_IBAN'),
                    'name' => $sender->name
                );
            }
        } catch (Exception $e) {
            throw new Exception(
                'Brak połączonego konta z Furgonetka.pl. Przejdź do konfiguracji modułu i podłącz swoje konto.'
            );
        }
        try {
            $soap = new SoapFurgonetka($this->test);
            $response = $soap->getPackageFormUrl($auth, $sender, $receiver, $order->id, $source_id, $parcel, $services);
            $response->validateThrow();
        } catch (Exception $e) {
            throw new Exception('Error get url.<br>' . $e->getMessage());
        }
        return $response->url;
    }

    private function __getSender()
    {
        $street = Configuration::getGlobalValue('FURGONETKA_STREET');
        $postcode = Configuration::getGlobalValue('FURGONETKA_POSTCODE');
        $city = Configuration::getGlobalValue('FURGONETKA_CITY');
        $name = Configuration::getGlobalValue('FURGONETKA_NAME');
        $email = Configuration::getGlobalValue('FURGONETKA_EMAIL');
        $company = Configuration::getGlobalValue('FURGONETKA_COMPANY');
        $phone = Configuration::getGlobalValue('FURGONETKA_PHONE');
        $country = Configuration::getGlobalValue('FURGONETKA_COUNTRY');
        $sender = new FurgonetkaSoapSender($street, $postcode, $city,$name, $email, $company, $country, null, $phone);
        return $sender;
    }

    private function __getReceiver(Order $order)
    {
        $machine = FurgonetkaDb::getMachineCodeFromOrder($order->id);
        $machineCode = isset($machine['machine_code'])? $machine['machine_code'] : null;
        $machineType = isset($machine['machine_type'])? $machine['machine_type'] : null;
        $address = new Address($order->id_address_delivery);
        $country = new Country($address->id_country);
        $customer = new Customer($order->id_customer);
        $phone = empty(\trim($address->phone)) ? empty(\trim($address->phone_mobile))? '' : $address->phone_mobile : $address->phone;

        /**
         * Use name from the address and fallback to customer name when name in address is empty
         */
        $name = $address->firstname . ' ' . $address->lastname;

        if (empty(\trim($name))) {
            $name = $customer->firstname . ' ' . $customer->lastname;
        }

        $receiver = new FurgonetkaSoapReceiver(
            $address->address1.' '.$address->address2,
            $address->postcode,
            $address->city,
            $name,
            $customer->email,
            $address->company,
            $country->iso_code,
            null,
            $phone,
            $machineCode,
            null,
            $machineType

        );
        return $receiver;
    }

    private function __getParcel(Order $order)
    {
        $weight = $order->getTotalWeight();
        $parcel = new FurgonetkaSoapParcel($weight);
        return $parcel;

    }
}
