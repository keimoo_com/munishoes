<?php

namespace PrestaShopBundle\Command;

use Configuration;
use Context;
use Db;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Hook;
use Image;
use ImageManager;
use ImageType;
use Language;
use PrestaShop\PrestaShop\Adapter\Entity\SpecificPrice;
use PrestaShopException;
use Product;
use StockAvailable;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Response;
use Tools;

final class ShoploImportCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Connection
     */
    private $conn;
    private $output;
    private $input;

    protected function configure()
    {
        // The name of the command (the part after "bin/console")
        $this
            ->setName('shoplo:import')
            ->setDescription('Shoplo import')
            ->addArgument('idProducts', InputArgument::REQUIRED, 'Id produktu z shopera (wiecej wprowadz po przecinku)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initDb();

        $this->output = $output;
        $this->input = $input;

        $productIds = explode(',', $input->getArgument('idProducts'));

        $outputStyle = new OutputFormatterStyle('blue', null);
        $this->output->getFormatter()->setStyle('blue', $outputStyle);

        foreach ($productIds as $id) {
            $this->output->writeln(sprintf('<fg=green>Start migration product %d!</>', $id));

            try {
                $this->runMigration($id);
            } catch (\Exception $exception) {
                $this->output->writeln(sprintf('<error>%s</error>', $exception->getMessage()));

                $file = __DIR__ . "/../../../csv_import_file/error_migration.txt";
                $error = sprintf('Error product %s, message: %s', $id, $exception->getMessage());
                file_put_contents($file, $error, FILE_APPEND | LOCK_EX);
                continue;
            }
        }

        // Here your business logic.
        $this->output->writeln('<info>All Export done!</info>');
    }


    /**
     * @throws \Exception
     * @throws DBALException
     */
    private function makeProduct(
        string $uuid,
        string $title,
        string $description,
        string $shortDescription,
        string $seoTitle,
        string $seoDescription,
        string $seoKeywords,
        string $manufacture,
        array  $categories,
        int    $parentId,
        string $price,
        int    $stockAll,
        array  $specialOffer,
        string $weight,
        string $permalink,
        array  $children,
        array  $urlImages,
        string $seoUrl
    ): void
    {

        ///https://github.com/ondrejd/fasardixml/blob/master/import.php
        $product = new Product();
        $product->force_id = false;

        $product->name = $this->create_multi_language_field($title);
        $product->description = $description;
        $product->reference = $uuid; //index
        $product->meta_title = $seoTitle;
        $product->meta_description = $seoDescription;
        $product->meta_keywords = $seoKeywords;
        $product->link_rewrite = $this->getSeoFriendlyLink($permalink, $seoUrl);

        $product->id_category_default = $parentId;
        $product->category = array_merge($categories, [2]);

        if (empty($categories)) {
            $this->output->writeln(sprintf('<fg=red> >> Error root categories not found %s</>', $uuid));
        }

//        $product->description_short = $this->create_multi_language_field($shortDescription);

        $product->price = $price;
        $product->id_manufacturer = $this->getIdManufactureByName($manufacture);
        $product->id_tax_rules_group = 0; //1
        $product->id_supplier = 1;
        $product->quantity = $stockAll;
        $product->minimal_quantity = 1;
        $product->additional_shipping_cost = 0;
        $product->wholesale_price = 0;
        $product->ecotax = 0;
        $product->width = 0;
        $product->height = 0;
        $product->depth = 0;
        $product->weight = $weight;
        $product->active = $stockAll > 0 ? 1 : 0;
        $product->new = false;
//        $product->date_add = '2022-01-17 22:38:03';
//        $product->date_upd = '2022-01-17 22:38:03';

        $product->available_for_order = 1;
        $product->show_price = 1;
        $product->on_sale = array_key_exists('promo_id', $specialOffer) ? 1 : 0;
        $product->online_only = 0;


        if (count($children) > 0) {
            $product->product_type = 'combinations';
        }

        $product->save();


        $this->updateDateAdd($product->id);

        if (array_key_exists('promo_id', $specialOffer)) {
            $this->addSpecificPrice(
                $product->id,
                0,
                $specialOffer['discount'],
                'amount'
            );
        }


        $this->output->writeln('<fg=blue> >>>>> Update categories</>');
        $product->updateCategories($product->category, true);

        foreach ($urlImages as $url) {
            try {
                $image = new Image();
                $image->id_product = (int)$product->id;
                $image->position = Image::getHighestPosition($product->id) + $url['order'];
                $image->cover = count($urlImages) === 1 ? true : $url['main'];
                $image->add();

                if (!$this->copyImg($product->id, $image->id, $url['url'], 'products', !Tools::getValue('regenerate'))) {
                    $image->delete();
                }

                $this->output->writeln('<fg=blue> >> Add image </>');
            } catch (\Exception $exception) {
                $this->output->writeln(sprintf('<fg=red> >> Error add image %s</>', $url['url']));
                continue;

            }


        }

        $defaultSet = false;

        if (count($children) > 0) {
            foreach ($children as $child) {

                $default = false;

                if ($child['stock'] > 0 && $defaultSet === false) {
                    $default = $child['stock'];
                    $defaultSet = true;
                }


                $productAttributeId = $product->addAttribute(
                    $child['price'],
                    0,
                    0,
                    0,
                    [],
                    '',
                    $child['code'],
                    $default,
                    null,
                    null,
                    1,
                    [],
                    null,
                    $child['stock'],
                    '',
                    null,
                    false,
                    null
                );

                $attributeSizeId = $this->getAttributeIdByValue($child['size']);

                Db::getInstance()->insert('product_attribute_combination',
                    [
                        'id_product_attribute' => $productAttributeId,
                        'id_attribute' => (int)$attributeSizeId,
                    ]
                );

                $this->output->writeln('<fg=blue> >> Create product variant  </>');

                try {
                    StockAvailable::setQuantity($product->id, $productAttributeId, $child['stock']);
                    $this->output->writeln('<fg=blue> >>>> Update stock variants  </>');

                } catch (Exception $exception) {
                    echo $exception->getMessage();
                    continue;
                }


            }
        }
    }

    private function getNettoPrice($priceBrutto)
    {

        $vat = 23;
        $vatDivisor = 1 + ($vat / 100);

        $price = $priceBrutto;
        $priceBeforeVat = $price / $vatDivisor;

        $vatAmount = $price - $priceBeforeVat;

        //Print out the price before VAT.
        echo number_format($priceBeforeVat, 2), '<br>';

        //Print out how much of the gross price was VAT.
        echo 'VAT @ ' . $vat . '% - ' . number_format($vatAmount, 2), '<br>';

        //Print out the gross price.
        echo $price;
    }



    public function getAttributeIdByValue(string $value): int
    {
        $stm = $this->conn->prepare(
            '
                SELECT al.`id_attribute` FROM `ps_attribute_lang` al
                JOIN ps_attribute a ON al.`id_attribute` = a.`id_attribute` 
                WHERE al.`name` LIKE :VALUE
                AND a.id_attribute_group = 1
            '
        );

        $stm->bindValue('VALUE', $value);
        $stm->execute();
        return (int)$stm->fetchColumn();

    }

    public function downloadFromCSV(): Response
    {
        $imageDownload = [];

        try {
            $handle = fopen(__DIR__ . "/../../../csv_import_file/products_part_1.min.csv", "r");
            $count = 0;
            $columnPositionImage = ['image1' => 15, 'image2' => 17, 'image3' => 19, 'image4' => 40, 'image5' => 42];

            while (($data = fgetcsv($handle, 3500, ';')) !== FALSE) {
                if ($count === 0) {
                    $count++;
                    continue;
                }

                //https://cdn.shoplo.com/8444/products/orig/aaak/14784-43a9b917-110e-46d1-9ce7-44d1c06935b6.jpeg
                foreach ($columnPositionImage as $key => $value) {
                    if (!empty($data[$value])) {
                        $image = $data[$value];
                        self::downloadImages($image);
                        $imageDownload[] = $image;
                    }

                }
                $count++;
            }

            return new Response(json_encode($imageDownload));
        } catch (Exception $exception) {
            print_r($exception->getMessage());
            return new Response(json_encode($imageDownload));
        }

    }

    public static function downloadImages(string $url)
    {
        $images = str_split($url, strrpos($url, '/'));
        $imageName = $images[1];
        if (isset($images[2])) {
            $imageName = $imageName . $images[2];
        }

        $content = file_get_contents($url);
        file_put_contents(__DIR__ . '/../../public/images/' . $imageName, $content);
    }

    /**
     * @throws DBALException
     */
    private function getParentCategoryIdByName(string $parentName): int
    {
        $stm = $this->conn->prepare(
            '
                SELECT cl.id_category
                FROM ps_category_lang cl
                WHERE cl.name = :PARENT_NAME 
            '
        );

        $stm->bindValue('PARENT_NAME', $parentName);
        $stm->execute();
        return (int)$stm->fetchColumn();
    }

    /**
     * @throws DBALException
     */
    private function getCategoryIdByName(string $categoryName, int $parentId): int
    {
        $stm = $this->conn->prepare(
            '
                SELECT  c.id_category
                FROM `ps_category`  c
                JOIN ps_category_lang cl ON cl.id_category = c.id_category
                WHERE c.id_parent = :PARENT_ID
                AND cl.name = :CATEGORY_NAME
            '
        );

        $stm->bindValue('PARENT_ID', $parentId);
        $stm->bindValue('CATEGORY_NAME', str_replace(' ', '', $categoryName));
        $stm->execute();

        return (int)$stm->fetchColumn();
    }

    /**
     * @throws DBALException
     * @throws Exception
     */
    private function getIdManufactureByName(string $manufacture): int
    {
        $stm = $this->conn->prepare(
            '
              SELECT id_manufacturer
              FROM `ps_manufacturer` 
              WHERE `name` = :NAME
        '
        );

        $stm->bindValue('NAME', $manufacture);

        $stm->execute();

        $result = $stm->fetchColumn();
        if (empty($result)) {
            throw new Exception('Producer %s not found. Change It!', $manufacture);
        }

        return (int)$result;
    }

    /**
     * Creates multi-language field.
     *
     * @param string $field
     * @return array
     * @internal Use {@see create_language_field} instead.
     *
     * @since 1.0.0
     *
     */
    protected function create_multi_language_field($field)
    {
        $languages = Language::getLanguages();
        $res = [];

        foreach ($languages as $lang) {
            $res[$lang['id_lang']] = $field;
        }

        return $res;
    }

    private function getSeoFriendlyLink(string $permalink, string $seoUrl): string
    {
        if (strpos($permalink, '/pl/p')) {
            $slug = trim($permalink, "https://www.munishoes.pl/pl/p/");
            $slugArray = str_split($slug, strrpos($slug, '/'));

            $file = __DIR__ . "/../../../csv_import_file/htaccess-munishoes.txt";

            $rule = sprintf("RewriteRule ^pl/p/%s(\/.*)?$ %s [R=301,L]".PHP_EOL, $slug, 'https://www.munishoes.pl/'.$slugArray[0]);
            file_put_contents($file, $rule, FILE_APPEND | LOCK_EX);

            return $slugArray[0];
        }

        return $seoUrl;
    }

    /**
     * @throws DBALException
     */
    private function getCategories(array $cat): array
    {
        $shoperApi = new ShoperClientApi();
        $treeCategories = $shoperApi->findRootCategories($cat);

        $categories = [];

        $resultParent = [];
        foreach ($treeCategories as $category) {
            $parentId = $this->getParentCategoryIdByName($category['rootNameCategory']);
            $childrenId = $this->getCategoryIdByName($category['name'], $parentId);

            $categories[] = $childrenId;

            if (in_array($parentId, $categories, true) === false) {
                $categories[] = $parentId;
                $resultParent[] = $parentId;
            }
        }

        return [
            'all' => $categories,
            'parents' => $resultParent
        ];
    }

    /**
     * @throws \Exception
     * @throws DBALException
     */
    private function runMigration(int $id): void
    {
        $shoperApi = new ShoperClientApi();
        $product = $shoperApi->getProductById($id);

        if ($this->productExists($product['code'])) {
            throw new \Exception(sprintf('Product %s exists.', $product['code']));
        }

        $variant = [];
        foreach ($product['options'] as $option) {
            $variant[] = $shoperApi->getVariant($option);
        }

        $images = $shoperApi->getImagesProducts($id);
        $urlImages = [];
        foreach ($images as $image) {
            $search = ['(', ')', '.jpg', '.png', '.jpeg'];
            $name = str_replace($search, '', $image['name']);
            $url = sprintf('https://www.munishoes.pl/userdata/public/gfx/%d/%s.%s', $image['gfxId'], str_replace(' ', '-', $name), $image['extension']);
            $urlImages[] = [
                'url' => $url,
                'main' => $image['main'],
                'order' => $image['order']
            ];
        }

        $categories = $this->getCategories($product['categories']);

        $this->makeProduct(
            $product['code'],
            $product['translations']['pl_PL']['name'],
            $product['translations']['pl_PL']['description'],
            $product['translations']['pl_PL']['short_description'],
            $product['translations']['pl_PL']['seo_title'],
            $product['translations']['pl_PL']['seo_description'],
            $product['translations']['pl_PL']['seo_keywords'],
            $shoperApi->getProducerNameById((int)$product['producer_id']),
            $categories['all'],
            end($categories['parents']),
            $product['stock']['price'],
            (int)$product['stock']['stock'],
            array_key_exists('special_offer', $product) ? $product['special_offer'] : [],
            $product['stock']['weight'],
            $product['translations']['pl_PL']['permalink'],
            $variant,
            $urlImages,
            $product['translations']['pl_PL']['seo_url'],
        );

    }

    private function initDb(): void
    {
        $container = $this->getContainer();
        $this->em = $container->get('doctrine')->getManager();
        $this->conn = $this->em->getConnection();
        $this->conn->beginTransaction();
    }

    private function copyImg($id_entity, $id_image = null, $url, $entity = 'products', $regenerate = true)
    {
        $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
        $watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));
        switch ($entity) {
            default:
            case 'products':
                $image_obj = new Image($id_image);
                $path = $image_obj->getPathForCreation();
                break;
            case 'categories':
                $path = _PS_CAT_IMG_DIR_ . (int)$id_entity;
                break;
            case 'manufacturers':
                $path = _PS_MANU_IMG_DIR_ . (int)$id_entity;
                break;
            case 'suppliers':
                $path = _PS_SUPP_IMG_DIR_ . (int)$id_entity;
                break;
        }
        $url = urldecode(trim($url));
        $parced_url = parse_url($url);
        if (isset($parced_url['path'])) {
            $uri = ltrim($parced_url['path'], '/');
            $parts = explode('/', $uri);
            foreach ($parts as &$part) {
                $part = rawurlencode($part);
            }
            unset($part);
            $parced_url['path'] = '/' . implode('/', $parts);
        }
        if (isset($parced_url['query'])) {
            $query_parts = [];
            parse_str($parced_url['query'], $query_parts);
            $parced_url['query'] = http_build_query($query_parts);
        }
        if (!function_exists('http_build_url')) {
            require_once(_PS_TOOL_DIR_ . 'http_build_url/http_build_url.php');
        }
        $url = http_build_url('', $parced_url);
        $orig_tmpfile = $tmpfile;
        if (Tools::copy($url, $tmpfile)) {
// Evaluate the memory required to resize the image: if it's too much, you can't resize it.
            if (!ImageManager::checkImageMemoryLimit($tmpfile)) {
                @unlink($tmpfile);
                return false;
            }
            $tgt_width = $tgt_height = 0;
            $src_width = $src_height = 0;
            $error = 0;
            ImageManager::resize($tmpfile, $path . '.jpg', null, null, 'jpg', false, $error, $tgt_width, $tgt_height, 5,
                $src_width, $src_height);
            $images_types = ImageType::getImagesTypes($entity, true);
            if ($regenerate) {
                $previous_path = null;
                $path_infos = [];
                $path_infos[] = [$tgt_width, $tgt_height, $path . '.jpg'];
                foreach ($images_types as $image_type) {
                    $tmpfile = $this->get_best_path($image_type['width'], $image_type['height'], $path_infos);
                    if (ImageManager::resize($tmpfile, $path . '-' . stripslashes($image_type['name']) . '.jpg', $image_type['width'],
                        $image_type['height'], 'jpg', false, $error, $tgt_width, $tgt_height, 5,
                        $src_width, $src_height)) {
// the last image should not be added in the candidate list if it's bigger than the original image
                        if ($tgt_width <= $src_width && $tgt_height <= $src_height) {
                            $path_infos[] = [$tgt_width, $tgt_height, $path . '-' . stripslashes($image_type['name']) . '.jpg'];
                        }
                        if ($entity == 'products') {
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int)$id_entity . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int)$id_entity . '.jpg');
                            }
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int)$id_entity . '_' . (int)Context::getContext()->shop->id . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int)$id_entity . '_' . (int)Context::getContext()->shop->id . '.jpg');
                            }
                        }
                    }
                    if (in_array($image_type['id_image_type'], $watermark_types)) {
                        Hook::exec('actionWatermark', ['id_image' => $id_image, 'id_product' => $id_entity]);
                    }
                }
            }
        } else {
            @unlink($orig_tmpfile);
            return false;
        }
        unlink($orig_tmpfile);
        return true;
    }

    private function get_best_path($tgt_width, $tgt_height, $path_infos)
    {
        $path_infos = array_reverse($path_infos);
        $path = '';
        foreach ($path_infos as $path_info) {
            [$width, $height, $path] = $path_info;
            if ($width >= $tgt_width && $height >= $tgt_height) {
                return $path;
            }
        }
        return $path;
    }

    private function addSpecificPrice(
        $id_product,
        $id_product_attribute = 0,
        $reduction_value = 0.00,
        $reduction_type = 'percentage',
        $from_date = '0000-00-00 00:00:00',
        $to_date = '0000-00-00 00:00:00'
    ): void
    {

        $specificPrice = new SpecificPrice();
        $specificPrice->id_product = (int)$id_product;
        $specificPrice->id_product_attribute = (int)$id_product_attribute; // if 0 then for all attributes

        $specificPrice->id_shop = 1;
        $specificPrice->id_currency = 0;
        $specificPrice->id_country = 0; // PL - 14
        $specificPrice->id_group = 0;
        $specificPrice->id_customer = 0;
        $specificPrice->from_quantity = 1;

        $specificPrice->reduction = $reduction_value; // 7% is 0.07
        $specificPrice->reduction_type = $reduction_type;
//        $specificPrice->reduction_tax = $tax;
        $specificPrice->price = '-1';
        $specificPrice->from = $from_date;
        $specificPrice->to = $to_date;

        $specificPrice->add();
        $specificPrice->save();

//        return $specificPrice->id;
    }


    private function productExists(string $code): bool
    {
        $stm = $this->conn->prepare(
            '
                SELECT  p.id_product
                FROM `ps_product`  p
                WHERE p.reference = :CODE        
            '
        );

        $stm->bindValue('CODE', $code);
        $stm->execute();

        $result = $stm->fetchColumn();

        if (empty($result)) {
            return false;
        }

        return true;
    }

    private function updateDateAdd(int $id): void
    {
        $sql = 'UPDATE ' . _DB_PREFIX_ . 'product
                    SET date_add = "2022-01-01 20:00:00"
                    WHERE id_product = ' . $id;

        Db::getInstance()->execute($sql);

        $sql2 = 'UPDATE ' . _DB_PREFIX_ . 'product_shop
                    SET date_add = "2022-01-01 20:00:00"
                    WHERE id_product = ' . $id;

        Db::getInstance()->execute($sql2);



    }
}
