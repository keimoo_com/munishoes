<?php
/**
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*/

class PdGoogleAnalytycEnhancedEcommerceProAjaxModuleFrontController extends ModuleFrontController
{
    private $name = '';

    public function initContent()
    {
        $this->ajax = true;
        parent::initContent();
        $this->name = 'pdgoogleanalytycenhancedecommercepro';
    }

    public function displayAjax()
    {
        $module = new PdGoogleAnalytycEnhancedEcommercePro();

        if (Tools::getValue('secure_key') == $module->secure_key) {

            $id_product = (int)Tools::getValue('product_id');
            $id_lang = (int)Context::getContext()->language->id;
            $id_shop = (int)Context::getContext()->shop->id;
            $currency_iso = (string)$this->context->currency->iso_code;
            $action = (string)Tools::getValue('action');

            if ($action == 'updateCart') {

                $id_product_attribute = Tools::getValue('product_id_product_attribute');
                
                $product = new Product((int)$id_product, false, $id_lang);
                $variant = '';
                $attribute_combination_resume = false;
                if ($id_product_attribute) {
                    $attribute_combination_resume = $product->getAttributeCombinationsById($id_product_attribute, $id_lang, true);
                    if ($attribute_combination_resume) {
                        foreach ($attribute_combination_resume as  $acr) {
                            $variant .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                        }
                        $variant = mb_substr($variant, 0, -3);
                    }
                }
                if (!empty($variant)) {
                    $product_name = addslashes($product->name.' ('.$variant.')');
                } else {
                    $product_name = addslashes($product->name);
                }

                $price = Product::getPriceStatic($id_product, true, $id_product_attribute, 6, null, false, true);
                $content_category = new Category($product->id_category_default, $id_lang, $id_shop);
                $content_ids = $module->getProductIdStringByType($product, $id_product_attribute);
                $data = array(
                    'content_ids' => $content_ids,
                    'content_category' => addslashes($content_category->name),
                    'content_name' => $product_name,
                    'content_value' => Tools::ps_round($price, 2),
                    'content_variant' => $variant,
                    'content_manufacturer' => $product->id_manufacturer ? addslashes(Manufacturer::getNameById($product->id_manufacturer)) : '',
                    'currency' => $currency_iso,
                );
                die(Tools::jsonEncode($data));

            } elseif ($action == 'productClick') {

                $id_product_attribute = Tools::getValue('product_id_product_attribute');
                $product = new Product((int)$id_product, false, $id_lang);
                $variant = '';
                $attribute_combination_resume = false;
                if ($id_product_attribute) {
                    $attribute_combination_resume = $product->getAttributeCombinationsById($id_product_attribute, $id_lang, true);
                    if ($attribute_combination_resume) {
                        foreach ($attribute_combination_resume as  $acr) {
                            $variant .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                        }
                        $variant = mb_substr($variant, 0, -3);
                    }
                }
                if (!empty($variant)) {
                    $product_name = addslashes($product->name.' ('.$variant.')');
                } else {
                    $product_name = addslashes($product->name);
                }

                $price = Product::getPriceStatic($id_product, true, $id_product_attribute, 6, null, false, true);
                $content_category = new Category($product->id_category_default, $id_lang, $id_shop);
                $content_ids = $module->getProductIdStringByType($product, $id_product_attribute);

                $data = array(
                    'content_ids' => $content_ids,
                    'content_category' => addslashes($content_category->name),
                    'content_name' => $product_name,
                    'content_value' => Tools::ps_round($price, 2),
                    'content_variant' => $variant,
                    'content_manufacturer' => $product->id_manufacturer ? addslashes(Manufacturer::getNameById($product->id_manufacturer)) : '',
                );
                die(Tools::jsonEncode($data));

            } elseif ($action == 'updateProduct') {

                $groups = Tools::getValue('attributes_groups');
                $id_product_attribute = (int)Product::getIdProductAttributeByIdAttributes((int)$id_product, $groups);
                $product = new Product((int)$id_product, false, $id_lang);
                $price = Product::getPriceStatic($id_product, true, $id_product_attribute, 2);
                $content_category = new Category($product->id_category_default, $id_lang, $id_shop);
                $content_ids = $module->getProductIdStringByType($product, $id_product_attribute);

                $variant = '';
                $attribute_combination_resume = false;
                if ($id_product_attribute) {
                    $attribute_combination_resume = $product->getAttributeCombinationsById($id_product_attribute, $id_lang, true);
                    if ($attribute_combination_resume) {
                        foreach ($attribute_combination_resume as  $acr) {
                            $variant .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                        }
                        $variant = mb_substr($variant, 0, -3);
                    }
                }

                if (!empty($variant)) {
                    $product_name = addslashes($product->name.' ('.$variant.')');
                } else {
                    $product_name = addslashes($product->name);
                }

                $price = Product::getPriceStatic($id_product, true, $id_product_attribute, 6, null, false, true);

                $data = array(
                    'content_ids' => $content_ids,
                    'content_category' => addslashes($content_category->name),
                    'content_name' => $product_name,
                    'content_value' => Tools::ps_round($price, 2),
                    'content_variant' => $variant,
                    'content_manufacturer' => $product->id_manufacturer ? addslashes(Manufacturer::getNameById($product->id_manufacturer)) : '',
                );
                die(Tools::jsonEncode($data));

            } elseif ($action == 'addDeliveryInfo') {

                $cart = Context::getContext()->cart;
                if (!($cart instanceof Cart)) {
                    return;
                }
                if (isset($cart->id)) {
                    if ($id_carrier = Tools::getValue('id_carrier')) {
                        $carriers = $module->getCarriersArray();
                        $carrier_name = $carriers[$id_carrier];
                    }

                    $this->context->smarty->assign(array(
                        'carrier_name' => $carrier_name,
                    ));
                    $html = $this->context->smarty->fetch(_PS_MODULE_DIR_.$this->name.'/views/templates/hook/addDeliveryInfo.tpl');
                    die(Tools::jsonEncode($html));
                }

            } elseif ($action == 'addPaymentInfo') {

                $cart = Context::getContext()->cart;
                if (!($cart instanceof Cart)) {
                    return;
                }
                if (isset($cart->id)) {

                    if ($payment_module = Tools::getValue('payment_module')) {
                        $payment_module = preg_replace("/[^A-Za-z.!?_-]/",'', $payment_module);
                        if (($payment_module_instance = Module::getInstanceByName($payment_module))) {
                            $payment_name = $payment_module_instance->displayName;
                        }
                    }

                    $this->context->smarty->assign(array('payment_name' => $payment_name));
                    $html = $this->context->smarty->fetch(_PS_MODULE_DIR_.$this->name.'/views/templates/hook/addPaymentInfo.tpl');
                    die(Tools::jsonEncode($html));
                }

            } elseif ($action == 'addPersonalInformtions') {

                $cart = Context::getContext()->cart;
                if (!($cart instanceof Cart)) {
                    return;
                }
                if (isset($cart->id)) {
                    $html = $this->context->smarty->fetch(_PS_MODULE_DIR_.$this->name.'/views/templates/hook/addPersonalInformtions.tpl');
                    die(Tools::jsonEncode($html));
                }

            } elseif ($action == 'addAddress') {

                $cart = Context::getContext()->cart;
                if (!($cart instanceof Cart)) {
                    return;
                }
                if (isset($cart->id)) {
                    $html = $this->context->smarty->fetch(_PS_MODULE_DIR_.$this->name.'/views/templates/hook/addAddress.tpl');
                    die(Tools::jsonEncode($html));
                }
            
            }
        }
    }
}
