{*
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*}

<!-- START > PD Google Analytycs Enhanced Ecommerce trought Tag Manager > DataLayer 1.7.x Module > checkoutOption -->
<script>
	console.log('Data layer push event: checkoutOption > Add personal informations step');
	dataLayer.push({ ecommerce: null });  // Clear the previous ecommerce object.
	dataLayer.push({
		'event': 'checkoutOption',
		'ecommerce': {
			'checkout_option': {
				'actionField': {
					'step': 3, 
					'option': 'Add personal informations step'
				}
			}
		}
	});
	dataLayer.push({
	    'event': 'checkout',
	    'ecommerce': {
	      	'checkout': {
	        	'actionField': {
	        		'step': 3, 
	        		'option': 'Add personal informations step'
	        	}
	     	}
	   	}
	});

</script>
<!-- END > PD Google Analytycs Enhanced Ecommerce trought Tag Manager > DataLayer 1.7.x Module > checkoutOption -->
