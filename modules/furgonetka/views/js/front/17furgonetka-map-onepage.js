$( document ).ready(function(){
    if(typeof Review === 'object'){
        Review.parentPlaceOrder = Review.placeOrder;
        Review.placeOrder = function(params){
           validate = validateCarriers();
            if (true !== validate) {
                Fronted.showModal({type: 'warning', message: validate});
                return false;
            }
            Review.parentPlaceOrder(params);
        }
    }
});

$(document).on('click', '#furgonetka-set-point', function(e){
    e.preventDefault();
    showMap()
});

$(document).on('opc-load-carrier:completed', function(){
    buttonManage();
});

function validateCarriers(){
        var id_delivery = getIdDelivery();
        var validate = true;
        if (!$('#furgonetka-machine-' + id_delivery).length){
            $.ajax({
                type: 'POST',
                url: furgonetkaCheckMapAjax,
                async: false,
                dataType: 'json',
                data: {id_delivery : id_delivery},
                success: function(jsonData) {
                    if(jsonData.status == 'error'){
                        validate = jsonData.message
                    }else{
                        validate = true;
                    }
                },
            });
        }
        return validate;
}

function getIdDelivery(){
    var id_delivery = parseInt($('input.delivery_option_radio:checked').val());
    if('NaN' == id_delivery){
        return 0;
    }
    return id_delivery;
}

function buttonManage(){
     id_delivery = getIdDelivery();
    var button = $('#furgonetka-set-point');
    if (!$('#furgonetka-delivery-' + id_delivery).length) {
        button.hide();
    }else{
        button.show();
    }
    showMachine();
}

function showMap(){
    var id_delivery = getIdDelivery();
    var service = $('#furgonetka-delivery-' + id_delivery).val().split(',');
    // var city = $('#furgonetka-city').val();
    var city = $('#delivery_city').val()
    var street = $('#delivery_address1').val()
    if(typeof FurgonetkaBiznesMap === 'function'){
        new FurgonetkaBiznesMap({service: service,city: city, street: street, 'callback': furgonetkaMapCallback}).view();
    }
}

function showMachine(){
    var id_delivery = getIdDelivery();
    $('.furgonetka-machine').hide();
    var machine_name = $('#furgonetka-machine-' + id_delivery);
    if (machine_name.length) {
        machine_name.show();
    }

}

function furgonetkaMapCallback(response){
    var id_delivery = getIdDelivery();
    $.ajax({
        type: 'POST',
        url: furgonetkaMapAjax,
        async: true,
        dataType: 'json',
        data: {id_delivery : id_delivery, machine_code: response.code, machine_name : response.name, machine_type: response.type},
        success: function(jsonData) {
            if(jsonData.status == 'error'){
                jAlert(jsonData.message)
            }else{
                var input = '<input disabled class="furgonetka-machine" id="furgonetka-machine-' + id_delivery +'" value="'+ response.name +'">'
                $('#furgonetka-machine').html(input);
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
        }
    });
}
