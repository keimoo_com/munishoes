{*
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*}

<!-- START > PD Google Analytycs Enhanced Ecommerce trought Tag Manager > DataLayer 1.7.x Module > BO HEADER-->
{if !empty($pd_gaeep_tm_id) && !empty($pd_gaeep_a_id)}
	<script async src="https://www.googletagmanager.com/gtag/js?id={$pd_gaeep_a_id nofilter}"></script>
	{literal}
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', '{/literal}{$pd_gaeep_a_id nofilter}{literal}', {'send_page_view': true});
	 	gtag('set', {currency: '{/literal}{$pd_gaeep_currency nofilter}{literal}'});
		gtag('set', {country: '{/literal}{$pd_gaeep_country nofilter}{literal}'});
	</script>
	{/literal}

	{literal}
	<script>
		(function (w, d, s, l, i) {
	       	w[l] = w[l] || [];
	        w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
	        var f = d.getElementsByTagName(s)[0];
	        var j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
	        j.async = true;
	        j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
	        f.parentNode.insertBefore(j, f);
	    })(window, document, 'script', 'dataLayer', '{/literal}{$pd_gaeep_tm_id nofilter}{literal}');
	</script>
	{/literal}
{/if}
<!-- END > PD Google Analytycs Enhanced Ecommerce trought Tag Manager > DataLayer 1.7.x Module > BO HEADER -->
