/*
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*/

$(document).ready(function() {

	$('body').on("click", ".product-miniature a", function(e) {
		e.preventDefault();
		let product_container = $(this).closest('.product-miniature'),
			product_url = $(this).attr('href').trim(),
			product_id = product_container.attr('data-id-product'),
			product_id_product_attribute = product_container.attr('data-id-product-attribute');

		setTimeout(function () {
			document.location = product_url;
		}, 500);

		if (typeof(product_id) !== 'undefined' && typeof(product_id_product_attribute) !== 'undefined') {
			$.ajax({
				type: "POST",
				url: pdgoogleanalytycenhancedecommercepro_ajax_link,
				data: {
					'action': 'productClick', 
					'product_id': product_id, 
					'product_id_product_attribute' : product_id_product_attribute, 
					'secure_key':  pdgoogleanalytycenhancedecommercepro_secure_key, 
					'ajax' : true
				},
				dataType: "json",
				success: function(data) {
					console.log('Data layer push event: productClick > Products lists pages');
					dataLayer.push({ ecommerce: null });
					dataLayer.push({
						'event': 'productClick',
						'ecommerce': {
								'click': {
								'actionField': {'list': 'Category'},
								'products': [{
									'name': data.content_name,
									'id': data.content_ids,
									'price': data.content_value,
									'brand': data.content_manufacturer,
									'category': data.content_category,
									'variant': data.content_variant,
									'position': 1
								}]
							 }
						},
						'eventCallback': function() {
							document.location = product_url
						}
					});
				}
			});
		}
	});

	// Transformer theme product click
	$('body').on("click", "article.js-product-miniature", function(e) {
		e.preventDefault();
		
		let product_container = $(this),
			product_container_a = product_container.find('.pro_first_box a.product_img_link'),
			product_url = product_container_a.attr('href').trim(),
			product_id = product_container.attr('data-id-product'),
			product_id_product_attribute = product_container.attr('data-id-product-attribute');

		setTimeout(function () {
			document.location = product_url;
		}, 500);

		if (typeof(product_id) !== 'undefined' && typeof(product_id_product_attribute) !== 'undefined') {
			$.ajax({
				type: "POST",
				url: pdgoogleanalytycenhancedecommercepro_ajax_link,
				data: {
					'action': 'productClick', 
					'product_id': product_id, 
					'product_id_product_attribute' : product_id_product_attribute, 
					'secure_key':  pdgoogleanalytycenhancedecommercepro_secure_key, 
					'ajax' : true
				},
				dataType: "json",
				success: function(data) {
					console.log('Data layer push event: productClick > Products lists pages');
					dataLayer.push({ ecommerce: null });
					dataLayer.push({
						'event': 'productClick',
						'ecommerce': {
								'click': {
								'actionField': {'list': 'Category'},
								'products': [{
									'name': data.content_name,
									'id': data.content_ids,
									'price': data.content_value,
									'brand': data.content_manufacturer,
									'category': data.content_category,
									'variant': data.content_variant,
									'position': 1
								}]
							 }
						},
						'eventCallback': function() {
							document.location = product_url
						}
					});
				}
			});
		}
	});

	// Transformer theme add to cart butons on product lists
	$('body').on("click", ".ajax_add_to_cart_button", function(e) {
		console.log('Click add');
		let a_button = $(this),
			id_product = parseInt(a_button.attr('data-id-product')),
			id_product_attribute = parseInt(a_button.attr('data-id-product-attribute')),
			iso_code = prestashop.currency.iso_code;

			console.log(id_product);
			console.log(id_product_attribute);

		if (typeof(id_product) !== 'undefined' && typeof(id_product_attribute) !== 'undefined') {
			$.ajax({
				type: "POST",
				url: pdgoogleanalytycenhancedecommercepro_ajax_link,
				data: {
					'action': 'productClick', 
					'product_id': id_product, 
					'product_id_product_attribute' : id_product_attribute, 
					'secure_key':  pdgoogleanalytycenhancedecommercepro_secure_key, 
					'ajax' : true
				},
				dataType: "json",
				success: function(data) {
					console.log('Data layer push event: addToCart > Cart increcase quantity button');
					dataLayer.push({ ecommerce: null });
					dataLayer.push({
						'event': 'addToCart',
						'ecommerce': {
							'currencyCode': iso_code,
							'add': {
								'products': [{
									'name': data.content_name,
									'id': data.content_ids,
									'price': data.content_value,
									'brand': data.content_manufacturer,
									'category': data.content_category,
									'variant': data.content_variant,
									'quantity': 1
								}]
							}
						}
					});

					if (pdgoogleanalytycenhancedecommercepro_gdr_enabled) {
						console.log('Data layer push GDR event: add_to_cart > Product add to cart button');
						dataLayer.push({
							'event': 'add_to_cart',
							'value': data.content_value,
							'items' : [{
								'id': data.content_ids,
								'google_business_vertical': 'retail'
							}]
						});
					}
				}
			});
		}
	});

	// qty up > thecheckout module opc
	$('body').on("click", "a.cart-line-product-quantity-up", function(e) {

		let qty_input = $(this).parent().find('input.cart-line-product-quantity'),
			updat_url = qty_input.attr('data-update-url'),
			url_params = parseQuery(updat_url),
			iso_code = prestashop.currency.iso_code;

		if (typeof(url_params.id_product) !== 'undefined' && typeof(url_params.id_product_attribute) !== 'undefined') {
			var product_id_product = url_params.id_product;
			var product_id_product_attribute = url_params.id_product_attribute;
		}

		if (typeof(product_id_product) !== 'undefined' && typeof(product_id_product_attribute) !== 'undefined') {
			$.ajax({
				type: "POST",
				url: pdgoogleanalytycenhancedecommercepro_ajax_link,
				data: {
					'action': 'productClick', 
					'product_id': product_id_product, 
					'product_id_product_attribute' : product_id_product_attribute, 
					'secure_key':  pdgoogleanalytycenhancedecommercepro_secure_key, 
					'ajax' : true
				},
				dataType: "json",
				success: function(data) {
					console.log('Data layer push event: addToCart > Cart increcase quantity button');
					dataLayer.push({ ecommerce: null });
					dataLayer.push({
						'event': 'addToCart',
						'ecommerce': {
							'currencyCode': iso_code,
							'add': {
								'products': [{
									'name': data.content_name,
									'id': data.content_ids,
									'price': data.content_value,
									'brand': data.content_manufacturer,
									'category': data.content_category,
									'variant': data.content_variant,
									'quantity': 1
								}]
							}
						}
					});

					if (pdgoogleanalytycenhancedecommercepro_gdr_enabled) {
						console.log('Data layer push GDR event: add_to_cart > Product add to cart button');
						dataLayer.push({
							'event': 'add_to_cart',
							'value': data.content_value,
							'items' : [{
								'id': data.content_ids,
								'google_business_vertical': 'retail'
							}]
						});
					}
				}
			});
		}
	});

	// qty down > stadard theme ps 17
	$('body').on("click", "a.cart-line-product-quantity-down", function(e) {

		let qty_input = $(this).parent().find('input.cart-line-product-quantity'),
			updat_url = qty_input.attr('data-update-url'),
			url_params = parseQuery(updat_url),
			iso_code = prestashop.currency.iso_code;

		if (typeof(url_params.id_product) !== 'undefined' && typeof(url_params.id_product_attribute) !== 'undefined') {
			var product_id_product = url_params.id_product;
			var product_id_product_attribute = url_params.id_product_attribute;
		}

		if (typeof(product_id_product) !== 'undefined' && typeof(product_id_product_attribute) !== 'undefined') {
			$.ajax({
				type: "POST",
				url: pdgoogleanalytycenhancedecommercepro_ajax_link,
				data: {
					'action': 'productClick', 
					'product_id': product_id_product, 
					'product_id_product_attribute' : product_id_product_attribute, 
					'secure_key':  pdgoogleanalytycenhancedecommercepro_secure_key, 
					'ajax' : true
				},
				dataType: "json",
				success: function(data) {
					console.log('Data layer push event: removeFromCart > Cart decrecase quantity button');
					dataLayer.push({ ecommerce: null });
					dataLayer.push({
						'event': 'removeFromCart',
						'ecommerce': {
							'currencyCode': iso_code,
							'remove': {
								'products': [{
									'name': data.content_name,
									'id': data.content_ids,
									'price': data.content_value,
									'brand': data.content_manufacturer,
									'category': data.content_category,
									'variant': data.content_variant,
									'quantity': 1
								}]
							}
						}
					});
				}
			});
		}
	});


	// qty up > stadard theme ps 17
	$('body').on("click", "button.js-increase-product-quantity", function(e) {

		let qty_input = $(this).parent().parent().find('input.js-cart-line-product-quantity'),
			updat_url = qty_input.attr('data-update-url'),
			url_params = parseQuery(updat_url),
			iso_code = prestashop.currency.iso_code;

		if (typeof(url_params.id_product) !== 'undefined' && typeof(url_params.id_product_attribute) !== 'undefined') {
			var product_id_product = url_params.id_product;
			var product_id_product_attribute = url_params.id_product_attribute;
		}

		if (typeof(product_id_product) !== 'undefined' && typeof(product_id_product_attribute) !== 'undefined') {
			$.ajax({
				type: "POST",
				url: pdgoogleanalytycenhancedecommercepro_ajax_link,
				data: {
					'action': 'productClick', 
					'product_id': product_id_product, 
					'product_id_product_attribute' : product_id_product_attribute, 
					'secure_key':  pdgoogleanalytycenhancedecommercepro_secure_key, 
					'ajax' : true
				},
				dataType: "json",
				success: function(data) {
					console.log('Data layer push event: addToCart > Cart increcase quantity button');
					dataLayer.push({ ecommerce: null });
					dataLayer.push({
						'event': 'addToCart',
						'ecommerce': {
							'currencyCode': iso_code,
							'add': {
								'products': [{
									'name': data.content_name,
									'id': data.content_ids,
									'price': data.content_value,
									'brand': data.content_manufacturer,
									'category': data.content_category,
									'variant': data.content_variant,
									'quantity': 1
								}]
							}
						}
					});

					if (pdgoogleanalytycenhancedecommercepro_gdr_enabled) {
						console.log('Data layer push GDR event: add_to_cart > Product add to cart button');
						dataLayer.push({
							'event': 'add_to_cart',
							'value': data.content_value,
							'items' : [{
								'id': data.content_ids,
								'google_business_vertical': 'retail'
							}]
						});
					}
				}
			});
		}
	});

	// qty down > stadard theme ps 17
	$('body').on("click", "button.js-decrease-product-quantity", function(e) {

		let qty_input = $(this).parent().parent().find('input.js-cart-line-product-quantity'),
			updat_url = qty_input.attr('data-update-url'),
			url_params = parseQuery(updat_url),
			iso_code = prestashop.currency.iso_code;

		if (typeof(url_params.id_product) !== 'undefined' && typeof(url_params.id_product_attribute) !== 'undefined') {
			var product_id_product = url_params.id_product;
			var product_id_product_attribute = url_params.id_product_attribute;
		}

		if (typeof(product_id_product) !== 'undefined' && typeof(product_id_product_attribute) !== 'undefined') {
			$.ajax({
				type: "POST",
				url: pdgoogleanalytycenhancedecommercepro_ajax_link,
				data: {
					'action': 'productClick', 
					'product_id': product_id_product, 
					'product_id_product_attribute' : product_id_product_attribute, 
					'secure_key':  pdgoogleanalytycenhancedecommercepro_secure_key, 
					'ajax' : true
				},
				dataType: "json",
				success: function(data) {
					console.log('Data layer push event: removeFromCart > Cart decrecase quantity button');
					dataLayer.push({ ecommerce: null });
					dataLayer.push({
						'event': 'removeFromCart',
						'ecommerce': {
							'currencyCode': iso_code,
							'remove': {
								'products': [{
									'name': data.content_name,
									'id': data.content_ids,
									'price': data.content_value,
									'brand': data.content_manufacturer,
									'category': data.content_category,
									'variant': data.content_variant,
									'quantity': 1
								}]
							}
						}
					});
				}
			});
		}
	});

	// Transformer theme
	$('body').on("click", "button.js-increase-product-quantity", function(e) {

		let qty_input = $(this).parent().parent().find('input.cart_quantity'),
			updat_url = qty_input.attr('data-update-url'),
			url_params = parseQuery(updat_url),
			iso_code = prestashop.currency.iso_code;

		if (typeof(url_params.id_product) !== 'undefined' && typeof(url_params.id_product_attribute) !== 'undefined') {
			var product_id_product = url_params.id_product;
			var product_id_product_attribute = url_params.id_product_attribute;
		}

		if (typeof(product_id_product) !== 'undefined' && typeof(product_id_product_attribute) !== 'undefined') {
			$.ajax({
				type: "POST",
				url: pdgoogleanalytycenhancedecommercepro_ajax_link,
				data: {
					'action': 'productClick', 
					'product_id': product_id_product, 
					'product_id_product_attribute' : product_id_product_attribute, 
					'secure_key':  pdgoogleanalytycenhancedecommercepro_secure_key, 
					'ajax' : true
				},
				dataType: "json",
				success: function(data) {
					console.log('Data layer push event: removeFromCart > Cart decrecase quantity buttonn');
					dataLayer.push({ ecommerce: null });
					dataLayer.push({
						'event': 'removeFromCart',
						'ecommerce': {
							'currencyCode': iso_code,
							'remove': {
								'products': [{
									'name': data.content_name,
									'id': data.content_ids,
									'price': data.content_value,
									'brand': data.content_manufacturer,
									'category': data.content_category,
									'variant': data.content_variant,
									'quantity': 1
								}]
							}
						}
					});
				}
			});
		}
	});

	// Transformer theme
	$('body').on("click", "button.js-decrease-product-quantity", function(e) {

		let qty_input = $(this).parent().parent().find('input.cart_quantity'),
			updat_url = qty_input.attr('data-update-url'),
			url_params = parseQuery(updat_url),
			iso_code = prestashop.currency.iso_code;

		if (typeof(url_params.id_product) !== 'undefined' && typeof(url_params.id_product_attribute) !== 'undefined') {
			var product_id_product = url_params.id_product;
			var product_id_product_attribute = url_params.id_product_attribute;
		}

		if (typeof(product_id_product) !== 'undefined' && typeof(product_id_product_attribute) !== 'undefined') {
			$.ajax({
				type: "POST",
				url: pdgoogleanalytycenhancedecommercepro_ajax_link,
				data: {
					'action': 'productClick', 
					'product_id': product_id_product, 
					'product_id_product_attribute' : product_id_product_attribute, 
					'secure_key':  pdgoogleanalytycenhancedecommercepro_secure_key, 
					'ajax' : true
				},
				dataType: "json",
				success: function(data) {
					console.log('Data layer push event: addToCart > Cart increcase quantity button');
					dataLayer.push({ ecommerce: null });
					dataLayer.push({
						'event': 'addToCart',
						'ecommerce': {
							'currencyCode': iso_code,
							'add': {
								'products': [{
									'name': data.content_name,
									'id': data.content_ids,
									'price': data.content_value,
									'brand': data.content_manufacturer,
									'category': data.content_category,
									'variant': data.content_variant,
									'quantity': 1
								}]
							}
						}
					});

					if (pdgoogleanalytycenhancedecommercepro_gdr_enabled) {
						console.log('Data layer push GDR event: add_to_cart > Product add to cart button');
						dataLayer.push({
							'event': 'add_to_cart',
							'value': data.content_value,
							'items' : [{
								'id': data.content_ids,
								'google_business_vertical': 'retail'
							}]
						});
					}
				}
			});
		}
	});

	// stadard theme ps 17
	$('body').on("click", "a.remove-from-cart", function(e) {

		let a_elem = $(this),
			updat_url = a_elem.attr('href'),
			url_params = parseQuery(updat_url),
			iso_code = prestashop.currency.iso_code,
			qty_input_val = parseInt(a_elem.parent().parent().prev().find('input.js-cart-line-product-quantity').val());

		if (typeof(qty_input_val) !== 'undefined') {
			qty_input_val = 1;
		}

		if (typeof(url_params.id_product) !== 'undefined' && typeof(url_params.id_product_attribute) !== 'undefined') {
			var product_id_product = url_params.id_product;
			var product_id_product_attribute = url_params.id_product_attribute;
		}

		if (typeof(product_id_product) !== 'undefined' && typeof(product_id_product_attribute) !== 'undefined') {
			$.ajax({
				type: "POST",
				url: pdgoogleanalytycenhancedecommercepro_ajax_link,
				data: {
					'action': 'productClick', 
					'product_id': product_id_product, 
					'product_id_product_attribute' : product_id_product_attribute, 
					'secure_key':  pdgoogleanalytycenhancedecommercepro_secure_key, 
					'ajax' : true
				},
				dataType: "json",
				success: function(data) {
					console.log('Data layer push event: removeFromCart > Delete in cart button');
					dataLayer.push({ ecommerce: null });
					dataLayer.push({
						'event': 'removeFromCart',
						'ecommerce': {
							'currencyCode': iso_code,
							'remove': {
								'products': [{
									'name': data.content_name,
									'id': data.content_ids,
									'price': data.content_value,
									'brand': data.content_manufacturer,
									'category': data.content_category,
									'variant': data.content_variant,
									'quantity': qty_input_val
								}]
							}
						}
					});
				}
			});
		}
	});

	// the checkout
	$('body#module-thecheckout-order').on("click", "a.remove-from-cart", function(e) {

		let a_elem = $(this),
			updat_url = a_elem.attr('href'),
			url_params = parseQuery(updat_url),
			iso_code = prestashop.currency.iso_code,
			qty_input_val = parseInt(a_elem.parent().parent().find('input.js-cart-line-product-quantity').val());

		if (typeof(qty_input_val) !== 'undefined') {
			qty_input_val = 1;
		}

		if (typeof(url_params.id_product) !== 'undefined' && typeof(url_params.id_product_attribute) !== 'undefined') {
			var product_id_product = url_params.id_product;
			var product_id_product_attribute = url_params.id_product_attribute;
		}

		if (typeof(product_id_product) !== 'undefined' && typeof(product_id_product_attribute) !== 'undefined') {
			$.ajax({
				type: "POST",
				url: pdgoogleanalytycenhancedecommercepro_ajax_link,
				data: {
					'action': 'productClick', 
					'product_id': product_id_product, 
					'product_id_product_attribute' : product_id_product_attribute, 
					'secure_key':  pdgoogleanalytycenhancedecommercepro_secure_key, 
					'ajax' : true
				},
				dataType: "json",
				success: function(data) {
					console.log('Data layer push event: removeFromCart > Delete in cart button');
					dataLayer.push({ ecommerce: null });
					dataLayer.push({
						'event': 'removeFromCart',
						'ecommerce': {
							'currencyCode': iso_code,
							'remove': {
								'products': [{
									'name': data.content_name,
									'id': data.content_ids,
									'price': data.content_value,
									'brand': data.content_manufacturer,
									'category': data.content_category,
									'variant': data.content_variant,
									'quantity': qty_input_val
								}]
							}
						}
					});
				}
			});
		}
	});

	$("body#module-thecheckout-order").on("click", "input[type=radio][name^=delivery_option]:checked", function() {
		let id_carrier =  parseInt(this.value);
		$.ajax({
			type: "POST",
			url: pdgoogleanalytycenhancedecommercepro_ajax_link,
			data: {
				'action': 'addDeliveryInfo', 
				'id_carrier': id_carrier, 
				'secure_key': pdgoogleanalytycenhancedecommercepro_secure_key, 
				'ajax': true
			},
			dataType: "json",
			success: function(data) {
				if (data) {
					$('#hook-display-before-carrier').append(data);
				}
			}
		});
	});

	$("body#module-thecheckout-order").on("click", "input[name=payment-option]:checked", function() {
		let payment_module = $(this).data('module-name');
		$.ajax({
			type: "POST",
			url: pdgoogleanalytycenhancedecommercepro_ajax_link,
			data: {
				'action': 'addPaymentInfo', 
				'payment_module': payment_module, 
				'secure_key': pdgoogleanalytycenhancedecommercepro_secure_key,
				'ajax': true
			},
			dataType: "json",
			success: function(data) {

				if (data) {
					$('#hook-display-before-carrier').html(data);
				}
			}
		});
	});

	// Thecheckout  module changeCheckoutStep > Add personal informations
	var email_typing_timer;
	var email_done_typing_interval = 3000;
	$('body#module-thecheckout-order form.address-fields input[name="email"]').keyup(function(){
	    clearTimeout(email_typing_timer);
	    if ($('body#module-thecheckout-order form.address-fields input[name="email"]').val()) {
	        email_typing_timer = setTimeout(checkoutOptionAddPersonalInformations, email_done_typing_interval);
	    }
	});

	function checkoutOptionAddPersonalInformations() {
	   $.ajax({
			type: "POST",
			url: pdgoogleanalytycenhancedecommercepro_ajax_link,
			data: {
				'action': 'addPersonalInformtions', 
				'secure_key': pdgoogleanalytycenhancedecommercepro_secure_key, 
				'ajax': true
			},
			dataType: "json",
			success: function(data) {
				if (data) {
					$('#hook-display-before-carrier').append(data);
				}
			}
		});
	}

	// Thecheckout module  changeCheckoutStep > Add address details
	var phone_typing_timer;
	var phone_done_typing_interval = 3000;
	$('body#module-thecheckout-order form.address-fields input[name="phone"]').keyup(function(){
	    clearTimeout(phone_typing_timer);
	    if ($('body#module-thecheckout-order form.address-fields input[name="phone"]').val()) {
	        phone_typing_timer = setTimeout(checkoutOptionAddAddress, phone_done_typing_interval);
	    }
	});

	function checkoutOptionAddAddress() {
	   $.ajax({
			type: "POST",
			url: pdgoogleanalytycenhancedecommercepro_ajax_link,
			data: {
				'action': 'addAddress', 
				'secure_key': pdgoogleanalytycenhancedecommercepro_secure_key, 
				'ajax': true
			},
			dataType: "json",
			success: function(data) {
				if (data) {
					$('#hook-display-before-carrier').append(data);
				}
			}
		});
	}


	if (typeof(prestashop) !== 'undefined') {
		prestashop.on('changedCheckoutStep', function(params) {

			if (typeof(params.event.currentTarget.id) !== 'undefined') {
				let step = params.event.currentTarget.id,
					step_call_elm = params.event.target.id;

				//console.log(step);
				//console.log(step_call_elm);

				if (step == 'checkout-personal-information-step') {
					$.ajax({
						type: "POST",
						url: pdgoogleanalytycenhancedecommercepro_ajax_link,
						data: {
							'action': 'addPersonalInformtions', 
							'secure_key': pdgoogleanalytycenhancedecommercepro_secure_key, 
							'ajax': true
						},
						dataType: "json",
						success: function(data) {
							if (data) {
								$('#checkout-personal-information-step').append(data);
							}
						}
					});
				}

				if (step == 'checkout-addresses-step') {
					$.ajax({
						type: "POST",
						url: pdgoogleanalytycenhancedecommercepro_ajax_link,
						data: {
							'action': 'addAddress', 
							'secure_key': pdgoogleanalytycenhancedecommercepro_secure_key, 
							'ajax': true
						},
						dataType: "json",
						success: function(data) {
							if (data) {
								$('#hook-display-before-carrier').append(data);
							}
						}
					});
				}

				if (step == 'checkout-delivery-step') {

					let id_carrier = parseInt($('.delivery-options input[type="radio"]:checked').val());

					$.ajax({
						type: "POST",
						url: pdgoogleanalytycenhancedecommercepro_ajax_link,
						data: {
							'action': 'addDeliveryInfo', 
							'id_carrier': id_carrier, 
							'secure_key': pdgoogleanalytycenhancedecommercepro_secure_key, 
							'ajax': true
						},
						dataType: "json",
						success: function(data) {
							if (data) {
								$('#hook-display-before-carrier').append(data);
							}
						}
					});
				}

				if (step == 'checkout-payment-step' && step_call_elm != 'conditions_to_approve[terms-and-conditions]') {

					let payment_module = $('input[name="payment-option"]:checked').data('module-name');

					$.ajax({
						type: "POST",
						url: pdgoogleanalytycenhancedecommercepro_ajax_link,
						data: {
							'action': 'addPaymentInfo', 
							'payment_module': payment_module, 
							'secure_key': pdgoogleanalytycenhancedecommercepro_secure_key, 
							'ajax': true
						},
						dataType: "json",
						success: function(data) {
							if (data) {
								$('#hook-display-before-carrier').append(data);
							}
						}
					});
				}
			}
		});

		prestashop.on('updateCart', function(params) {

			if (typeof(params) !== 'undefined' && typeof(prestashop.cart) !== 'undefined') {

				let iso_code = prestashop.currency.iso_code,
					product_id = params.reason.idProduct,
					product_id_product_attribute = params.reason.idProductAttribute;

				if (typeof(product_id) !== 'undefined' && typeof(product_id_product_attribute) !== 'undefined') {
					$.ajax({
						type: "POST",
						url: pdgoogleanalytycenhancedecommercepro_ajax_link,
						data: {
							'action': 'updateCart', 
							'product_id': product_id, 
							'product_id_product_attribute': product_id_product_attribute, 
							'secure_key': pdgoogleanalytycenhancedecommercepro_secure_key, 
							'ajax': true
						},
						dataType: "json",
						success: function(data) {
							console.log('Data layer push GA event: addToCart > Product add to cart button');
							dataLayer.push({ ecommerce: null });
							dataLayer.push({
								'event': 'addToCart',
								'ecommerce': {
									'currencyCode': iso_code,
									'add': {
										'products': [{
											'name': data.content_name,
											'id': data.content_ids,
											'price': data.content_value,
											'brand': data.content_manufacturer,
											'category': data.content_category,
											'variant': data.content_variant,
											'quantity': 1
										}]
									}
								}
							});

							if (pdgoogleanalytycenhancedecommercepro_gdr_enabled) {
								dataLayer.push({ ecommerce: null });
								console.log('Data layer push GDR event: add_to_cart > Product add to cart button');
								dataLayer.push({
								  'event': 'add_to_cart',
								  'value': data.content_value,
								  'items' : [{
								    'id': data.content_ids,
								    'google_business_vertical': 'retail'
								  }]
								});
							}
						}
					});
				}
			}
		});

		prestashop.on('updatedProduct', function(params) {

			if (typeof(params) !== 'undefined') {

				let iso_code = prestashop.currency.iso_code,
					product_id = parseInt(document.getElementsByName('id_product')[0].value),
					groups = [],
					select_groups = document.getElementsByClassName('form-control-select'),
					input_color_group = document.getElementsByClassName('input-color'),
					input_radio_group = document.querySelector('.input-radio:checked');

				if (typeof(select_groups) != 'undefined' && select_groups != null) {
					for (select_count = 0; select_count < select_groups.length; select_count++) {
						groups.push(select_groups[select_count].value);
					}
				}

				if (typeof(input_color_group) != 'undefined' && input_color_group != null) {
					for (color_count = 0; color_count < input_color_group.length; color_count++) {
						if (input_color_group[color_count].checked) {
							groups.push(input_color_group[color_count].value);
						}
					}
				}

				if (typeof(input_radio_group) != 'undefined' && input_radio_group != null) {
					for (radio_count = 0; radio_count < input_radio_group.length; radio_count++) {
						if (input_radio_group[radio_count].checked) {
							groups.push(input_radio_group[radio_count].value);
						}
					}
				}
				
				if (typeof groups !== 'undefined' && groups.length > 0 && typeof product_id !== 'undefined') {
					$.ajax({
						type: "POST",
						url: pdgoogleanalytycenhancedecommercepro_ajax_link,
						data: {
							'action': 'updateProduct', 
							'product_id': product_id, 
							'attributes_groups': groups,
							'secure_key':  pdgoogleanalytycenhancedecommercepro_secure_key,
							'ajax' : true
						},
						dataType: "json",
						success: function(data) {
							setEventFireUpDelay(250);
							console.log('Data layer push GA event: productDetailImpression > Product page attribute change');
							dataLayer.push({ ecommerce: null });
							dataLayer.push({
								'event': 'productDetailImpression',
								'ecommerce': {
									'currencyCode': iso_code,
									'detail': {
										'actionField': {
											'list': 'product-page'
										},
										'products': [{
											'id': data.content_ids,
											'name': data.content_name,
											'price': data.content_value,
											'brand': data.content_manufacturer,
											'category': data.content_category,
											'variant': data.content_variant
										}]
									}
								}
							});

							if (pdgoogleanalytycenhancedecommercepro_gdr_enabled) {
								dataLayer.push({ ecommerce: null });
								console.log('Data layer push GDR event: view_item > Product page attribute change');
								dataLayer.push({
									'event': 'view_item', 
									'value': data.content_value,
										'items': [{
											'id': data.content_ids,
											'google_business_vertical': 'retail',
										}]
								});
							}
						}
					});
				}
			}
		});
	}

	function setEventFireUpDelay(ms) {
		var cur_d = new Date();
		var cur_ticks = cur_d.getTime();
		var ms_passed = 0;
		while(ms_passed < ms) {
			var d = new Date();
			var ticks = d.getTime();
			ms_passed = ticks - cur_ticks;
		}
	}

	function parseQuery(str) {

		if(typeof str != "string" || str.length == 0) return {};
		var s = str.split("&");
		var s_length = s.length;
		var bit, query = {}, first, second;
		for(var i = 0; i < s_length; i++) {
			bit = s[i].split("=");
			first = decodeURIComponent(bit[0]);
			if(first.length == 0) continue;
			second = decodeURIComponent(bit[1]);
			if(typeof query[first] == "undefined") query[first] = second;
			else if(query[first] instanceof Array) query[first].push(second);
			else query[first] = [query[first], second]; 
		}
		return query;
	}

});