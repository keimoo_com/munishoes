$( document ).ready(buttonManage);

$(document).on('click', '#furgonetka-set-point', function(e){
    e.preventDefault();
    showMap()
});

$(document).on('change', '.delivery-options-list', function(){
    buttonManage();
});

$(document).on('click', 'button[name = "confirmDeliveryOption"]', function(e){
    var that = this;
    if($(this).hasClass('clicked')){
        $(this).removeClass('clicked');
    }else{
        e.preventDefault();
        var id_delivery = parseInt($('.delivery-options-list input:checked').val());
        if (!$('#furgonetka-machine-' + id_delivery).length){
            $.ajax({
                type: 'POST',
                url: furgonetkaCheckMapAjax,
                async: true,
                dataType: 'json',
                data: {id_delivery : id_delivery},
                success: function(jsonData) {
                    if(jsonData.status == 'error'){
                        alert(jsonData.message)
                    }else{
                        $(that).addClass('clicked');
                        $(that).click();
                    }
                },
            });
        }else{
            $(this).addClass('clicked');
            $(this).click();
        }
    }



});

$('body').off('click', '#payment-confirmation button').on('click', '#payment-confirmation button', function(e){
    var that = this;
        var id_delivery = parseInt($('.delivery-options-list input:checked').val());
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        if (!$('#furgonetka-machine-' + id_delivery).length){
            $.ajax({
                type: 'POST',
                url: furgonetkaCheckMapAjax,
                async: true,
                dataType: 'json',
                data: {id_delivery : id_delivery},
                success: function(jsonData) {
                    if(jsonData.status == 'error'){
                        alert(jsonData.message)
                    }else{
                        var option = $('input[name="payment-option"]:checked').attr('id');
                        if (option) {
                            $('#payment-confirmation button').prop('disabled', true);
                            $('#pay-with-' + option + '-form form').submit();
                        }
                    }
                },
            });
        }else{
            var option = $('input[name="payment-option"]:checked').attr('id');
            if (option) {
                $('#payment-confirmation button').prop('disabled', true);
                $('#pay-with-' + option + '-form form').submit();
            }
        }
});


function buttonManage(){
    var id_delivery = parseInt($('.delivery-options-list input:checked').val());
    var button = $('#furgonetka-set-point');
    if (!$('#furgonetka-delivery-' + id_delivery).length) {
        button.hide();
    }else{
        button.show();
    }
    showMachine();
}

function showMap(){
    var id_delivery = parseInt($('.delivery-options-list input:checked').val());
    var service = $('#furgonetka-delivery-' + id_delivery).val().split(',');
    var city = $('#furgonetka-city').val();
    var street = $('#furgonetka-street').val();
    if(typeof FurgonetkaBiznesMap === 'function'){
        new FurgonetkaBiznesMap({service: service,city: city, street: street, 'callback': furgonetkaMapCallback}).view();
    }
}

function showMachine(){
    var id_delivery = parseInt($('.delivery-options-list input:checked').val());
    $('.furgonetka-machine').hide();
    var machine_name = $('#furgonetka-machine-' + id_delivery);
    if (machine_name.length) {
        machine_name.show();
    }

}

function furgonetkaMapCallback(response){
    var id_delivery = parseInt($('.delivery-options-list input:checked').val());
    $.ajax({
        type: 'POST',
        url: furgonetkaMapAjax,
        async: true,
        dataType: 'json',
        data: {id_delivery : id_delivery, machine_code: response.code, machine_name : response.name, machine_type: response.type},
        success: function(jsonData) {
            if(jsonData.status == 'error'){
                jAlert(jsonData.message)
            }else{
                var input = '<input disabled class="furgonetka-machine" id="furgonetka-machine-' + id_delivery +'" value="'+ response.name +'">'
                $('#furgonetka-machine').html(input);
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
        }
    });
}
