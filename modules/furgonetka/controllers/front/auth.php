<?php
require_once __DIR__.'/../../classes/FurgonetkaAuth.php';
class FurgonetkaAuthModuleFrontController extends ModuleFrontController
{
    public function __construct()
    {
        parent::__construct();
        $this->display_column_left = false;
        $this->display_column_right = false;
        $this->display_header = false;
        $this->display_footer = false;
    }

    public function postProcess()
    {
        if(Tools::isSubmit('responseAuth')){
            return $this->postProcessResponseAuth();
        }
    }


    public function postProcessResponseAuth()
    {
        $backUrl = isset($_COOKIE['furgonetka_oauth_back_url']) ? $_COOKIE['furgonetka_oauth_back_url'] : null;
        $code = Tools::getValue('code');
        $code = urldecode($code);
        $auth = new FurgonetkaAuth();
        try{
            $auth->getTokenRequest(FurgonetkaAuth::getEnvironment(), $code);
            $auth->addIntegrationSource(FurgonetkaAuth::getEnvironment());
            $this->context->smarty->assign(array('status' => true));

            if ($backUrl) {
                unset($_COOKIE['furgonetka_oauth_back_url']);
                setcookie('furgonetka_oauth_back_url', '', time() - 3600, '/', '', false, true);
                Tools::redirect(\urldecode($backUrl));
            }

        }catch(Exception $e){
            FurgonetkaDb::resetConnection(FurgonetkaAuth::getEnvironment());
            $this->context->smarty->assign(array('status' => false, 'error' => $e->getMessage()));
        }

        $this->context->smarty->assign('is17', Tools::version_compare(_PS_VERSION_, '1.7', '>='));
        if(Tools::version_compare(_PS_VERSION_, '1.7', '>=')){
            $this->setTemplate('module:furgonetka/views/templates/front/furgonetka-auth-status.tpl');
        }else{
            $this->setTemplate('furgonetka-auth-status.tpl');
        }

    }
}
