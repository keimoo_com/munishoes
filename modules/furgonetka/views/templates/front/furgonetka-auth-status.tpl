{if $is17}
    <link rel="stylesheet" href="{$urls.base_url}modules/furgonetka/views/css/style.css" type="text/css" />
{else}
    <link rel="stylesheet" href="{$base_dir_ssl}modules/furgonetka/views/css/style.css" type="text/css" />
{/if}
{if isset($status) && $status}
    {l s="Połączenie wykonane pomyślnie." mod='furgonetka'}
{else}
    {l s="Wystąpił błąd podczas połączenia. Spróbuj ponownie lub skontaktuj się z Furgonetka.pl." mod='furgonetka'}
{/if}

