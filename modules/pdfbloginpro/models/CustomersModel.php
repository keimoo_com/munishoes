<?php
/**
* 2012-2017 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - Facebook login Pro © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek PrestaDev.pl <info@prestadev.pl>
* @copyright 2012-2017 Patryk Marek - PrestaDev.pl
* @license   License is for use in domain / or one multistore enviroment (do not modify or reuse this code or part of it) if you want any changes please contact with me at info@prestadev.pl
* @link      http://prestadev.pl
* @package   Facebook login Pro - PrestaShop 1.5.x and 1.6.x Module
* @version   1.1.0
* @date      10-06-2016
*/

class PdFbLoginProCustomersModel extends ObjectModel
{
    public $id;
    public $id_user;
    public $id_shop_group;
    public $id_shop;
    public $first_name;
    public $last_name;
    public $email;
    public $gender;
    public $birthday;
    public $date_add;
    public $date_upd;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'pdfbloginpro_customers_model',
        'primary' => 'id',
        'fields' => array(
            'id_user' => array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 128, 'required' => true),
            'id_shop_group' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'size' => 11, 'required' => true),
            'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'size' => 11, 'required' => true),
            'first_name' => array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 128),
            'last_name' => array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 128),
            'email' => array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 128),
            'gender' => array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 128),
            'birthday' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        ),
    );


    public function __construct($id = null)
    {
        parent::__construct($id);
    }

    public function add($autodate = false, $null_values = false)
    {
        return parent::add($autodate, $null_values);
    }

    public function delete()
    {
        if ((int)$this->id === 0) {
            return false;
        }

        return parent::delete();
    }

    public function update($null_values = false)
    {
        if ((int)$this->id === 0) {
            return false;
        }

        return parent::update($null_values);
    }

    /**
     * Creates tables
     */
    public static function createTables()
    {
        return Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pdfbloginpro_customers_model` (
               `id` INT NOT NULL AUTO_INCREMENT,
               `id_user` VARCHAR (128) NOT NULL,
               `id_shop_group` INT (11) NOT NULL,
               `id_shop` INT (11) NOT NULL,
               `first_name` VARCHAR (128) NOT NULL,
               `last_name` VARCHAR (128) NOT NULL,
               `email` VARCHAR (128) NOT NULL,
               `gender` VARCHAR (128) NOT NULL,
               `birthday` DATE NOT NULL,
               `date_add` DATE NOT NULL,
               `date_upd` DATE NOT NULL,
               PRIMARY KEY ( `id` )
            ) ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
        ');
    }

    public static function dropTables()
    {
        $sql = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'pdfbloginpro_customers_model`';

        return Db::getInstance()->execute($sql);
    }
}
