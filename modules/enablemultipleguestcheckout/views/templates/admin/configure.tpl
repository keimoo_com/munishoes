
<fieldset class="panel">
    <iframe src="//tech.wediscompany.ro/levi/test/promo.html" width="100%" height="184" border="0" style="border:none;"></iframe>
    <legend class="panel-heading"> Forced Guest Checkout </legend>
</fieldset>



<fieldset class="panel">

    <legend class="panel-heading"> Module Description </legend>
<p>
        Simplify the checkout process for your customers. Our module will allow your customers to make orders as guests even if their email addresses already have accounts associated.  <br />
        The Forced Guest Checkout module is ideal for you and it works with all checkout modules.
    </p>
</fieldset>
