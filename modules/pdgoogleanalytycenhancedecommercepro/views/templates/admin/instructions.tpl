
{*
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analytics Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analytics Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*}


<div class="panel">
	<p>
    	<b>{l s='Login to Your account' mod='pdgoogleanalytycenhancedecommercepro'} <a href="https://analytics.google.com/analytics/web/#management/Settings/"> {l s='Google Analytics' mod='pdgoogleanalytycenhancedecommercepro'} </a></b>
	</p>
   	<ol>
       	<li>{l s='Go to: Google Analytics > Administration > E-commerce settings' mod='pdgoogleanalytycenhancedecommercepro'}</li>
        <li>{l s='Turn on "e-commerce" i "enhanced e-commerce"' mod='pdgoogleanalytycenhancedecommercepro'}</li>
        <li>{l s='Enter the tracking ID in the Google Analytics tab, e.g. "UA-00000000-0" you will find it after opening page' mod='pdgoogleanalytycenhancedecommercepro'} <a href="https://analytics.google.com/analytics/web/#management/Settings/">{l s='Google Analytics' mod='pdgoogleanalytycenhancedecommercepro'}</a> {l s='next go to section Administration > Service settings > Tracking identifier' mod='pdgoogleanalytycenhancedecommercepro'}</li>
    </ol>
    <br />
    <p>
    	<b>{l s='Login to Your account' mod='pdgoogleanalytycenhancedecommercepro'} <a href="https://www.google.com/analytics/tag-manager/"> {l s='Google Tag Manager' mod='pdgoogleanalytycenhancedecommercepro'} </a></b>
	<ol>
       	<li>{l s='Create a new "Web" type container and then configure the event or import a ready file with all events' mod='pdgoogleanalytycenhancedecommercepro'}</li>
       	<li>{l s='Enter the container ID, eg "GTM-XXXXXXX" in the tab "Google Tag Manager"' mod='pdgoogleanalytycenhancedecommercepro'}</li>
       </ol>
	</p>
	<br />

	<p class="alert alert-info">
		<b>{l s='How does the module work? ' mod='pdgoogleanalytycenhancedecommercepro'}</b><br />
		<span>{l s='The module sends data from the store via the DataLayer variable to Google Tag Manager and via the tags / triggers configured in the Tag Manager it sends the data to the Google Analytics account.' mod='pdgoogleanalytycenhancedecommercepro'}</span><br />
		<b>{l s='Each action (product view, add to cart, purchase, etc.) in this module uses events.' mod='pdgoogleanalytycenhancedecommercepro'}</b>
    </p>
   	<p>
   		{l s='Please also refer to the official documentation' mod='pdgoogleanalytycenhancedecommercepro'} <b><a href="https://developers.google.com/tag-manager/enhanced-ecommerce">{l s='Google Analytics Enhanced Ecommerce UA via DataLayer' mod='pdgoogleanalytycenhancedecommercepro'}</a></b> {l s='on tag configuration for each event.' mod='pdgoogleanalytycenhancedecommercepro'}
   	</p>
</div>