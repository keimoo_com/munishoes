
{*
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analytics Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analytics Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*}


<div class="panel">

	<p class="alert alert-info">
		<b>{l s='Download ready-made configurations for import in Tag Manager' mod='pdgoogleanalytycenhancedecommercepro'}</b><br />
	</p>


	<p class="alert alert-danger">
		<b>{l s='IMPORTANT: You must add Google Tag Manager id and Google Analytics id, before You download file with automatic Google Tag Manager setup!!!' mod='pdgoogleanalytycenhancedecommercepro'}</b><br />
	</p>

	<p>
		{l s='With belowe link You can automaticly import tags configuration to Google Tag Manager account:' mod='pdgoogleanalytycenhancedecommercepro'}
	</p>

	<a href="{$gtm_link nofilter}">{l s='Download GTM configuration file for your module configuration ' mod='pdgoogleanalytycenhancedecommercepro'}</a>
</div>