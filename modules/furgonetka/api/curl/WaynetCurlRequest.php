<?php
require_once __DIR__.'/WaynetCurlResponse.php';

class WaynetCurlRequest
{
    private $url;
    private $requestMethod;
    private $headers = array();
    private $postFields = array();
    private $optParams = array();

    /**
     * @return array
     */
    public function getOptParams()
    {
        return $this->optParams;
    }

    /**
     * @param array $optParams
     */
    public function setOptParams(array $optParams)
    {
        $this->optParams = $optParams;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getRequestMethod()
    {
        return $this->requestMethod;
    }

    /**
     * @param mixed $requestMethod
     */
    public function setRequestMethod($requestMethod)
    {
        $this->requestMethod = $requestMethod;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }

    /**
     * @return array|string
     */
    public function getPostFields()
    {
        return (!empty($this->postFields) && is_array($this->postFields))?
            http_build_query($this->postFields):  $this->postFields;
    }

    /**
     * @param array $postFields
     */
    public function setPostFields($postFields)
    {
        $this->postFields = $postFields;
    }

    public function exec()
    {
        $debugMode = false;

        $postFields = $this->getPostFields();

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->getUrl());

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $this->getRequestMethod());

        if (!empty($postFields)) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $this->getPostFields());

        }
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeaders());

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLINFO_HEADER_OUT, true);

        if (($optParams = $this->getOptParams()) != false) {
            curl_setopt_array($curl, $optParams);
        }
        $response = curl_exec($curl);

        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($response === false) {
            throw new Exception(curl_error($curl), curl_errno($curl));
        }

        curl_close($curl);
        return new WaynetCurlResponse($code, $response);
    }
}
