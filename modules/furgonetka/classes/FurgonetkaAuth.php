<?php
require_once 'PrestaApi.php';
require_once __DIR__.'/../api/soap/SoapFurgonetka.php';
require_once __DIR__.'/../api/soap/SoapClasess.php';
require_once __DIR__.'/../api/curl/FurgonetkaCurl.php';

class FurgonetkaAuth
{
    const INTEGRATION_TYPE = 'prestashop';

    public function integrateAuth($test)
    {
        $soap = new SoapFurgonetka($test);
        $url =  urlencode($this->__getResponseUrl());
        $data_1  =  $this->__getBaseUrl();
        $data_2  =  $this->__getPrestaApiKey();
        try {
            $response = $soap->createIntegrationOAuthApplication($url, $data_1, $data_2, self::INTEGRATION_TYPE);
            $response->validateThrow();
        } catch (Exception $e) {
            throw new Exception('Error oAuth integration.<br>' . $e->getMessage());
        }
        $client_id = $response->client_id;
        $client_secret = $response->client_secret;
        $status = $test? 'TEST_' : '';
        Configuration::updateGlobalValue($status.'FURGONETKA_CLIENT_ID', $client_id);
        Configuration::updateGlobalValue($status.'FURGONETKA_CLIENT_SECRET', $client_secret);

    }

    public function getRedirectAuthorizationLink($test)
    {
        $clients = $this->getAuthClients($test);

        $urlTest = $test? '-test' : '';
        $url = 'https://konto'.$urlTest.'.furgonetka.pl/oauth/authorize'.
        '?response_type=code'.
        '&client_id='.$clients['client_id'].
        '&redirect_uri='.urlencode($this->__getResponseUrl());

        return $url;
    }

    public function getTokenRequest($test, $code)
    {
        $clients = self::getAuthClients($test);
        $authorization = base64_encode($clients['client_id'].':'.$clients['client_secret']);
        $redirect_uri = $this->__getResponseUrl();
        $curl = new FurgonetkaCurl();
        $response = $curl->getAuthToken($test, $authorization,$code, 'authorization_code', $redirect_uri);
        $responseData = json_decode($response->getResponse());
        $checkResponse = isset($responseData->access_token)
            &&  !empty($responseData->access_token)
            && isset($responseData->refresh_token)
            && !empty($responseData->refresh_token);

        if($response->getCode() != 200 || !$checkResponse){
            $message = isset($responseData->message)? $responseData->message : '';
            throw new Exception('Error get access token.<br>'.$message);
        }

        $access_token = $responseData->access_token;
        $refresh_token = $responseData->refresh_token;
        $status = $test? 'TEST_' : '';
        Configuration::updateGlobalValue($status.'FURGONETKA_ACCESS_TOKEN', $access_token);
        Configuration::updateGlobalValue($status.'FURGONETKA_REFRESH_TOKEN', $refresh_token);
        $expire = new DateTime();
        $refreshMainToken = new DateTime();
        $expire->modify('+3 month');
        $refreshMainToken->modify('+1 week');
        Configuration::updateGlobalValue($status.'FURGONETKA_REFRESH_TOKEN_EXPIRE', $expire->format('Y-m-d'));
        Configuration::updateGlobalValue($status.'FURGONETKA_MAIN_TOKEN_DATE_REFRESH', $refreshMainToken->format('Y-m-d'));

    }

    public function addIntegrationSource($test)
    {
        $auth = self::getAuth($test);
        $data_1  =  $this->__getBaseUrl();
        $data_2  =  $this->__getPrestaApiKey();
        $data_3  =  Context::getContext()->link->getModuleLink(
            'furgonetka',
            'cartDetailsGate',
            array(),
            null,
            null,
            Configuration::get('PS_SHOP_DEFAULT'));;
        $soap = new SoapFurgonetka($test);
        try {
            $response = $soap->addIntegrationSource($auth, $data_1, $data_2, $data_3);
            $response->validateThrow();
        } catch (Exception $e) {
            throw new Exception('Error source integration.<br>' . $e->getMessage());
        }
        $status = $test? 'TEST_' : '';
        $source_id = $response->source_id;
        Configuration::updateGlobalValue($status.'FURGONETKA_SOURCE_ID', $source_id);
    }

    public static function getTokens($test)
    {
        $status = $test? 'TEST_' : '';
        if ((empty(Configuration::get($status.'FURGONETKA_ACCESS_TOKEN')) || empty(Configuration::get($status.'FURGONETKA_REFRESH_TOKEN')))) {
            throw new Exception('Przejdź do ustawień modułu i skonfiguruj swoje konto.');
        }
        $tokens = array();
        $tokens['access_token'] = Configuration::get($status.'FURGONETKA_ACCESS_TOKEN');
        $tokens['refresh_token'] = Configuration::get($status.'FURGONETKA_REFRESH_TOKEN');
        return $tokens;
    }
    
    public function refreshToken($test)
    {
        $tokens = self::getTokens($test);
        $refresh_token = $tokens['refresh_token'];
        $clients = self::getAuthClients($test);
        $authorization = base64_encode($clients['client_id'].':'.$clients['client_secret']);
        $redirect_uri = $this->__getResponseUrl();
        $curl = new FurgonetkaCurl();
        $response = $curl->refreshToken($test, $authorization, 'refresh_token', $refresh_token, $redirect_uri);
        $responseData = json_decode($response->getResponse());
        $checkResponse = isset($responseData->access_token)
            &&  !empty($responseData->access_token)
            && isset($responseData->refresh_token)
            && !empty($responseData->refresh_token);

        if($response->getCode() != 200 || !$checkResponse){
            $message = isset($responseData->message)? $responseData->message : '';
            throw new Exception('Error refresh token.<br>'.$message);
        }

        $access_token = $responseData->access_token;
        $refresh_token = $responseData->refresh_token;
        $status = $test? 'TEST_' : '';
        Configuration::updateGlobalValue($status.'FURGONETKA_ACCESS_TOKEN', $access_token);
        Configuration::updateGlobalValue($status.'FURGONETKA_REFRESH_TOKEN', $refresh_token);
        $expire = new DateTime();
        $refreshMainToken = new DateTime();
        $expire->modify('+3 month');
        $refreshMainToken->modify('+1 week');
        Configuration::updateGlobalValue($status.'FURGONETKA_REFRESH_TOKEN_EXPIRE', $expire->format('Y-m-d'));
        Configuration::updateGlobalValue($status.'FURGONETKA_MAIN_TOKEN_DATE_REFRESH', $refreshMainToken->format('Y-m-d'));


    }

    public static function getAuthClients($test)
    {
        $status = $test? 'TEST_' : '';
        if ((empty(Configuration::get($status.'FURGONETKA_CLIENT_ID')) || empty(Configuration::get($status.'FURGONETKA_CLIENT_SECRET')))) {
            throw new Exception('Client ids doesnt exist');
        }
        $clients = array();
        $clients['client_id'] = Configuration::get($status.'FURGONETKA_CLIENT_ID');
        $clients['client_secret'] = Configuration::get($status.'FURGONETKA_CLIENT_SECRET');
        return $clients;

    }

    private function __getResponseUrl()
    {
        return  Context::getContext()->link->getModuleLink(
            'furgonetka',
            'auth',
            array('responseAuth' => 1),
            null,
            null,
            Configuration::get('PS_SHOP_DEFAULT'));
    }

    private function __getBaseUrl()
    {
        $prestaApi = new PrestaApi();
        return $prestaApi->getShopUrl();
    }

    private function __getPrestaApiKey()
    {
        $prestaApi = new PrestaApi();
        $prestaApi->addWebservice();
        $token = $prestaApi->getWebServiceToken();
        if (empty($token)) {
            throw new Exception('Presta api token doesnt exist');
        }
        return $token;

    }

    public static function getAuth($test)
    {
        $tokens = self::getTokens($test);
        $token = $tokens['access_token'];
        return new FurgonetkaSoapAuth($token);
    }

    public static function getSourceId($test)
    {
        $status = $test? 'TEST_' : '';
        if(empty(Configuration::getGlobalValue($status.'FURGONETKA_SOURCE_ID'))){
            throw new Exception('Source doesnt exist');
        }
        return Configuration::getGlobalValue($status.'FURGONETKA_SOURCE_ID');
    }

    public static function getEnvironment()
    {
        $env = Configuration::get('FURGONETKA_ENVIRONMENT');
        return $env == 'TEST';
    }


}
