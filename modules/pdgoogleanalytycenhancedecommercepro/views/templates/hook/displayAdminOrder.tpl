{*
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*}

<div class="row">
	<div class="col-lg-7">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-user"></i>
				{l s='Google Analitycs' mod='pdgoogleanalytycenhancedecommercepro'}
			</div>
			<div class="well hidden-print">
				<label>{l s='Order send' mod='pdgoogleanalytycenhancedecommercepro'}:</label> 
				{if isset($data->order_send) && $data->order_send}
					{l s='Yes' mod='pdgoogleanalytycenhancedecommercepro'}
				{else}
					{l s='No' mod='pdgoogleanalytycenhancedecommercepro'}
				{/if}
				<br />
				<label>{l s='Order refunded' mod='pdgoogleanalytycenhancedecommercepro'}:</label> 
				{if isset($data->refund_send) && $data->refund_send}
					{l s='Yes' mod='pdgoogleanalytycenhancedecommercepro'}
				{else}
					{l s='No' mod='pdgoogleanalytycenhancedecommercepro'}
				{/if}
			    <br />
			    <label>{l s='Date last change' mod='pdgoogleanalytycenhancedecommercepro'}:</label> 
			    {if isset($data->date_upd)}
			    	{$data->date_upd}
			    {/if}
			    <input type="hidden" id="pdgoogleanalytycenhancedecommercepro_id_order" value="{$id_order}"> 
				<div style="display:hidden;" id="pdgoogleanalytycenhancedecommercepro_ajax_response"></div>
			</div>
		</div>
	</div>
</div>
