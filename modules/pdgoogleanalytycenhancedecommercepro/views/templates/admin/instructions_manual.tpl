
{*
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*}


<div class="panel">

	<p class="alert alert-info">
		<b>{l s='Tag configuration for all events:' mod='pdgoogleanalytycenhancedecommercepro'}</b>
	</p>
   	<ul>
   		<li><b>{l s='Tag type:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='Universal Analytics' mod='pdgoogleanalytycenhancedecommercepro'}</li>
		<li><b>{l s='Tracking type:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='Event' mod='pdgoogleanalytycenhancedecommercepro'}</li>
		<li><b>{l s='Tag category:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='Ecommerce' mod='pdgoogleanalytycenhancedecommercepro'}</li>
		<li><b>{l s='Action:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='(enter event name)' mod='pdgoogleanalytycenhancedecommercepro'}</li>
		<li><b>{l s='Non-interaction activity:' mod='pdgoogleanalytycenhancedecommercepro'}</b></li>
			<ul>
		    <li><b>{l s='True - for events like:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='productImpressions, productDetailImpression, checkout, checkoutOption, purchase, refund' mod='pdgoogleanalytycenhancedecommercepro'}</li>
		    <li><b>{l s='False - for events like:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='productClick, addToCart, removeFromCart' mod='pdgoogleanalytycenhancedecommercepro'}</li>
		    </ul>
		<li><b>{l s='Enable Enhanced Ecommerce Features:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='yes' mod='pdgoogleanalytycenhancedecommercepro'}</li>
		<li><b>{l s='Use the data layer:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='yes' mod='pdgoogleanalytycenhancedecommercepro'}</li>
		<li><b>{l s='Rule:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='event is equal to (enter event name)' mod='pdgoogleanalytycenhancedecommercepro'}</li>
   	</ul>
   	<br />
	<p class="alert alert-info">
		<b>{l s='List of events / actions (name and description):' mod='pdgoogleanalytycenhancedecommercepro'}</b>
	</p>
	<ul>
   		<li><b>productImpressions</b> - {l s='product impressions, one the lists of products like category, new products, prices-drops, bestsellers' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>productClick</b> - {l s='product clicks, one the lists of products like category, new products, prices-drops, bestsellers' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>productDetailImpression</b> - {l s='view of product details, on the product page' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>addToCart</b> - {l s='adding product to the shopping cart' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>removeFromCart</b> - {l s='removing product from the shopping cart' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>checkout</b> - {l s='measuring checkout steps' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>checkoutOption</b> - {l s='measuring checkout options like add personal, payment or shipping informations' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>refund</b> - {l s='measuring refunds' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>purchase</b> - {l s='measuring purchases' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>promoView</b> - {l s='measuring views of promotions' mod='pdgoogleanalytycenhancedecommercepro'}</li>

	</ul>
	<br />
	<p class="alert alert-info">
		<b>{l s='Purchase event configuration example:' mod='pdgoogleanalytycenhancedecommercepro'}</b>
	</p>
	<ul>
   		<li><b>{l s='Tag type:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='Universal Analytics' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>{l s='Tracking type:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='Event' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>{l s='Tag category:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='Ecommerce' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>{l s='Action:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='Purchase' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>{l s='Non-interaction activity:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='True' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>{l s='Enable Enhanced Ecommerce Features:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='yes' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>{l s='Use the data layer:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='yes' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>{l s='Rule:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='event is equal to purchase' mod='pdgoogleanalytycenhancedecommercepro'}</li></li>
	</ul>
	<br />
	<p class="alert alert-info">
		<b>{l s='Order steps labeling' mod='pdgoogleanalytycenhancedecommercepro'}</b>
	</p>
	<ul>
   		<li><b>{l s='1:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='Cart summary' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>{l s='2:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='Initiate checkout' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>{l s='3:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='Add personal informations' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>{l s='4:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='Add address' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>{l s='5:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='Choose shipping method' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>{l s='6:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='Choose payment method' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	  	<li><b>{l s='7:' mod='pdgoogleanalytycenhancedecommercepro'}</b> {l s='Order confirmation' mod='pdgoogleanalytycenhancedecommercepro'}</li>
	</ul>
</div>