<div class="furgonetka-iframe-container">
    <div class="furgonetka-iframe" id="furgonetka-iframe">
        <iframe src="{$url}" width="1000" height="900"></iframe>
        <a href="#" class="furgonetka-iframe-exit" id="furgonetka-iframe-exit">
            <svg viewport="0 0 12 12" enable-background="new 0 0 12 12" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <line x1="1" y1="11" x2="11" y2="1" stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
                <line x1="1" y1="1" x2="11" y2="11" stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
            </svg>
        </a>
    </div>
</div>
<div class="furgonetka-iframe-backdrop"></div>

<script>
    $(function(){
        $('.furgonetka-iframe-container, #furgonetka-iframe').fadeIn().addClass("in");
        $('#main-div').addClass("f-modal");
    });

    $('#furgonetka-iframe-exit').on("click", function(e){
        e.preventDefault();

        $('.furgonetka-iframe-container, #furgonetka-iframe, .furgonetka-iframe-backdrop').fadeOut(function(){
            $(this).remove();
            $(document).trigger('furgonetka-iframe:close', this);
        });
        $('#main-div').removeClass("f-modal");
    });
</script>

<style>
    #main-div.f-modal {
        float: left;
        width: 100%;
        z-index: 10;
    }
    .furgonetka-iframe{
        position: relative;
        width: calc(100% - 40px);
        max-width: 1400px;
        margin: 0 20px;
        padding: 0;

        height:900px;
        max-height: calc(100vh - 140px);
        overflow: hidden;

        background: white;

        box-shadow: 0 3px 6px rgba(0,0,0,0.2), 0 3px 6px rgba(0,0,0,0.3);
        transition: all 0.3s cubic-bezier(.25,.8,.25,1);
    }

    .furgonetka-iframe iframe{
        width: 100%;
        height: 100%;
        overflow: auto;
        border: 0;
    }

    .furgonetka-iframe-exit{
        top: 0px;
        right: 0px;
        width: 12px;
        height: 13px;
        color: white;
        position: absolute;
        font-size: 13px;
        padding: 7px;
        background-color: black;
        box-sizing: content-box;
        text-decoration: none;
    }

    .furgonetka-iframe-backdrop {
        position: fixed;
        width: 100vw;
        height: 100vh;
        z-index: 1000;
        background-color: rgba(0,0,0, 0.6);
    }

    .furgonetka-iframe-container {
        position: fixed;
        width: 100vw;
        height: 100vh;
        display: flex;
        justify-content: center;
        align-items: center;
        z-index: 1050;
    }
</style>