/*
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*/

// $(document).ready(function() {

	// // old ordr page on ps 17
	// $(document).on("click", "button#submit_state", function(e) {
	// 	e.preventDefault();

	// 	$('#pdgoogleanalytycenhancedecommercepro_ajax_response').html('');
	// 	let select_elem = $(this).parent().parent().find('select#id_order_state'),
	// 		id_order_status = parseInt(select_elem.val()),
	// 		id_order = parseInt($('input[name="id_order"]').val());


	// 	if (typeof(id_order) !== 'undefined' 
	// 		&& typeof(id_order_status) !== 'undefined'
	// 		&& inArray(id_order_status, pdgoogleanalytycenhancedecommercepro_os_send)
	// 		) {
	// 		$.ajax({
	// 			type: "POST",
	// 			url: pdgoogleanalytycenhancedecommercepro_ajax_url,
	// 			data: {
	// 				'action': 'SendOrder',
	// 				'id_order': id_order, 
	// 				'id_order_status': id_order_status, 
	// 				'ajax' : true
	// 			},
	// 			dataType: "json",
	// 			success: function(data) {
	// 				$('#pdgoogleanalytycenhancedecommercepro_ajax_response').append(data);
	// 				setEventFireUpDelay(500);
	// 				$(this).trigger(e);
	// 			}
	// 		});

	// 	} else {
	// 		$(this).trigger(e);
	// 	}

	// 	if (typeof(id_order) !== 'undefined' 
	// 		&& typeof(id_order_status) !== 'undefined'
	// 		&& inArray(id_order_status, pdgoogleanalytycenhancedecommercepro_os_refund)
	// 		) {
			
	// 		$.ajax({
	// 			type: "POST",
	// 			url: pdgoogleanalytycenhancedecommercepro_ajax_url,
	// 			data: {
	// 				'action': 'RefundOrder',
	// 				'id_order': id_order, 
	// 				'id_order_status': id_order_status, 
	// 				'ajax' : true
	// 			},
	// 			dataType: "json",
	// 			success: function(data) {
	// 				$('#pdgoogleanalytycenhancedecommercepro_ajax_response').append(data);
	// 				setEventFireUpDelay(500);
	// 				$(this).trigger(e);
	// 			}
	// 		});

	// 	} else {
	// 		$(this).trigger(e);
	// 	}
	// });


	// // new ordr page on ps 17
	// $(document).on("click", "button#update_order_status_action_btn", function(e) {
	// 	e.preventDefault();

	// 	$('#pdgoogleanalytycenhancedecommercepro_ajax_response').html('');
	// 	let select_elem = $(this).parent().find('select#update_order_status_action_input'),
	// 		id_order_status = parseInt(select_elem.val()),
	// 		id_order = parseInt($('input#pdgoogleanalytycenhancedecommercepro_id_order').val());

	// 	if (typeof(id_order) !== 'undefined' 
	// 		&& typeof(id_order_status) !== 'undefined'
	// 		&& inArray(id_order_status, pdgoogleanalytycenhancedecommercepro_os_send)
	// 		) {
	// 		$.ajax({
	// 			type: "POST",
	// 			url: pdgoogleanalytycenhancedecommercepro_ajax_url,
	// 			data: {
	// 				'action': 'SendOrder',
	// 				'id_order': id_order, 
	// 				'id_order_status': id_order_status, 
	// 				'ajax' : true
	// 			},
	// 			dataType: "json",
	// 			success: function(data) {
	// 				$('#pdgoogleanalytycenhancedecommercepro_ajax_response').append(data);
	// 				setEventFireUpDelay(500);
	// 				$('form#update_order_status_action_form').submit();
	// 			}
	// 		});
	// 	}

	// 	if (typeof(id_order) !== 'undefined' 
	// 		&& typeof(id_order_status) !== 'undefined'
	// 		&& inArray(id_order_status, pdgoogleanalytycenhancedecommercepro_os_refund)
	// 		) {
			
	// 		$.ajax({
	// 			type: "POST",
	// 			url: pdgoogleanalytycenhancedecommercepro_ajax_url,
	// 			data: {
	// 				'action': 'RefundOrder',
	// 				'id_order': id_order, 
	// 				'id_order_status': id_order_status, 
	// 				'ajax' : true
	// 			},
	// 			dataType: "json",
	// 			success: function(data) {
	// 				$('#pdgoogleanalytycenhancedecommercepro_ajax_response').append(data);
	// 				setEventFireUpDelay(500);
	// 				$('form#update_order_status_action_form').submit();
	// 			}
	// 		});
	// 	}
	// });

	// function setEventFireUpDelay(ms) {
	// 	var cur_d = new Date();
	// 	var cur_ticks = cur_d.getTime();
	// 	var ms_passed = 0;
	// 	while(ms_passed < ms) {
	// 		var d = new Date();
	// 		var ticks = d.getTime();
	// 		ms_passed = ticks - cur_ticks;
	// 	}
	// }

	// function inArray(needle, haystack) {
	//     var length = haystack.length;
	//     for(var i = 0; i < length; i++) {
	//         if(haystack[i] == needle) return true;
	//     }
	//     return false;
	// }

// });