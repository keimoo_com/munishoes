{*
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*}

<!-- START > PD Google Analytycs Enhanced Ecommerce trought Tag Manager > DataLayer 1.7.x Module > purchase -->
<script>
	console.log('Data layer push GA event: purchase > Admin order page');
	dataLayer.push({ ecommerce: null });  // Clear the previous ecommerce object.
	dataLayer.push({
		'event': 'purchase',
		'ecommerce': {
			'purchase': {
				'actionField': {
					'id': '{$content_transaction_id nofilter}',
					'affiliation': '{$http_referer nofilter}',
					'revenue': {$content_value nofilter},
					'tax': {$content_tax nofilter},
					'shipping': {$content_shipping nofilter},
					'coupon': '{$content_coupon nofilter}'
				},
				'products': [{foreach from=$content_products item=product name=products}
					{
						'id': '{$product.content_ids nofilter}',
						'name': '{$product.product_name nofilter}',
						'coupon': '{$product.content_coupon nofilter}',
						'brand': '{$product.manufacturer nofilter}',
						'category': '{$product.category_name nofilter}',
						'variant': '{if isset($product.variant)}{$product.variant}{/if}',
						'price': {$product.price nofilter},
						'quantity': '{$product.product_quantity nofilter}'
					},
					{/foreach}]
			}
		}
	});
	
	console.log('Data layer push GDR event: purchase > Admin order page');
	dataLayer.push({
		'event': 'purchase', 
        'value': {$content_value nofilter},
        'items': [{foreach from=$content_products item=product name=products}
            {
                'id': '{$product.content_ids nofilter}',
                'google_business_vertical': 'retail'
            },
        	{/foreach}
        ]
    });
</script>
<!-- END > PD Google Analytycs Enhanced Ecommerce trought Tag Manager > DataLayer 1.7.x Module > purchase -->

