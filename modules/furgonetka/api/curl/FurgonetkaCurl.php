<?php
require_once __DIR__.'/WaynetCurlRequest.php';
require_once __DIR__.'/WaynetCurlResponse.php';

class FurgonetkaCurl
{
    public function getAuthToken($test, $authorization, $code, $grant_type, $redirect_uri)
    {
        $url = $test? 'https://konto-shop-test.furgonetka.pl/oauth/token' : 'https://konto-shop.furgonetka.pl/oauth/token';
        $params = array(
            'code' => $code,
            'grant_type' => $grant_type,
            'redirect_uri' => $redirect_uri,
        );
        $curl = new WaynetCurlRequest();
        $curl->setUrl($url);
        $curl->setRequestMethod('POST');
        $curl->setPostFields($params);
        $curl->setOptParams(array(CURLOPT_USERAGENT  => $this->getUserAgent()));
        $curl->setHeaders(array('Content-Type: application/x-www-form-urlencoded', 'Authorization: Basic '.$authorization));
        return $curl->exec();
    }

    public function refreshToken($test, $authorization, $grant_type, $refresh_token, $redirect_uri)
    {
        $url = $test? 'https://konto-shop-test.furgonetka.pl/oauth/token' : 'https://konto-shop.furgonetka.pl/oauth/token';
        $params = array(
            'refresh_token' => $refresh_token,
            'grant_type' => $grant_type,
            'redirect_uri' => $redirect_uri,
        );
        $curl = new WaynetCurlRequest();
        $curl->setUrl($url);
        $curl->setRequestMethod('POST');
        $curl->setPostFields($params);
        $curl->setOptParams(array(CURLOPT_USERAGENT  => $this->getUserAgent()));
        $curl->setHeaders(array('Content-Type: application/x-www-form-urlencoded', 'Authorization: Basic '.$authorization));
        return $curl->exec();
    }

    private function getUserAgent()
    {
        $module = Module::getInstanceByName('furgonetka');
        $module->version;

        return 'Prestashop '._PS_VERSION_.' Plugin '. $module->version;;
    }
}
