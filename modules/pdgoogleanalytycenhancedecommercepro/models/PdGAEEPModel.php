<?php
/**
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*/

class PdGAEEPModel extends ObjectModel
{
    public $id_order;
    public $order_send;
    public $refund_send;
    public $client_id;
    public $date_add = '0000-00-00 00:00:00';
    public $date_upd = '0000-00-00 00:00:00';

    public static $definition = array(
        'table' => 'pdgoogleanalytycenhancedecommercepro',
        'primary' => 'id_order',
        'multilang' => false,
        'fields' => array(
            'id_order' =>        array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => false),
            'order_send' =>      array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false),
            'refund_send' =>     array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false),
            'client_id' =>       array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => false),
            'date_add' =>        array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false),
            'date_upd' =>        array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false)
        )
    );

    public function __construct($id_order = false)
    {
        parent::__construct($id_order);
    }

    public function add($autodate = true, $null_values = false)
    {
        return parent::add($autodate, $null_values);
    }

    public function delete()
    {
        if ((int)$this->id_order === 0) {
            return false;
        }
        return parent::delete();
    }

    public function update($null_values = false)
    {
        if ((int)$this->id_order === 0) {
            return false;
        }
        return parent::update($null_values);
    }

    public static function getGAClientIdByIdOrder($id_order)
    {
        return Db::getInstance()->getValue('
            SELECT `client_id` 
            FROM `'._DB_PREFIX_.'pdgoogleanalytycenhancedecommercepro` 
            WHERE `id_order` = '.(int)$id_order
        );
    }

    public static function getOrdersToSendByIdOrder($id_order)
    {
        $res = Db::getInstance()->getRow('
            SELECT `order_send` 
            FROM `'._DB_PREFIX_.'pdgoogleanalytycenhancedecommercepro` 
            WHERE `id_order` = '.(int)$id_order
        );
        if (isset($res['order_send'])) {
            return $res['order_send'];
        } else {
            return 2;
        }
    }

    public static function getOrdersToSendRefundByIdOrder($id_order)
    {
        $res = Db::getInstance()->getRow('
            SELECT `refund_send` 
            FROM `'._DB_PREFIX_.'pdgoogleanalytycenhancedecommercepro` 
            WHERE `id_order` = '.(int)$id_order
        );
        if (isset($res['refund_send'])) {
            return $res['refund_send'];
        } else {
            return 2;
        }
    }

    public static function installDB()
    {
        return Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pdgoogleanalytycenhancedecommercepro` (
                `id_order` int(11) unsigned NOT NULL,
                `order_send` tinyint(1) NOT NULL DEFAULT \'0\',
                `refund_send` tinyint(1) NOT NULL DEFAULT \'0\',
                `client_id` varchar(64) NOT NULL,
                `date_add` datetime,
                `date_upd` datetime,
                PRIMARY KEY (`id_order`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;
        ');
    }

    public static function uninstallDB()
    {
        return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'pdgoogleanalytycenhancedecommercepro`');
    }
}
