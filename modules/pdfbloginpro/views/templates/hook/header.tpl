{*
* 2012-2017 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - Facebook login Pro © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek PrestaDev.pl <info@prestadev.pl>
* @copyright 2012-2017 Patryk Marek - PrestaDev.pl
* @license   License is for use in domain / or one multistore enviroment (do not modify or reuse this code or part of it) if you want any changes please contact with me at info@prestadev.pl
* @link      http://prestadev.pl
* @package   Facebook login Pro - PrestaShop 1.5.x and 1.6.x Module
* @version   1.1.0
* @date      10-06-2016
*}
<div id="fb-root"></div>
<script>
	{literal}

			var redirect = '{/literal}{$fb_redirect|escape:'htmlall':'UTF-8'}{literal}';
			if (typeof window.fbAsyncInit === 'undefined') {
				window.fbAsyncInit = function() {
					FB.init({
						appId: '{/literal}{$fb_app_id|escape:'htmlall':'UTF-8'}{literal}',
						autoLogAppEvents : true,
						scope: 'email, user_birthday',
						cookie: true,
						status: true,
						xfbml: true,
						version: 'v8.0'
					});
				};
			}

			function PdFbLoginPro() {
				FB.api('/me?fields=email,birthday,first_name,last_name,gender', function(response) {
					$.ajax({

						type: "POST",
						url: pdfbloginpro_ajax_link,
						data: {
							firstname: response.first_name, 
							lastname: response.last_name, 
							email: response.email, 
							id: response.id, 
							gender: response.gender,
							birthday: response.birthday,
							response: response
						},
						success:  function(data){
							if(redirect == false)
								window.location.reload();
							if(redirect == true)
								window.location.href = "{/literal}{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}{literal}";	
						}
					});
				});
			}
			
			function fb_login(){
				FB.login(function(response) {
					if (response.authResponse) {
						access_token = response.authResponse.accessToken;
						user_id = response.authResponse.userID;
						PdFbLoginPro();
					}	
				},
				{
					scope: 'public_profile,email'
				});
			}

	{/literal}
</script>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/{$fb_lang_iso|escape:'html':'UTF-8'}/sdk.js"></script>