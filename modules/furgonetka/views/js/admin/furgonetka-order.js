$(document).ready(function() {
    $(document).on('click', '#furgonetka-order-get-iframe', function(e) {
        e.preventDefault();
        var that = $(this);
        var href = that.attr('href');
        $.ajax({
            type: 'POST',
            async: true,
            dataType: 'json',
            url: href,
            success: function(response) {
                if(response.status == 'error'){
                    showErrorMessage(response.error)
                    return;
                }else{
                    openIframe(response.html);
                }
            }
        });

    });

    function openIframe(html){
        $('body').append(html);
        $('#furgonetka-iframe').fadeIn();
    }
});
