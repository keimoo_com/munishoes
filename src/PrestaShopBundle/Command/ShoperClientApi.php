<?php

namespace PrestaShopBundle\Command;

use GuzzleHttp\Client;

class ShoperClientApi
{
    private const DOMAIN = 'https://muni-shoes-61353.shoparena.pl';
    private const SIZE_ATTRIBUTE_ID = '9';

    public function getProducerNameById(int $id): string
    {
        $url = sprintf('%s/webapi/rest/producers/%d', self::DOMAIN, $id);
        $result = $this->getResultByUrl($url);

        return $result['name'];
    }

    public function getVariant(int $id): array
    {
        $url = sprintf('%s/webapi/rest/product-stocks/%d', self::DOMAIN, $id);
        $variant = $this->getResultByUrl($url);

        if (array_key_exists(self::SIZE_ATTRIBUTE_ID, $variant['options'])) {
            $idAttribute = (int)$variant['options'][self::SIZE_ATTRIBUTE_ID];
        } else {
            $idAttribute = reset($variant['options']);
        }

        return [
            'price' => $variant['price'],
            'code' => $variant['ean'],
            'stock' => $variant['stock'],
            'size' => $this->getSizeVariant($idAttribute),
            'default' => $variant['default']
        ];
    }

    public function getImagesProducts(int $id): array
    {
        $url = sprintf('%s/webapi/rest/product-images?filters={"product_id":"%d"}', self::DOMAIN, $id);
        $images = $this->getResultByUrl($url);

        $result = [];
        foreach ($images['list'] as $image) {
            $result[$image['order']] = [
                'gfxId' =>  $image['gfx_id'],
                'name' => $image['name'],
                'order' => (int)$image['order'],
                'extension' => $image['extension'],
                'main' => $image['main']
            ];
        }
        return $result;
    }

    public function getImagesProductsCount(int $id): int
    {
        $url = sprintf('%s/webapi/rest/product-images?filters={"product_id":"%d"}', self::DOMAIN, $id);
        $images = $this->getResultByUrl($url);

        return (int)$images['count'];
    }


    private function getSizeVariant(int $id): string
    {
        $url = sprintf('%s/webapi/rest/option-values/%d', self::DOMAIN, $id);
        $result = $this->getResultByUrl($url);

        return $result['translations']['pl_PL']['value'];
    }

    public function getProductById(int $id): array
    {
        $url = sprintf('%s/webapi/rest/products/%d', self::DOMAIN, $id);
        return $this->getResultByUrl($url);
    }

    public function getCategoryNameById(int $id): string
    {
        $url = sprintf('%s/webapi/rest/categories/%d', self::DOMAIN, $id);
        $result = $this->getResultByUrl($url);

        return $result['translations']['pl_PL']['name'];
    }

    private function getResultByUrl(string $url): ?array
    {
        sleep(1);
        try {
            $client = new Client();
            $result = $client->get(
                $url,
                [
                    'headers' => [
                        'Authorization' => 'Bearer 8c071091da94d525bc2c0138a85948201ab5bc27'
                    ],
                ]
            );

            return json_decode($result->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die;
        }
    }

    public function findRootCategories(array $categories): array
    {
        $rootCategories = [];

        foreach ($categories as $category) {
            $url = sprintf('%s/webapi/rest/categories-tree/%d', self::DOMAIN, $category);
            $result = $this->getResultByUrl($url);

            if (empty($result['children'])) {
                continue;
            }

            $rootCategories[] = [
                'rootId' => $category,
                'children' => $result['children']
            ];
        }

        $child = [];
        foreach ($rootCategories as $root) {
            foreach ($root['children'] as $children) {
                if (in_array($children['id'], $categories, true)) {
                     $child[] = [
                         'name' => $this->getCategoryNameById($children['id']),
                         'rootNameCategory' => $this->getCategoryNameById($root['rootId'])
                     ];
                }
            }
        }

        return $child;
    }
}
