$( document ).ready(function(){
    if(typeof loadCarriers === 'function') {
        window.parentLoadCarriers = loadCarriers;
        window.loadCarriers = function () {
            parentLoadCarriers();
            buttonManage();
        }
    }
    if(typeof placeOrder === 'function') {
        window.parentPlaceOrde = placeOrder;
        window.placeOrder = function () {
            validate = validateCarriers();
            if (true !== validate) {
                alert(validate);
                return false;
            }
            parentPlaceOrde();
        }
    }

});

$(document).on('click', '#furgonetka-set-point', function(e){
    e.preventDefault();
    showMap()
});

$(document).on('change', '.delivery_option_radio', function(){
    buttonManage();
});

function validateCarriers(){
    var id_delivery = getIdDelivery();
    var validate = true;
    if (!$('#furgonetka-machine-' + id_delivery).length){
        $.ajax({
            type: 'POST',
            url: furgonetkaCheckMapAjax,
            async: false,
            dataType: 'json',
            data: {id_delivery : id_delivery},
            success: function(jsonData) {
                if(jsonData.status == 'error'){
                    validate = jsonData.message
                }else{
                    validate = true;
                }
            },
        });
    }
    return validate;
}

function getIdDelivery(){
    var id_delivery = parseInt($('input.delivery_option_radio:checked').val());
    if('NaN' == id_delivery){
        return 0;
    }
    return id_delivery;
}


function buttonManage(){
    id_delivery = getIdDelivery();
    var button = $('#furgonetka-set-point');
    if (!$('#furgonetka-delivery-' + id_delivery).length) {
        button.hide();
    }else{
        button.show();
    }
    showMachine();
}

function showMap(){
    id_delivery = getIdDelivery();
    var service = $('#furgonetka-delivery-' + id_delivery).val().split(',');
    var cityMain = $('#furgonetka-city').val();
    var streetMain = $('#furgonetka-street').val();
    var city = $("input[name='shipping_address[city]']:visible").val();
    var street = $("input[name='shipping_address[address1]']:visible").val();
    if(typeof city === 'undefined'){
        var cityMap = cityMain;
    }else{
        var cityMap = city;
    }
    if(typeof street === 'undefined'){
        var streetMap = streetMain;
    }else{
        var streetMap = street;
    }
    if(typeof FurgonetkaBiznesMap === 'function'){
        new FurgonetkaBiznesMap({service: service,city: cityMap, street: streetMap, 'callback': furgonetkaMapCallback}).view();
    }
}

function showMachine(){
    id_delivery = getIdDelivery();
    $('.furgonetka-machine').addClass('hidden');
    var machine_name = $('#furgonetka-machine-' + id_delivery);
    if (machine_name.length) {
        machine_name.removeClass('hidden');
    }

}

function furgonetkaMapCallback(response){
    id_delivery = getIdDelivery();
    $.ajax({
        type: 'POST',
        url: furgonetkaMapAjax,
        async: true,
        dataType: 'json',
        data: {id_delivery : id_delivery, machine_code: response.code, machine_name : response.name, machine_type: response.type},
        success: function(jsonData) {
            if(jsonData.status == 'error'){
                jAlert(jsonData.message)
            }else{
                var input = '<input disabled class="furgonetka-machine" id="furgonetka-machine-' + id_delivery +'" value="'+ response.name +'">'
                $('#furgonetka-machine').html(input);
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
        }
    });
}
