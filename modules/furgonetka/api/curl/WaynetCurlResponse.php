<?php
class WaynetCurlResponse
{
    private $code;
    private $response;

    public function __construct($code, $response = null)
    {
        $this->code = $code;
        $this->response = $response;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
