<?php

namespace PrestaShopBundle\Command;

use Address;
use Customer;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Response;
use Tools;

class ShoploClientImportCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Connection
     */
    private $conn;
    private $output;
    private $input;

    private $errorEmail = [];


    protected function configure()
    {
        // The name of the command (the part after "bin/console")
        $this
            ->setName('shoplo:client:import')
            ->setDescription('Shoplo client import');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->input = $input;

        $outputStyle = new OutputFormatterStyle('blue', null);
        $this->output->getFormatter()->setStyle('blue', $outputStyle);

        $this->output->writeln(sprintf('<fg=green>Start migration client!</>'));

        try {
            $handle = fopen(__DIR__ . "/../../../csv_import_file/clients.csv", "r");
            $count = 0;

            while (($data = fgetcsv($handle, 3500)) !== FALSE) {
                if ($count === 0) {
                    $count++;
                    continue;
                }

                $this->runMigration($data);

                $count++;
            }

            $this->output->writeln('<danger>All Export done!</danger>');
            $this->output->writeln('<error>Not save</error>');
            print_r($this->errorEmail);

        } catch (\Exception $exception) {
            var_dump($exception->getMessage());
            die;
        }


        // Here your business logic.
        $this->output->writeln('<info>All Export done!</info>');
    }

    private function runMigration(array $customerRaw): void
    {
        $position = ['add_date' => 1, 'first_name' => 1, 'last_name' => 2, 'email' => 3, 'active' => 4, 'address' => 5, 'city' => 6, 'post_code' => 7, 'country' => 8, 'phone' => 9];

        $addressRaw  = $customerRaw[$position['address']];
        if ($addressRaw === '') {
            $addressRaw = $customerRaw[$position['post_code']] . ' ' . $customerRaw[$position['city']];
        }


        try {
            $customer = new Customer();
            $customer->firstname = $customerRaw[$position['first_name']];
            $customer->lastname = $customerRaw[$position['last_name']];
            $customer->email = $customerRaw[$position['email']];
            $customer->date_add = $customerRaw[$position['add_date']];
            $customer->passwd = Tools::passwdGen();
            $customer->newsletter = 1;
            $customer->active = 1;
            $customer->save();

            $address = new Address();

            $address->firstname = $customerRaw[$position['first_name']];
            $address->lastname = $customerRaw[$position['last_name']];
            $address->id_country = 14; // PL - country
            $address->city = $customerRaw[$position['city']] === '' ? '-' : $customerRaw[$position['city']];
            $address->alias = $customerRaw[$position['email']];
            $address->address1 = $addressRaw === '' ? '-' : $addressRaw;
            $address->postcode = $customerRaw[$position['post_code']];
            $address->id_customer = $customer->id;

            $address->save();

            $this->output->writeln(sprintf('<fg=blue>User %s added.</>',$customerRaw[$position['email']] ));

        } catch (\Exception $exception) {
            $this->errorEmail[] = $customerRaw[$position['email']];
            var_dump($exception->getMessage());
            $this->output->writeln(sprintf('<error>Not save customer %s </error>',$customerRaw[$position['email']] ));
            return;
        }
    }




}

