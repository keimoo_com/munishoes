<?php
/**
* 2012-2017 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - Facebook login Pro © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek PrestaDev.pl <info@prestadev.pl>
* @copyright 2012-2017 Patryk Marek - PrestaDev.pl
* @license   License is for use in domain / or one multistore enviroment (do not modify or reuse this code or part of it) if you want any changes please contact with me at info@prestadev.pl
* @link      http://prestadev.pl
* @package   Facebook login Pro - PrestaShop 1.5.x and 1.6.x Module
* @version   1.1.0
* @date      10-06-2016
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once(dirname(__FILE__).'/models/CustomersModel.php');

class PdFbLoginPro extends Module
{
    public $_postErrors = array();
    public $_html = '';


    public function __construct()
    {
        $this->name = 'pdfbloginpro';
        $this->tab = 'front_office_features';
        $this->version = '1.1.3';
        $this->author = 'PrestaDev.pl';
        $this->need_instance = 0;
        $this->bootstrap = true;
        $this->module_key = '5a244a399fb021ea1c9cb8273c616der';

        parent::__construct();

        $this->displayName = $this->l('Facebook login Pro');
        $this->description = $this->l('Allows login to store trought Facebook account');

        $this->prefix = 'PD_FLP_';
        $this->ps_ver_15 = (version_compare(Tools::substr(_PS_VERSION_, 0, 3), '1.5', '=')) ? true : false;
        $this->ps_ver_16 = (version_compare(Tools::substr(_PS_VERSION_, 0, 3), '1.6', '=')) ? true : false;

        $this->fb_redirect = (bool)Configuration::get($this->prefix.'REDIRECT');
        $this->fb_active = (bool)Configuration::get($this->prefix.'ON');
        $this->fb_app_id = (string)Configuration::get($this->prefix.'APP_ID');
        $this->fb_position = (int)Configuration::get($this->prefix.'POSITION');
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }
        
        if (!parent::install()
            || !$this->registerHook('header')
            || !$this->registerHook('displayNav')
            || !$this->registerHook('displayNav1')
            || !$this->registerHook('top')
            || !$this->registerHook('rightColumn')
            || !$this->registerHook('leftColumn')
            || !$this->registerHook('footer')
            || !$this->registerHook('displayCustomHookFbLogin')
            || !Configuration::updateValue($this->prefix.'REDIRECT', 0)
            || !Configuration::updateValue($this->prefix.'ON', 0)
            || !Configuration::updateValue($this->prefix.'APP_ID', '')
            || !Configuration::updateValue($this->prefix.'POSITION', 1)
            || !PdFbLoginProCustomersModel::createTables()) {
            return false;
        } else {
            return true;
        }
    }

    public function uninstall()
    {
        PdFbLoginProCustomersModel::dropTables();
        return parent::uninstall();
    }


    public function getContent()
    {
        if (Tools::isSubmit('btnSubmit')) {
            $this->_postValidation();
            if (!count($this->_postErrors)) {
                $this->_postProcess();
            } else {
                foreach ($this->_postErrors as $err) {
                    $this->_html .= $this->displayError($err);
                }
            }
        } else {
            $this->_html .= '<br />';
        }
        
        $this->_html .= $this->renderForm();
        return $this->_html;
    }


    private function _postValidation()
    {
        if (Tools::isSubmit('btnSubmit')) {
            if (!Tools::getValue($this->prefix.'APP_ID')) {
                $this->_postErrors[] = $this->l('Please provide Facebook App ID');
            }
        }
    }


    private function _postProcess()
    {
        if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue($this->prefix.'REDIRECT', Tools::getValue($this->prefix.'REDIRECT'));
            Configuration::updateValue($this->prefix.'ON', Tools::getValue($this->prefix.'ON'));
            Configuration::updateValue($this->prefix.'APP_ID', Tools::getValue($this->prefix.'APP_ID'));
            Configuration::updateValue($this->prefix.'POSITION', Tools::getValue($this->prefix.'POSITION'));
        }

        $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
    }


    public function renderForm()
    {
        $switch = version_compare(_PS_VERSION_, '1.6.0', '>=') ? 'switch' : 'radio';

        $fields_form_1 = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Module configuration'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => $switch,
                        'label' => $this->l('Enable'),
                        'name' => $this->prefix.'ON',
                        'desc' => $this->l('Enable / diable login functionality'),
                        'values' => array(
                            array(
                                'id' => 'on',
                                'value' => 1,
                                'label' => $this->l('Enable')
                            ),
                            array(
                                'id' => 'off',
                                'value' => 0,
                                'label' => $this->l('Disable')
                            ),
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('App ID'),
                        'lang' => false,
                        'desc' => $this->l('Facebook aplication identyficator'),
                        'name' => $this->prefix.'APP_ID',
                        'required' => true
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Login button display possition'),
                        'name' => $this->prefix.'POSITION',
                        'width' => 300,
                        'class' => 'fixed-width-ld',
                        'desc' => $this->l('You can select position to display fb login {hook h=\'displayCustomHookFbLogin\'}'),
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => '1',
                                    'name' => $this->l('Display top hook (displayTop)')
                                ),
                                array(
                                    'id' => '2',
                                    'name' => $this->l('Display navigation top hook (displayNav)')
                                ),
                                array(
                                    'id' => '6',
                                    'name' => $this->l('Display navigation top hook (displayNav1)')
                                ),
                                array(
                                    'id' => '3',
                                    'name' => $this->l('Display left column hook (displayLeft)')
                                ),
                                array(
                                    'id' => '4',
                                    'name' => $this->l('Display right column hook (displayRight)')
                                ),
                                array(
                                    'id' => '5',
                                    'name' => $this->l('Display custom hook (displayCustomHookFbLogin)')
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name',
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Redirection after login'),
                        'name' => $this->prefix.'REDIRECT',
                        'width' => 300,
                        'class' => 'fixed-width-ld',
                        'desc' => $this->l('You can choose if module should redirect after login or should stay on same page'),
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => '0',
                                    'name' => $this->l('No redirect just login')
                                ),
                                array(
                                    'id' => '1',
                                    'name' => $this->l('Redirect after login to my account page')
                                ),
                               
                            ),
                            'id' => 'id',
                            'name' => 'name',
                        )
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );
                    
      
        $helper = new HelperForm();
        $helper->module = $this;
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form_1));
    }
    

    public function getConfigFieldsValues()
    {
        $return = array();
        $return[$this->prefix.'REDIRECT'] = Tools::getValue($this->prefix.'REDIRECT', Configuration::get($this->prefix.'REDIRECT'));
        $return[$this->prefix.'ON'] = Tools::getValue($this->prefix.'ON', Configuration::get($this->prefix.'ON'));
        $return[$this->prefix.'APP_ID'] = Tools::getValue($this->prefix.'APP_ID', Configuration::get($this->prefix.'APP_ID'));
        $return[$this->prefix.'POSITION'] = Tools::getValue($this->prefix.'POSITION', Configuration::get($this->prefix.'POSITION'));
        return $return;
    }



    public function hookDisplayHeader()
    {

        Media::addJsDef(array(
            'pdfbloginpro_ajax_link' => $this->context->link->getModuleLink('pdfbloginpro', 'ajax', array()),
        ));

        $customer_is_logged = $this->context->customer->isLogged();
        if ($this->fb_active && $this->fb_app_id && !$customer_is_logged) {
            $this->context->controller->addCSS(($this->_path).'views/css/styles.css', 'all');
            $this->context->smarty->assign(array(
                'fb_app_id' => $this->fb_app_id,
                'fb_lang_iso' => $this->context->language->iso_code.'_'.strtoupper($this->context->country->iso_code),
                'fb_redirect' => $this->fb_redirect
            ));
            
            return $this->display(__FILE__, 'header.tpl');
        }
    }


    public function hookTop($params)
    {
        $customer_is_logged = $this->context->customer->isLogged();
        if ($this->fb_active && $this->fb_app_id && !$customer_is_logged && $this->fb_position == 1) {
            $this->context->smarty->assign(array(
                'fb_app_id' => $this->fb_app_id,
                'fb_redirect' => $this->fb_redirect
            ));
            return $this->display(__FILE__, 'hookNav.tpl');
        }
    }

    public function hookDisplayNav($params)
    {
        $customer_is_logged = $this->context->customer->isLogged();
        if ($this->fb_active && $this->fb_app_id && !$customer_is_logged && $this->fb_position == 2) {
            $this->context->smarty->assign(array(
                'fb_app_id' => $this->fb_app_id,
                'fb_redirect' => $this->fb_redirect
            ));
           
            return $this->display(__FILE__, 'hookNav.tpl');
        }
    }

    public function hookDisplayNav1($params)
    {
        $customer_is_logged = $this->context->customer->isLogged();
        if ($this->fb_active && $this->fb_app_id && !$customer_is_logged && $this->fb_position == 6) {
            $this->context->smarty->assign(array(
                'fb_app_id' => $this->fb_app_id,
                'fb_redirect' => $this->fb_redirect
            ));
           
            return $this->display(__FILE__, 'hookNav.tpl');
        }
    }

    public function hookLeftColumn($params)
    {
        $customer_is_logged = $this->context->customer->isLogged();
        if ($this->fb_active && $this->fb_app_id && !$customer_is_logged && $this->fb_position == 3) {
            $this->context->smarty->assign(array(
                'fb_app_id' => $this->fb_app_id,
                'fb_redirect' => $this->fb_redirect
            ));
        
            return $this->display(__FILE__, 'leftRightColumn.tpl');
        }
    }
    
    public function hookRightColumn($params)
    {
        $customer_is_logged = $this->context->customer->isLogged();
        if ($this->fb_active && $this->fb_app_id && !$customer_is_logged && $this->fb_position == 4) {
            $this->context->smarty->assign(array(
                'fb_app_id' => $this->fb_app_id,
                'fb_redirect' => $this->fb_redirect
            ));
        
            return $this->display(__FILE__, 'leftRightColumn.tpl');
        }
    }

    public function hookDisplayCustomHookFbLogin($params)
    {
        $customer_is_logged = $this->context->customer->isLogged();
        if ($this->fb_active && $this->fb_app_id && !$customer_is_logged && $this->fb_position == 5) {
            $this->context->smarty->assign(array(
                'fb_app_id' => $this->fb_app_id,
                'fb_redirect' => $this->fb_redirect
            ));
        
            return $this->display(__FILE__, 'customHook.tpl');
        }
    }

    public function sendConfirmationMail(Customer $customer, $password)
    {
        if (!Configuration::get('PS_CUSTOMER_CREATION_EMAIL')) {
            return true;
        }
        $tpl_vars = array(
            '{firstname}' => $customer->firstname,
            '{lastname}' => $customer->lastname,
            '{email}' => $customer->email,
            '{passwd}' => $password,
        );

        return Mail::Send(
            $this->context->language->id,
            'account',
            Mail::l('Welcome!'),
            $tpl_vars,
            $customer->email,
            $customer->firstname.' '.$customer->lastname
        );
    }
}
