<?php
require_once 'api/soap/SoapFurgonetka.php';
require_once 'classes/FurgonetkaAuth.php';
require_once 'classes/PrestaApi.php';
require_once 'classes/FurgonetkaDb.php';
require_once 'classes/FurgonetkaDelivery.php';

class furgonetka extends Module
{
    const INPOST = 'inpost';
    const KIOSK = 'kiosk';
    const POCZTA = 'poczta';
    const UAP = 'uap';
    const DPD = 'dpd';
    const PARCELSHOP = 'parcelshop';
    const FEDEX = 'fedex';

    const MODULE_ONEPAGE_17 = 'module_onepage_checkout_17';
    const MODULE_SUPER_17 = 'module_super_checkout_17';
    const PRESTA_17 = 'presta_17';

    const MODULE_ONEPAGE_16 ='module_onepage_checkout_16';
    const MODULE_SUPER_16 = 'module_super_checkout_16';
    const PRESTA_16 = 'presta_16';

    private $env;

    public function __construct()
    {
        $this->name = 'furgonetka';
        $this->tab = 'others';
        $this->version = '1.3.5';
        $this->author = 'Waynet';
        $this->need_instance = 0;
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Furgonetka');
        $this->description = $this->l('Integracja z Furgonetka.pl');
        $this->ps_versions_compliancy = array('min' => '1.6.1', 'max' => _PS_VERSION_);
        $this->setEnvironment();
    }

    public function install()
    {
        return parent::install() && $this->registerHooks() && $this->installSqls();
    }

    public function registerHooks()
    {
        $return = true;
        $return &= $this->registerHook('actionAdminControllerSetMedia');
        $return &= $this->registerHook('displayAdminOrder');
        $return &= $this->registerHook('header');
        if (Tools::version_compare(_PS_VERSION_, '1.7', '>=')){
            $return &= $this->registerHook('displayAfterCarrier');
        }else{
            $return &= $this->registerHook('displayCarrierList');
        }
        return $return;
    }

    public function installSqls()
    {
        $return = true;
        $return &= FurgonetkaDb::createTable(
            'furgonetka_cart_delivery_machine',
            array(
                'id_cart' => 'INT(11) NOT NULL',
                'id_delivery' => 'INT(11) NOT NULL',
                'machine_code' => 'VARCHAR(255) NOT NULL',
                'machine_name' => 'VARCHAR(255) NOT NULL',
                'machine_type' => 'VARCHAR(255) NOT NULL',
            ),
            array('id_cart'),
            'InnoDB'
        );
        $return &= FurgonetkaDb::createTable(
            'furgonetka_machine_type',
            array(
                'id_machine_type' => 'INT(11) NOT NULL AUTO_INCREMENT',
                'machine' => 'VARCHAR(255) NOT NULL',
            ),
            array('id_machine_type'),
            'InnoDB'
        );

        $return &= FurgonetkaDb::createTable(
            'furgonetka_carrier_machine_type',
            array(
                'carrier_reference' => 'INT(11) NOT NULL',
                'id_machine_type' => 'INT(11) NOT NULL',
            ),
            array('carrier_reference', 'id_machine_type'),
            'InnoDB'
        );

        foreach ($this->getTypes() as $name => $key) {
            $typeMachines[] = array('id_machine_type' => $key, 'machine' => $name);
        }

        Db::getInstance()->insert(
            'furgonetka_machine_type',
            $typeMachines,
            false,
            true,
            Db::INSERT_IGNORE
            );

        FurgonetkaDb::addAdminController('AdminFurgonetka', $this->name);
        return $return;
    }

    public function getContent()
    {
        if(Tools::isSubmit('saveFurgonetkaConfiguration')){
            return $this->saveForm(). $this->getForms();
        }elseif(Tools::isSubmit('FurgonetkaConnect')){
           return $this->connectFurgonetka(Tools::getValue('driver')). $this->getForms();
        }elseif(Tools::isSubmit('FurgonetkaReset')){
            return $this->resetConnectFurgonetka(Tools::getValue('driver')). $this->getForms();
        }elseif(Tools::isSubmit('saveFurgonetkaCarriersConfiguration')){
            return $this->saveFormCarriers(). $this->getForms();
        }

       return $this->getForms();
    }

    public function getForms()
    {
        return $this->getList(). $this->getForm(). $this->getFormCarriers();
    }

    public function displayConnectLink($token = null, $id, $name = null)
    {
        $link = $this->context->link->getAdminLink('AdminModules', true)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' .
            $this->name.'&FurgonetkaConnect&driver='.$id;
        return  '<a class="furgonetka-connect btn btn-default" href="'.$link.'">'.$this->l('Połącz').'</a>';
    }

    public function displayResetLink($token = null, $id, $name = null)
    {
        $link = $this->context->link->getAdminLink('AdminModules', true)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' .
            $this->name.'&FurgonetkaReset&driver='.$id;
        return  '<a class="furgonetka-reset" href="'.$link.'">'.$this->l('Resetuj').'</a>';
    }

    private function getList()
    {
        $values = array(
            array(
                'type' => 'Production',
                'name' => $this->l('Środowisko produkcyjne ').
                    '<a href="https://furgonetka.pl/">(https://furgonetka.pl/)</a>',
                'status' => 'prod'
            ),
            array(
                'type' => 'Test',
                'name' => $this->l('Środowisko testowe ').
                    '<a href="https://test.furgonetka.pl/">(https://test.furgonetka.pl/)</a>',
                'status' => 'test'
            ),
        );

        $this->fields_list = array(
            'type' =>
                array(
                    'title' => $this->l('Typ'),
                    'class' => 'hidden',
                    'type' => 'text'
                ),
            'name' =>
                array(
                    'title' => $this->l('Typ'),
                    'type' => 'text',
                    'callback' => 'getName',
                    'callback_object' => 'furgonetka',
                ),
            'status' =>
                array(
                    'title' => $this->l('Status'),
                    'type' => 'text',
                    'callback' => 'getStatus',
                    'callback_object' => 'furgonetka'
                ),
        );
        $helper = new HelperList();
        $helper->title = $this->l('Środowisko');
        $helper->table = 'Environment';
        $helper->identifier = 'type';
        $helper->actions = array('connect', 'reset');
        $helper->no_link = true;
        $helper->shopLinkType = '';
        $helper->module = $this;
        $helper->tpl_vars = array('show_filters' => false,);
        $helper->listTotal = 2;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', true)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' .
            $this->name;
        return $helper->generateList($values, $this->fields_list);
    }

    private function getForm()
    {
        $countries[] = array('iso_code' => false, 'name' => $this->l('Wybierz kraj'));
        $countries = array_merge($countries, Country::getCountries($this->context->language->id));

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'tinymce' => false,
                    'title' => $this->l('Dane nadawcy'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Imię i nazwisko'),
                        'name' => 'furgonetka-name',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Nazwa firmy'),
                        'name' => 'furgonetka-company',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Kod pocztowy'),
                        'name' => 'furgonetka-postcode',
                        'required' => true,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Miasto'),
                        'name' => 'furgonetka-city',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Ulica i numer'),
                        'name' => 'furgonetka-street',
                        'required' => true,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('E-mail'),
                        'name' => 'furgonetka-email',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Telefon'),
                        'name' => 'furgonetka-phone',
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Kraj'),
                        'name' => 'furgonetka-country',
                        'options' => array(
                            'query' => $this->getFurgonetkaCountry(),
                            'id' => 'iso_code',
                            'name' => 'name',
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Numer konta bankowego dla przesyłek pobraniowych'),
                        'name' => 'furgonetka-iban',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Zapisz'),
                ),
            ),
        );

        $helper = new HelperForm();
        $helper->tpl_vars['fields_value']['furgonetka-street'] = Configuration::getGlobalValue('FURGONETKA_STREET');
        $helper->tpl_vars['fields_value']['furgonetka-postcode'] = Configuration::getGlobalValue('FURGONETKA_POSTCODE');
        $helper->tpl_vars['fields_value']['furgonetka-city'] = Configuration::getGlobalValue('FURGONETKA_CITY');
        $helper->tpl_vars['fields_value']['furgonetka-name'] = Configuration::getGlobalValue('FURGONETKA_NAME');
        $helper->tpl_vars['fields_value']['furgonetka-email'] = Configuration::getGlobalValue('FURGONETKA_EMAIL');
        $helper->tpl_vars['fields_value']['furgonetka-company'] = Configuration::getGlobalValue('FURGONETKA_COMPANY');
        $helper->tpl_vars['fields_value']['furgonetka-phone'] = Configuration::getGlobalValue('FURGONETKA_PHONE');
        $helper->tpl_vars['fields_value']['furgonetka-country'] = Configuration::getGlobalValue('FURGONETKA_COUNTRY');
        $helper->tpl_vars['fields_value']['furgonetka-iban'] = Configuration::getGlobalValue('FURGONETKA_IBAN');
        $helper->show_toolbar = false;
        $helper->table = 'Furgonetka';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', true) . '&configure=' .
            $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->submit_action = 'saveFurgonetkaConfiguration';
        $helper->identifier = $this->identifier;

        return $helper->generateForm(array($fields_form));
    }

    private function saveForm()
    {
        $street = Tools::getValue('furgonetka-street');
        $postcode = Tools::getValue('furgonetka-postcode');
        $city = Tools::getValue('furgonetka-city');
        $name = Tools::getValue('furgonetka-name');
        $email = Tools::getValue('furgonetka-email');
        $company = Tools::getValue('furgonetka-company');
        $phone = Tools::getValue('furgonetka-phone');
        $country = Tools::getValue('furgonetka-country');
        $iban = Tools::getValue('furgonetka-iban');

        if(
            empty($street)
            || empty($postcode)
            || empty($city)
        ){
            return $this->displayError($this->l('Proszę uzupełnić wymagane pola.'));
        }

        Configuration::updateGlobalValue('FURGONETKA_STREET', $street);
        Configuration::updateGlobalValue('FURGONETKA_POSTCODE', $postcode);
        Configuration::updateGlobalValue('FURGONETKA_CITY', $city);
        Configuration::updateGlobalValue('FURGONETKA_NAME', $name);
        Configuration::updateGlobalValue('FURGONETKA_EMAIL', $email);
        Configuration::updateGlobalValue('FURGONETKA_COMPANY', $company);
        Configuration::updateGlobalValue('FURGONETKA_PHONE', $phone);
        Configuration::updateGlobalValue('FURGONETKA_COUNTRY', $country);
        Configuration::updateGlobalValue('FURGONETKA_IBAN', $iban);

        return $this->displayConfirmation($this->l('Zapisz konfigurację'));

    }

    public function saveFormCarriers()
    {
        FurgonetkaDb::clearCarrierMachine();
        foreach (FurgonetkaDb::getTypes() as $name => $id) {
            if(Tools::getValue($name)){
                FurgonetkaDb::addCarriersType($id, Tools::getValue($name));
            }
        }

    }

    public function getFormCarriers()
    {
        $carriers = Carrier::getCarriers(
            $this->context->language->id,
            true,
            false,
            false,
            null,
            Carrier::ALL_CARRIERS
            );

        $types = FurgonetkaDb::getTypeCarriers();
        $helper = new HelperForm();
        $inputs [] = array(
            'type' => 'free',
            'name' => 'info',
        );

        $helper->tpl_vars['fields_value']['info'] =
            '<style>'
            .'@media(min-width: 1200px){.furgonetka-label {margin-left: -33%}};'
            .'</style>'
            .'<span class="furgonetka-label">'
            .$this->l('Wybierz mapę punktów, która będzie wyświetlana dla poszczególnych sposobów dostawy.')
            .'</span>';

        foreach (FurgonetkaDb::getTypes() as $name => $id) {
           $inputs[] = array(
                'type' => 'select',
                'label' => $this->getLabelType($name),
                'name' => $name . '[]',
                'class' => 'chosen',
                'multiple' => true,
                'options' => array(
                    'query' => $carriers,
                    'id' => 'id_reference',
                    'name' => 'name'
                ),
            );
            $helper->tpl_vars['fields_value'][$name.'[]'] = isset($types[$id])? $types[$id] : array();
        }

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'tinymce' => false,
                    'title' => $this->l('Konfiguracja map dla sposobów dostawy'),
                    'icon' => 'icon-cogs'
                ),
                'input' => $inputs,
                'submit' => array(
                    'title' => $this->l('Zapisz'),
                ),
            ),
        );


        $helper->show_toolbar = false;
        $helper->table = 'furgonetka_carrier_machine_type';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', true) . '&configure=' .
            $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->submit_action = 'saveFurgonetkaCarriersConfiguration';
        $helper->identifier = $this->identifier;

        return $helper->generateForm(array($fields_form));
    }



    private function getLabelType($type)
    {
        $labels = array(
            self::INPOST => $this->l('Dodaj mapę InPost do:'),
            self::KIOSK => $this->l('Dodaj mapę Paczki w Ruchu do:'),
            self::POCZTA => $this->l('Dodaj mapę Poczty Polskiej do:'),
            self::UAP => $this->l('Dodaj mapę UPS Access Point do:'),
            self::DPD => $this->l('Dodaj mapę DPD Pickup do:'),
            self::PARCELSHOP => $this->l('Dodaj mapę DHL Parcelshop do:'),
            self::FEDEX => $this->l('Dodaj mapę FedEx Punkt do:'),
        );

        return $labels[$type];
    }

    private function getTypes()
    {
        $types = array(
            self::INPOST => 1,
            self::KIOSK => 2,
            self::POCZTA => 3,
            self::UAP => 4,
            self::DPD => 5,
            self::PARCELSHOP => 6,
            self::FEDEX => 7,
        );

        return $types;
    }

    public static function getStatus($type)
    {
        $module = Module::getInstanceByName('furgonetka');

        $test = $type == 'test';
        if (self::checkConfiguration($test)) {
            return '<span style="font-weight: bold; color: green">'.$module->l('POŁĄCZONO').'</span>';
        }else{
            return '<span style="font-weight: bold; color: red">'.$module->l('NIE POŁĄCZONO').'</span>';
        }
    }

    public static function getName($name)
    {
        return $name;
    }

    private static function checkConfiguration($test)
    {
        $status = $test? 'TEST_' : '';
        $expire = Configuration::get($status.'FURGONETKA_REFRESH_TOKEN_EXPIRE');

        $notEmpty = !empty(Configuration::get($status.'FURGONETKA_CLIENT_ID'))
        && !empty(Configuration::get($status.'FURGONETKA_CLIENT_SECRET'))
        && !empty(Configuration::get($status.'FURGONETKA_ACCESS_TOKEN'))
        && !empty(Configuration::get($status.'FURGONETKA_REFRESH_TOKEN'))
        && !empty(Configuration::get($status.'FURGONETKA_SOURCE_ID'))
        && !empty($expire)
        && (Configuration::getGlobalValue('FURGONETKA_ENVIRONMENT') == ($test? 'TEST' : 'PROD'));

        if(!$notEmpty){
            return false;
        }
        $today = new Datetime();
        $expireDate = new DateTime($expire);
        return $notEmpty && ($today < $expireDate);

    }

    private function resetConnectFurgonetka($type)
    {
        if (!in_array($type, array('Production', 'Test'))) {
            $response = array('status' => 'error', 'error' => $this->l('Zły driver'));
            die(json_encode($response));
        }
        FurgonetkaDb::resetConnection($type == 'Test');
        $response = array('status' => 'ok');
        die(json_encode($response));
    }

    private function connectFurgonetka($type)
    {
        if (!in_array($type, array('Production', 'Test'))) {
            $response = array('status' => 'error', 'error' => $this->l('Zły driver'));
            die(json_encode($response));
        }
        $street = Configuration::getGlobalValue('FURGONETKA_STREET');
        $postcode = Configuration::getGlobalValue('FURGONETKA_POSTCODE');
        $city = Configuration::getGlobalValue('FURGONETKA_CITY');

        if (
            empty($street)
            || empty($postcode)
            || empty($city)
        ) {
            $response = array('status' => 'error', 'error' => $this->l('Proszę uzupełnić wymagane pola w sekcji dane nadawcy.'));
            die(json_encode($response));
        }


        $env = $type == 'Test';
        Configuration::updateGlobalValue('FURGONETKA_ENVIRONMENT', $env? 'TEST': 'PROD');

        if (self::checkConfiguration($env)) {
            $response = array('status' => 'ok');
            die(json_encode($response));
        }
        try {
            $furgonetka = new FurgonetkaAuth();
            $furgonetka->integrateAuth($env);
            $link = $furgonetka->getRedirectAuthorizationLink($env);
        } catch (Exception $e) {
            $response = ['status' => 'error', 'error' => $this->l('Błąd połączenia: ') . '<br>' . $e->getMessage()];
            die(json_encode($response));
        }

        $backUrl = $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . Tools::safeOutput('furgonetka');

        if (\strpos($backUrl, 'http') !== 0) {
            $adminFolder = \basename(_PS_ADMIN_DIR_);
            $backUrl = Tools::getShopDomainSsl(true) . \DIRECTORY_SEPARATOR . $adminFolder . \DIRECTORY_SEPARATOR . $backUrl;
        }

        setcookie('furgonetka_oauth_back_url', \urlencode($backUrl), time() + 300, '/', '', false, true);

        $this->context->smarty->assign(array('url' => $link));
        $response = json_encode(array('status' => 'iframe', 'html' => $this->display(
            __FILE__,
            '/views/templates/admin/furgonetka-oauth.tpl') ));
        die($response);
    }

    public function hookActionAdminControllerSetMedia($params)
    {
        $this->context->controller->addJS($this->_path.'views/js/admin/furgonetka-order.js', 'all');
        if (Tools::getValue('module_name') == $this->name || Tools::getValue('configure') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/admin/furgonetka-module.js', 'all');
        }
    }

    public function hookDisplayAdminOrder($params)
    {
        $order = new Order($params['id_order']);

//        $order = $params['order'];
        $link = $this->context->link->getAdminLink(
            'AdminFurgonetka',
            true)
            .'&ajax=1&action=getOrderIframe&id_order='.(int)$order->id;
        $machine = FurgonetkaDb::getMachineCodeFromOrder($order->id);
        $machineName = '';
        $machineCode = '';
        if(!empty($machine)){
            $machineName = $machine['machine_name'];
            $machineCode = $machine['machine_code'];
        }
        $this->context->smarty->assign(
            array(
                'machine_name' => $machineName,
                'machine_code' => $machineCode,
                'url' => $link,
            )
        );
        return $this->display(__FILE__,'/views/templates/admin/furgonetka-order-machine.tpl');
    }

    public function getOrderControllers()
    {
        return array(
            'supercheckout',
            'orderopc',
            'order'
        );
    }

    public function hookHeader()
    {
        $env = FurgonetkaAuth::getEnvironment();
        $status = $env? 'TEST_' : '';
        $date = Configuration::getGlobalValue($status.'FURGONETKA_MAIN_TOKEN_DATE_REFRESH');
        if(!empty($date)){
            $today = new DateTime();
            $date = new DateTime($date);
            if($date < $today){
                try{
                    $furgonetka = new FurgonetkaAuth();
                    $furgonetka->refreshToken($env);
                }catch (Exception $e){
                    PrestaShopLogger::addLog('Furgonetka error refresh token<br>'.$e->getMessage());
                }
            }
        }
        if(!isset($_GET['controller']) || !in_array($_GET['controller'], $this->getOrderControllers())){
            return;
        }
        switch ($this->env) {
            case self::MODULE_ONEPAGE_17:
                $this->context->controller->registerJavascript('furgonetka-module-onepage', 'modules/'.$this->name.'/views/js/front/17furgonetka-map-onepage.js');
                $this->context->controller->registerJavascript('furgonetka-map', 'https://konto.furgonetka.pl/js/plugins/map.min.js', array('server' => 'remote'));
                break;
            case self::MODULE_SUPER_17:
                $this->context->controller->registerJavascript('furgonetka-module-super', 'modules/'.$this->name.'/views/js/front/17furgonetka-map-super.js');
                $this->context->controller->registerJavascript('furgonetka-map', 'https://konto.furgonetka.pl/js/plugins/map.min.js', array('server' => 'remote'));
                break;
            case  self::PRESTA_17:
                $this->context->controller->registerJavascript('furgonetka-no-module', 'modules/'.$this->name.'/views/js/front/17furgonetka-map.js');
                $this->context->controller->registerJavascript('furgonetka-map', 'https://konto.furgonetka.pl/js/plugins/map.min.js', array('server' => 'remote'));
                break;
            case self::MODULE_ONEPAGE_16:
                $this->context->controller->addJS('https://konto.furgonetka.pl/js/plugins/map.min.js', 'all');
                $this->context->controller->addJS($this->_path.'views/js/front/furgonetka-map-onepage.js', 'all');
                break;
            case self::MODULE_SUPER_16:
                $this->context->controller->addJS('https://konto.furgonetka.pl/js/plugins/map.min.js', 'all');
                $this->context->controller->addJS($this->_path.'views/js/front/furgonetka-map-super.js', 'all');
                break;
            case  self::PRESTA_16:
                $this->context->controller->addJS('https://konto.furgonetka.pl/js/plugins/map.min.js', 'all');
                $this->context->controller->addJS($this->_path.'views/js/front/furgonetka-map.js', 'all');
                break;
        }

        $link = $this->context->link->getModuleLink
        (
            'furgonetka',
            'map',
            array('ajax' => 1, 'action' => 'saveMachine')
        );
        $linkValidate = $this->context->link->getModuleLink
        (
            'furgonetka',
            'map',
            array('ajax' => 1, 'action' => 'validate')
        );
        Media::addJsDef(array('furgonetkaMapAjax' => $link));
        Media::addJsDef(array('furgonetkaCheckMapAjax' => $linkValidate));
    }

    public function hookDisplayAfterCarrier($params)
    {
        $idAddress = $this->context->cart->id_address_delivery;
        $address = new Address($idAddress);
        $cartMachine = FurgonetkaDb::getCartDelivery($this->context->cart->id);
        $machineName =  '';
        $idDelivery =  '';
        if(!empty($cartMachine)){
            $machineName = $cartMachine['machine_name'];
            $idDelivery  = $cartMachine['id_delivery'];
        }

        $this->context->smarty->assign(
            array(
                'city' => $address->city,
                'street' => $address->address1,
                'machineName' => $machineName,
                'idDelivery' => $idDelivery,
                'carriersTypes' => FurgonetkaDb::getCarrierTypes(),
           )
                );
        if (Tools::version_compare(_PS_VERSION_, '1.7', '>=')){
            return $this->display(__FILE__, 'views/templates/front/17furgonetka-map.tpl');
        }
            return $this->display(__FILE__, 'views/templates/front/furgonetka-map.tpl');
    }

    public function hookDisplayCarrierList($params)
    {
        return $this->hookDisplayAfterCarrier($params);
    }

    public function setEnvironment()
    {
        if (Tools::version_compare(_PS_VERSION_, '1.7', '>=') && Module::isEnabled('onepagecheckoutps')) {
            $this->env = self::MODULE_ONEPAGE_17;
        }elseif(Tools::version_compare(_PS_VERSION_, '1.7', '>=') && Module::isEnabled('supercheckout')){
            $this->env = self::MODULE_SUPER_17;
        }elseif(Tools::version_compare(_PS_VERSION_, '1.7', '>=')){
            $this->env = self::PRESTA_17;
        }elseif(Tools::version_compare(_PS_VERSION_, '1.7', '<') && Module::isEnabled('onepagecheckoutps')){
            $this->env = self::MODULE_ONEPAGE_16;
        }elseif(Tools::version_compare(_PS_VERSION_, '1.7', '<') && Module::isEnabled('supercheckout')){
            $this->env = self::MODULE_SUPER_16;
        }elseif(Tools::version_compare(_PS_VERSION_, '1.7', '<')){
            $this->env = self::PRESTA_16;
        }

    }

    public function getTranslate($name)
    {
        $translate = array(
           'Choose machine' => $this->l('Wybierz punkt odbioru.'),
        );

        return isset($translate[$name])? $translate[$name] : $name;
    }

    public function getFurgonetkaCountry()
    {

       return  array(
           array('iso_code' => null, 'name' => $this->l('Wybierz kraj')),
           array('iso_code' => 'AT', 'name' => $this->l('Austria')),
           array('iso_code' => 'BE', 'name' => $this->l('Belgia')),
           array('iso_code' => 'BG', 'name' => $this->l('Bułgaria ')),
           array('iso_code' => 'CZ', 'name' => $this->l('Czechy')),
           array('iso_code' => 'DK', 'name' => $this->l('Dania')),
           array('iso_code' => 'EE', 'name' => $this->l('Estonia')),
           array('iso_code' => 'FI', 'name' => $this->l('Finlandia')),
           array('iso_code' => 'FR', 'name' => $this->l('Francja')),
           array('iso_code' => 'ES', 'name' => $this->l('Hiszpania')),
           array('iso_code' => 'NL', 'name' => $this->l('Holandia')),
           array('iso_code' => 'IE', 'name' => $this->l('Irlandia')),
           array('iso_code' => 'IP', 'name' => $this->l('Irlandia Północna')),
           array('iso_code' => 'LT', 'name' => $this->l('Litwa')),
           array('iso_code' => 'LU', 'name' => $this->l('Luksemburg')),
           array('iso_code' => 'LV', 'name' => $this->l('Łotwa')),
           array('iso_code' => 'DE', 'name' => $this->l('Niemcy')),
           array('iso_code' => 'PL', 'name' => $this->l('Polska')),
           array('iso_code' => 'PT', 'name' => $this->l('Portugalia')),
           array('iso_code' => 'RO', 'name' => $this->l('Rumunia')),
           array('iso_code' => 'SK', 'name' => $this->l('Słowacja')),
           array('iso_code' => 'SI', 'name' => $this->l('Słowenia')),
           array('iso_code' => 'SE', 'name' => $this->l('Szwecja')),
           array('iso_code' => 'HU', 'name' => $this->l('Węgry')),
           array('iso_code' => 'GB', 'name' => $this->l('Wielka Brytania')),
           array('iso_code' => 'IT', 'name' => $this->l('Włochy')),
           );

    }

}
