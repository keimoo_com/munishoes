<?php
/**
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*/
 
include_once dirname(__FILE__).'/../../models/PdGAEEPModel.php';

class AdminPdGoogleAnalytycEnhancedEcommerceProController extends AdminController
{
    public $module = null;
    public $module_name = 'pdgoogleanalytycenhancedecommercepro';

    public function __construct()
    {
        $this->table = 'pdgoogleanalytycenhancedecommercepro';
        $this->className = 'PdGAEEPModel';
        $this->identifier = 'id_order';

        $this->lang = false;
        $this->bootstrap = true;
        if (Module::isInstalled($this->module_name)) {
            $this->module = Module::getInstanceByName($this->module_name);
        }

        $this->_select = 'o.reference';
        $this->_join = 'LEFT JOIN '._DB_PREFIX_.'orders o ON (o.id_order = a.id_order)';

        parent::__construct();
        
        $this->allow_export = false;
        $this->ps_ver_17 = (version_compare(Tools::substr(_PS_VERSION_, 0, 3), '1.6', '=')) ? true : false;
        $this->context = Context::getContext();
        $this->default_form_language = $this->context->language->id;


        $this->fields_list = array(
            'id_order' => array(
                'title' => $this->l('Id order'),
                'align' => 'center',
                'filter' => false,
                'width' => 25
            ),
            'reference' => array(
                'title' => $this->l('Reference'),
                'align' => 'right',
                'filter' => true,
                'filter_key' => 'o!reference'
            ),
            'order_send' => array(
                'title' => $this->l('Order send'),
                'active' => 'status',
                'filter_key' => 'a!order_send',
                'align' => 'text-center',
                'type' => 'bool',
                'class' => 'fixed-width-sm',
                'orderby' => false
            ),
            'refund_send' => array(
                'title' => $this->l('Order refund'),
                'active' => 'status',
                'filter_key' => 'a!refund_send',
                'align' => 'text-center',
                'type' => 'bool',
                'class' => 'fixed-width-sm',
                'orderby' => false
            ),
            'date_add' => array(
                'title' => $this->l('Date add'),
                'align' => 'right',
                'width' => 'auto',
                'type' => 'datetime',
                'filter_key' => 'a!date_add'
            ),
            'date_upd' => array(
                'title' => $this->l('Status changed'),
                'align' => 'right',
                'width' => 'auto',
                'type' => 'datetime',
                'filter_key' => 'a!date_upd'
            )
        );
    }

    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();
        unset($this->toolbar_btn['new']);
        $this->page_header_toolbar_btn['configure'] = array(
            'href' => $this->context->link->getAdminLink('AdminModules').'&configure='.$this->module_name.'&conf=12',
            'desc' => $this->l('Configure module', null, null, false),
            'icon' => 'process-icon-modules-list'
        );
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        Media::addJsDef(array(
            'orders_list_ajax_url' => $this->context->link->getAdminLink(
                'AdminPdGoogleAnalytycEnhancedEcommercePro'
            ),
            'orders_list_controller_token' => Tools::getAdminTokenLite(
                'AdminPdGoogleAnalytycEnhancedEcommercePro'
            ),
        ));
    }

    public function initContent()
    {
        parent::initContent();
    }

    public function renderView()
    {
        return parent::renderView();
    }

    public function postProcess()
    {
        // Assign free delivery to products by params
        if (Tools::isSubmit('downloadGtmConfig')) {
            $this->getGTMConfigurationFile();
        }


        return parent::postProcess();
    }

    public function ajaxProcessSendOrder()
    {
        $id_order = (int)Tools::getValue('id_order');
        $id_order_status = (int)Tools::getValue('id_order_status'); 
        if ($id_order 
            && $id_order_status 
            && in_array($id_order_status, $this->module->os_statuses_send)
        ) {

            $obj = new $this->className($id_order);
            if (isset($obj->id_order) && !$obj->order_send) {
                $order = new Order($id_order);
                if (!(Validate::isLoadedObject($order))) {
                    return;
                }

                $order_value = $order->total_paid;
                $order_tax = $order->total_paid_tax_incl - $order->total_paid_tax_excl;
                $shipping_cost = $order->total_shipping_tax_incl;

                $id_lang = (int)$this->context->language->id;
                $id_shop = (int)$this->context->shop->id;
                $id_currency = $order->id_currency;
                $currency = new Currency($id_currency);
                $currency_iso = $currency->iso_code;
                $order_products = $order->getProducts();
                $cart_rules = $this->module->getCartRuleWithCoupon($order);
                
                foreach ($order_products as &$op) {
                    $category = new Category($op['id_category_default'], $id_lang, $id_shop);
                    $price_old = Product::getPriceStatic($op['id_product'], true, $op['product_attribute_id'], 6, null, false, false);
                    $op['category_name'] = $category->name;
                    $op['content_ids'] = $this->module->getProductIdStringByType($op);
                    $op['discount'] = 0;
                    $op['discount'] = Tools::ps_round($price_old - $op['unit_price_tax_incl'], 2);
                    $op['price'] = Tools::ps_round($op['unit_price_tax_incl'], 2);
                    $op['manufacturer'] = Manufacturer::getNameById($op['id_manufacturer']) ? Manufacturer::getNameById($op['id_manufacturer']) : '';
                    $product = new Product($op['id_product'], false, $id_lang);
                    $attribute_combination_resume = $product->getAttributeCombinationsById($op['product_attribute_id'], $id_lang, true);
                    if ($attribute_combination_resume) {
                        $op['variant'] = '';
                        foreach ($attribute_combination_resume as  $acr) {
                            $op['variant'] .=  $acr['group_name'].': '.$acr['attribute_name'].' - ';
                        }
                        $op['variant'] = mb_substr($op['variant'], 0, -3);
                    }

                    if (!empty($op['variant'])) {
                        $op['name'] = addslashes($product->name.' ('.$op['variant'].')');
                    } else {
                        $op['name'] = addslashes($product->name);
                    }
                }

                $this->context->smarty->assign(array(
                    'content_transaction_id' =>  $order->reference,
                    'content_shipping' => Tools::ps_round($shipping_cost, 2),
                    'content_value' => Tools::ps_round($order_value, 2),
                    'content_tax' => Tools::ps_round($order_tax, 2),
                    'content_coupon' => sizeof($cart_rules) ? $cart_rules['name'].' - '.$cart_rules['code'] : '',
                    'content_products' => $order_products,
                    'http_referer' => $this->module->http_referer,
                    'currency' => $currency_iso,
                ));

                $html = $this->context->smarty->fetch(
                    _PS_MODULE_DIR_.$this->module_name.'/views/templates/hook/sendOrder.tpl'
                );

                $obj->order_send = 1;
                $obj->date_upd = date('Y-m-d H:i:s');
                $obj->update();
                die(Tools::jsonEncode($html));
            }
        }
    }

    public function ajaxProcessRefundOrder()
    {
        $id_order = (int)Tools::getValue('id_order');
        $id_order_status = (int)Tools::getValue('id_order_status'); 
        if ($id_order 
            && $id_order_status 
            && in_array($id_order_status, $this->module->os_statuses_refund)
        ) {
            $obj = new $this->className($id_order);
            if (isset($obj->id_order) && !$obj->refund_send) {
                $order = new Order($id_order);
                if (!(Validate::isLoadedObject($order))) {
                    return;
                }
                $this->context->smarty->assign(array(
                    'content_transaction_id' => $order->reference
                ));
                $html = $this->context->smarty->fetch(
                    _PS_MODULE_DIR_.$this->module_name.'/views/templates/hook/refundOrder.tpl'
                );

                $obj->refund_send = 1;
                $obj->date_upd = date('Y-m-d H:i:s');
                $obj->update();
                die(Tools::jsonEncode($html));
            }
        }
    }

    public function getGTMConfigurationFile() 
    {
        $local_file = dirname(__FILE__).'/../../views/json/export.json';
        $data = json_decode(file_get_contents($local_file), false);

        if (!empty($this->module->pd_google_tag_menager_id)) {
            $data->containerVersion->container->publicId = $this->module->pd_google_tag_menager_id;
        }

        if (!empty($this->module->pd_google_analitycs_id)) {
            $data->containerVersion->variable[0]->parameter[9]->value = $this->module->pd_google_analitycs_id;
        }

        $json = json_encode($data);
        header('Content-disposition: attachment; filename='.$this->module->pd_google_tag_menager_id.'.json');
        header('Content-type: application/json');
        die($json);
    }
}
