<?php

if (!defined('_PS_VERSION_')) {
    exit;
}


function upgrade_module_1_3_0($object)
{
    $result = Db::getInstance()->insert(
        'furgonetka_machine_type',
        array(array('id_machine_type' => 7, 'machine' => 'fedex')),
        false,
        true,
        Db::INSERT_IGNORE
    );

    return $result;
}