<?php
abstract class FurgonetkaSoapResult
{
    public $errors;

    abstract public function validate();

    public function validateThrow()
    {
        if(!$this->validate()){
            $error = $this->getErrors();
            throw new Exception($error);
        }

        return true;
    }

    public function getErrors($asArray = false)
    {
        $errors = array();
        $errorsString = '';
        if(!empty($this->errors)){
            foreach ($this->errors as $error) {
                $errors[] = isset($error->message)? $error->message : null;
            }
            $errors = array_filter($errors);
            $errorsString = implode('<br>', $errors);
        }

        return $asArray? $errors : $errorsString;
    }

    public function check($value, $default = null)
    {
        return isset($value)? $value : $default;
    }
}

class CreateIntegrationOAuthApplicationParams{
    public $integration_type;
    public $url;
    public $data_1;
    public $data_2;
    public $data_3;

    public function __construct($integration_type, $url, $data_1, $data_2, $data_3 = null)
    {
        $this->integration_type = $integration_type;
        $this->url = $url;
        $this->data_1 = $data_1;
        $this->data_2 = $data_2;
        $this->data_3 = $data_3;
    }
}

class CreateIntegrationOAuthApplicationResult extends FurgonetkaSoapResult
{
    public $client_id;
    public $client_secret;

    public function __construct($objectResponse)
    {
        $this->client_id = $this->check($objectResponse->createIntegrationOAuthApplicationResult->client_id);
        $this->client_secret = $this->check($objectResponse->createIntegrationOAuthApplicationResult->client_secret);
        $this->errors = $this->check($objectResponse->createIntegrationOAuthApplicationResult->errors);
    }

    public function validate()
    {
        return !empty($this->client_id)
            && !empty($this->client_secret)
            && empty($this->errors);
    }
}

class FurgonetkaSoapAuth{
    public $access_token;

    public function __construct($access_token)
    {
        $this->access_token = $access_token;
    }
}

class FurgonetkaSoapSender
{
    public $name;
    public $email;
    public $company;
    public $street;
    public $postcode;
    public $city;
    public $country_code;
    public $county;
    public $phone;
    public $point;

    public function __construct(
        $street,
        $postcode,
        $city,
        $name = null,
        $email = null,
        $company = null,
        $country_code = null,
        $county = null,
        $phone = null,
        $point = null)
    {
        $this->name = $name;
        $this->email = $email;
        $this->company = $company;
        $this->street = $street;
        $this->postcode = $postcode;
        $this->city = $city;
        $this->country_code = $country_code;
        $this->county = $county;
        $this->phone = $phone;
        $this->point = $point;
    }
}

class FurgonetkaSoapReceiver
{
    public $name;
    public $email;
    public $company;
    public $street;
    public $postcode;
    public $city;
    public $country_code;
    public $county;
    public $phone;
    public $point;
    public $alternative_point;
    public $type_point;

    public function __construct(
        $street,
        $postcode,
        $city,
        $name = null,
        $email = null,
        $company = null,
        $country_code = null,
        $county = null,
        $phone = null,
        $point = null,
        $alternative_point = null,
        $type_point = null
    )
    {
        $this->name = $name;
        $this->email = $email;
        $this->company = $company;
        $this->street = $street;
        $this->postcode = $postcode;
        $this->city = $city;
        $this->country_code = $country_code;
        $this->county = $county;
        $this->phone = $phone;
        $this->point = $point;
        $this->alternative_point = $alternative_point;
        $this->type_point = $type_point;
    }


}

class FurgonetkaSoapParcel
{
    public $weight;
    public $width;
    public $height;
    public $depth;
    public $value;
    public $gauge;
    public $description;
    public $customer_data;

    public function __construct(
        $weight = null,
        $width = null,
        $height = null,
        $depth = null,
        $value = null,
        $gauge = null,
        $description = null,
        $customer_data = null)
    {
        $this->weight = $weight;
        $this->width = $width;
        $this->height = $height;
        $this->depth = $depth;
        $this->value = $value;
        $this->gauge = $gauge;
        $this->description = $description;
        $this->customer_data = $customer_data;
    }
}

class FurgonetkaSoapServices
{
    public $cod;
    public $rod;
    public $guarantee_0930;
    public $guarantee_1200;
    public $saturday_delivery;
    public $additional_handling;
    public $ups_saver;
    public $sending_at_point;
    public $private_shipping;
    public $sms_predelivery_information;
    public $insurance;
    public $fragile;
    public $self_pickup;
    public $fedex_priority;
    public $xpress_plus;

    public function __construct($cod = null, $rod = null, $guarantee_0930 = null, $guarantee_1200 = null, $saturday_delivery = null, $additional_handling = null, $ups_saver = null, $sending_at_point = null, $private_shipping = null, $sms_predelivery_information = null, $insurance = null, $fragile = null, $self_pickup = null, $fedex_priority = null, $xpress_plus = null)
    {
        $this->cod = $cod;
        $this->rod = $rod;
        $this->guarantee_0930 = $guarantee_0930;
        $this->guarantee_1200 = $guarantee_1200;
        $this->saturday_delivery = $saturday_delivery;
        $this->additional_handling = $additional_handling;
        $this->ups_saver = $ups_saver;
        $this->sending_at_point = $sending_at_point;
        $this->private_shipping = $private_shipping;
        $this->sms_predelivery_information = $sms_predelivery_information;
        $this->insurance = $insurance;
        $this->fragile = $fragile;
        $this->self_pickup = $self_pickup;
        $this->fedex_priority = $fedex_priority;
        $this->xpress_plus = $xpress_plus;
    }
}

class FurgonetkaSoapPickupDate
{
    public $date;
    public $min_time;
    public $max_time;

    public function __construct($date, $min_time, $max_time)
    {
        $this->date = $date;
        $this->min_time = $min_time;
        $this->max_time = $max_time;
    }
}

class GetPackageFormUrlParams
{
    public $auth;
    public $sender;
    public $receiver;
    public $service;
    public $type;
    public $parcels;
    public $services;
    public $pickup_date;
    public $partner_reference_number;
    public $user_reference_number;
    public $sale_source_id;


    public function __construct(
        FurgonetkaSoapAuth $auth,
        FurgonetkaSoapSender $sender,
        FurgonetkaSoapReceiver $receiver,
        $partner_reference_number,
        $service = null,
        $sale_source_id = null,
        $type = null,
        $parcels = null,
        FurgonetkaSoapServices $services = null,
        FurgonetkaSoapPickupDate $pickup_date = null,
        $user_reference_number = null
        )
    {
        $this->auth = $auth;
        $this->sender = $sender;
        $this->receiver = $receiver;
        $this->service = $service;
        $this->type = $type;
        $this->parcels = $parcels;
        $this->services = $services;
        $this->pickup_date = $pickup_date;
        $this->partner_reference_number = $partner_reference_number;
        $this->user_reference_number = $user_reference_number;
        $this->sale_source_id = $sale_source_id;
    }

}

class GetPackageFormUrlResult extends FurgonetkaSoapResult
{
    public $url;

    public function __construct($objectResponse)
    {
        $this->url = $this->check($objectResponse->getPackageFormUrlResult->url);
        $this->errors = $this->check($objectResponse->getPackageFormUrlResult->errors);
    }

    public function validate()
    {
        return !empty($this->url)
            && empty($this->errors);
    }
}

class AddIntegrationSourceParams
{
    public $auth;
    public $integration_type;
    public $module_version;
    public $data_1;
    public $data_2;
    public $data_3;
    public $data_4;
    public $data_5;
    public $data_6;

    /**
     * AddIntegrationSourceParams constructor.
     * @param $auth
     * @param $integration_type
     * @param $module_version
     * @param $data_1
     * @param $data_2
     * @param $data_3
     * @param $data_4
     * @param $data_5
     * @param $data_6
     */
    public function __construct(
        $auth,
        $integration_type,
        $module_version,
        $data_1,
        $data_2,
        $data_3 = null,
        $data_4 = null,
        $data_5 = null,
        $data_6 = null
    )
    {
        $this->auth = $auth;
        $this->integration_type = $integration_type;
        $this->module_version = $module_version;
        $this->data_1 = $data_1;
        $this->data_2 = $data_2;
        $this->data_3 = $data_3;
        $this->data_4 = $data_4;
        $this->data_5 = $data_5;
        $this->data_6 = $data_6;
    }
}

class AddIntegrationSourceResult extends FurgonetkaSoapResult
{
    public $source_id;

    public function __construct($objectResponse)
    {
        $this->source_id = $this->check($objectResponse->addIntegrationSourceResult->source_id);
        $this->errors = $this->check($objectResponse->addIntegrationSourceResult->errors);
    }

    public function validate()
    {
        return !empty($this->source_id)
            && empty($this->errors);
    }
}
