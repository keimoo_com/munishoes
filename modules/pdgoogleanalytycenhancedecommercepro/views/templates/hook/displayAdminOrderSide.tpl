{*
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*}

<div class="card mt-2 d-print-none">
  	<div class="card-header">
	    <div class="row">
	    	<div class="col-md-12">
		        <h3 class="card-header-title">
		        	{l s='Google Analitycs' mod='pdgoogleanalytycenhancedecommercepro'}
		        </h3>
	    	</div>
	    </div>
	</div>
	<div class="card-body">
		<p class="mb-1">
        	<strong>{l s='Order send' mod='pdgoogleanalytycenhancedecommercepro'}</strong>
            <span>
				{if isset($data->order_send) && $data->order_send}
					{l s='Yes' mod='pdgoogleanalytycenhancedecommercepro'}
				{else}
					{l s='No' mod='pdgoogleanalytycenhancedecommercepro'}
				{/if}
			</span>
        </p>
        <p class="mb-1">
        	<strong>{l s='Order refunded' mod='pdgoogleanalytycenhancedecommercepro'}</strong>
            <span>
				{if isset($data->refund_send) && $data->refund_send}
					{l s='Yes' mod='pdgoogleanalytycenhancedecommercepro'}
				{else}
					{l s='No' mod='pdgoogleanalytycenhancedecommercepro'}
				{/if}
			</span>
        </p>
		<p class="mb-1">
	       	<strong>{l s='Date last change' mod='pdgoogleanalytycenhancedecommercepro'}</strong> 
	        {if isset($data->date_upd)} <span>{$data->date_upd}</span>{/if}
	   	</p>
	   	<input type="hidden" id="pdgoogleanalytycenhancedecommercepro_id_order" value="{$id_order}"> 
	   	<div style="display:hidden;" id="pdgoogleanalytycenhancedecommercepro_ajax_response"></div>
	</div>
</div>
