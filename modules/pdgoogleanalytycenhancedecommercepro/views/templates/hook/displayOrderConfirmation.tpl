{*
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*}

<!-- START > PD Google Analytycs Enhanced Ecommerce trought Tag Manager > DataLayer 1.7.x Module > checkout -->
<script>
    console.log('Data layer push GA event: checkout > Order confirmation');
    dataLayer.push({ ecommerce: null });  // Clear the previous ecommerce object.
    dataLayer.push({
        'event': 'checkout',
        'ecommerce': {
            'checkout': {
                'actionField': {
                    'step': 7, 
                    'option': 'Order confirmation'
                }
            }
        }
    });
</script>
<!-- END > PD Google Analytycs Enhanced Ecommerce trought Tag Manager > DataLayer 1.7.x Module > checkout -->
