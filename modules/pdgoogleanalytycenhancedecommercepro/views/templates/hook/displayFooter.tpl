{*
* 2012-2021 Patryk Marek PrestaDev.pl
*
* Patryk Marek PrestaDev.pl - PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at info@prestadev.pl.
*
* @author    Patryk Marek <info@prestadev.pl>
* @copyright 2012-2021 Patryk Marek @ PrestaDev.pl
* @license   Do not edit, modify or copy this file, if you wish to customize it, contact us at info@prestadev.pl.
* @link      http://prestadev.pl
* @package   PD Google Analitycs Enhanced Ecommerce Pro 1.7.x Module
* @version   1.0.1
* @date      20-08-2021
*}

{if $tagType != 'none'}
<!-- START > PD Google Analytycs Enhanced Ecommerce trought Tag Manager > DataLayer 1.7.x Module > FOOTER-->

<script type="text/javascript" >

	{if ($tagType === 'product')}
	
		console.log('Data layer push GA event: productDetailImpression > Product page');
		dataLayer.push({ ecommerce: null });
		dataLayer.push({
		  	'event': 'productDetailImpression',
		  	'ecommerce': {
		    	'currencyCode': '{$currency nofilter}',
		    	'detail': {
		    		'actionField': {
                        'list': 'product-page'
                    },
				    'products': [{
						'id': '{$content_ids nofilter}',
						'name': '{$content_name nofilter}',
						'price': {$content_value nofilter},
						'brand': '{$content_manufacturer nofilter}',
						'category': '{$content_category nofilter}',
						'variant': '{$content_variant nofilter}'
					}]
				}
		  	}
		});

		{if $pd_gaeep_gdr_enabled}
		console.log('Data layer push GDR event: view_item > Product page');
		dataLayer.push({
			'event': 'view_item',
			'value': {$content_value nofilter},
			'items': [{
				'id': '{$content_ids nofilter}',
				'google_business_vertical': 'retail'
			}]
		});
		{/if}


	{else if ($tagType === 'cart')}
		
		console.log('Data layer push GA event: checkout > Cart page');
		dataLayer.push({ ecommerce: null });
		dataLayer.push({
		  	'event': 'checkout',
		  	'ecommerce': {
		    	'checkout': {
				    'actionField': {
				    	'step': 1, 
				    	'option': 'Cart summary > Initiate checkout'
				    },
				    'products': [
					    {foreach from=$content_products item=product name=products} {
							'id': '{$product.content_ids nofilter}',
							'name': '{$product.name nofilter}',
							'price': {$product.price nofilter},
							'brand': '{$product.manufacturer nofilter}',
							'category': '{$product.category nofilter}',
							'variant': '{if isset($product.variant)}{$product.variant}{/if}',
							'quantity': '{$product.cart_quantity nofilter}'
						},
						{/foreach}
					]
				}
		  	}
		});

	{else if ($tagType === 'order')}
		
		console.log('Data layer push GA event: checkout > Order page');
		dataLayer.push({ ecommerce: null });
		dataLayer.push({
		  	'event': 'checkout',
		  	'ecommerce': {
		    	'checkout': {
				    'actionField': {
				    	'step': 2, 
				    	'option': 'Initiate checkout'
				    },
				    'products': [
					    {foreach from=$content_products item=product name=products} {
							'id': '{$product.content_ids nofilter}',
							'name': '{$product.name nofilter}',
							'price': {$product.price nofilter},
							'brand': '{$product.manufacturer nofilter}',
							'category': '{$product.category nofilter}',
							'variant': '{if isset($product.variant)}{$product.variant}{/if}',
							'quantity': '{$product.cart_quantity nofilter}'
						},
						{/foreach}
					]
				}
		  	}
		});

	{else if ($tagType === 'category')}

		console.log('Data layer push GA vent: productImpressions > Category products page');
		dataLayer.push({ ecommerce: null });
		dataLayer.push({
		  	'event': 'productImpressions',
		  	'ecommerce': {
		    	'currencyCode': '{$currency nofilter}',
		    	'impressions': [
			    	{foreach from=$content_products item=product name=products}{
						'id': '{$product.content_ids nofilter}',
						'name': '{$product.name nofilter}',
						'price': {$product.price_amount nofilter},
						'position': {$smarty.foreach.products.index},
						'list': '{$content_name nofilter}',
						'brand': '{$product.manufacturer nofilter}',
						'category': '{$product.category_name nofilter}',
						'variant': '{if isset($product.variant)}{$product.variant}{/if}'
					},
					{/foreach}
				]
		  	}
		});

		{if $pd_gaeep_gdr_enabled}
		console.log('Data layer push GDR event: view_item_list > Category products page');
		dataLayer.push({
			'event': 'view_item_list',
			'value': {$total_value nofilter},
			'items': [
				{foreach from=$content_products item=product name=products}
				{
					'id': '{$product.content_ids nofilter}',
					'google_business_vertical': 'retail',
				},
				{/foreach}
			]
		});
		{/if}

	
	{else if ($tagType === 'prices-drop')}

		console.log('Data layer push GA event: productImpressions > Prices drop page');
		dataLayer.push({ ecommerce: null });
		dataLayer.push({
		  	'event': 'productImpressions',
		  	'ecommerce': {
		    	'currencyCode': '{$currency nofilter}',
		    	'impressions': [
			    	{foreach from=$content_products item=product name=products}{
						'id': '{$product.content_ids nofilter}',
						'name': '{$product.name nofilter}',
						'price': {$product.price_amount nofilter},
						'position': {$smarty.foreach.products.index},
						'list': '{$content_name nofilter}',
						'brand': '{$product.manufacturer nofilter}',
						'category': '{$product.category_name nofilter}',
						'variant': '{if isset($product.variant)}{$product.variant}{/if}'
					},
					{/foreach}
				]
		  	}
		});

		console.log('Data layer push GA event: promoView > Prices drop page');
		dataLayer.push({ ecommerce: null });
		dataLayer.push({
			'event': 'promoView',
		  	'ecommerce': {
		    	'promoView': {
			      'promotions': [
			       	{foreach from=$content_products item=product name=products}{
			         	'id': '{$product.content_ids nofilter}',
			         	'name': '{$product.name nofilter}'
			       	},
					{/foreach}
			       ]
		   	 	}
		  	}
		});


		{if $pd_gaeep_gdr_enabled}
		console.log('Data layer push GDR event: view_item_list > Prices drop page');
		dataLayer.push({
			'event': 'view_item_list',
			'value': {$total_value nofilter},
			'items': [
				{foreach from=$content_products item=product name=products}
				{
					'id': '{$product.content_ids nofilter}',
					'google_business_vertical': 'retail',
				},
				{/foreach}
			]
		});
		{/if}

	{else if ($tagType === 'best-sales')}

		console.log('Data layer push GA event: productImpressions > Best sales page');
		dataLayer.push({ ecommerce: null });
		dataLayer.push({
		  	'event': 'productImpressions',
		  	'ecommerce': {
		    	'currencyCode': '{$currency nofilter}',
		    	'impressions': [
			    	{foreach from=$content_products item=product name=products}{
						'id': '{$product.content_ids nofilter}',
						'name': '{$product.name nofilter}',
						'price': {$product.price_amount nofilter},
						'position': {$smarty.foreach.products.index},
						'list': '{$content_name nofilter}',
						'brand': '{$product.manufacturer nofilter}',
						'category': '{$product.category_name nofilter}',
						'variant': '{if isset($product.variant)}{$product.variant}{/if}'
					},
					{/foreach}
				]
		  	}
		});

		{if $pd_gaeep_gdr_enabled}
		console.log('Data layer push GDR event: view_item_list > Best sales page');
		dataLayer.push({
			'event': 'view_item_list',
			'value': {$total_value nofilter},
			'items': [
				{foreach from=$content_products item=product name=products}
				{
					'id': '{$product.content_ids nofilter}',
					'google_business_vertical': 'retail',
				},
				{/foreach}
			]
		});
		{/if}

	{else if ($tagType === 'new-products')}

		console.log('Data layer push GA event: productImpressions > New products page');
		dataLayer.push({ ecommerce: null });
		dataLayer.push({
		  	'event': 'productImpressions',
		  	'ecommerce': {
		    	'currencyCode': '{$currency nofilter}',
		    	'impressions': [
			    	{foreach from=$content_products item=product name=products}{
						'id': '{$product.content_ids nofilter}',
						'name': '{$product.name nofilter}',
						'price': {$product.price_amount nofilter},
						'position': {$smarty.foreach.products.index},
						'list': '{$content_name nofilter}',
						'brand': '{$product.manufacturer nofilter}',
						'category': '{$product.category_name nofilter}',
						'variant': '{if isset($product.variant)}{$product.variant}{/if}'
					},
					{/foreach}
				]
		  	}
		});

		{if $pd_gaeep_gdr_enabled}
		console.log('Data layer push GDR event: view_item_list > New products page');
		dataLayer.push({
			'event': 'view_item_list',
			'value': {$total_value nofilter},
			'items': [
				{foreach from=$content_products item=product name=products}
				{
					'id': '{$product.content_ids nofilter}',
					'google_business_vertical': 'retail',
				},
				{/foreach}
			]
		});
		{/if}

	{else if ($tagType === 'search')}

		{if $pd_gaeep_gdr_enabled}
		console.log('Data layer push GDR event: view_search_results');
		dataLayer.push({
			'event': 'view_search_results', 
			'value': '{$total_value nofilter}',
			'items': [
				{foreach from=$content_products item=product name=products}
				{
					'id': '{$product.content_ids nofilter}',
					'google_business_vertical': 'retail',
				},
				{/foreach}
			]
		});
		{/if}

	{/if}

</script>
<!-- END > PD Google Analytycs Enhanced Ecommerce trought Tag Manager > DataLayer 1.7.x Module > FOOTER -->
{/if}