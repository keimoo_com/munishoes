<?php
class PrestaApi
{


    public static function getResources()
    {
        return [
            'addresses' =>
                [
                    'GET' => 'on',
                ],
            'carriers' =>
                [
                    'GET' => 'on',
                ],
            'combinations' =>
                [
                    'GET' => 'on',
                ],
            'countries' =>
                [
                    'GET' => 'on',
                ],
            'customer_messages' =>
                [
                    'GET' => 'on',
                ],
            'customer_threads' =>
                [
                    'GET' => 'on',
                ],
            'customers' =>
                [
                    'GET' => 'on',
                ],
            'order_payments' =>
                [
                    'GET' => 'on',
                ],
            'order_states' =>
                [
                    'GET' => 'on',
                ],
            'orders' =>
                [
                    'GET' => 'on',
                    'PUT' => 'on',
                ],
            'products' =>
                [
                    'GET' => 'on',

                ],
            'states' =>
                [
                    'GET' => 'on',
                ],
            'taxes' =>
                [
                    'GET' => 'on',
                ],
        ];
    }

    public function addWebservice()
    {
        $idWebservice = Configuration::get('FURGONETKA_ID_WEBSERVICE');
        $webservice = new WebserviceKey($idWebservice);
        $i = 0;
        if(!$webservice->id){
            do{
                $key = md5('auth'.time().$i++);

            }while(WebserviceKey::keyExists($key));

            $webservice->key = $key;
            $webservice->description = 'furgonetka-auth';
            $webservice->active = '1';
            if (!$webservice->save()) {
                throw new Exception('Error add webservice prestashop');
            }
//            Db::getInstance()->delete('webservice_account_shop', 'id_webservice_account = '.(int)$webservice->id);

            if (!WebserviceKey::setPermissionForAccount($webservice->id, self::getResources())) {
                throw new Exception('Error add webservice prestashop permission');
            }

            Configuration::updateGlobalValue('FURGONETKA_ID_WEBSERVICE', $webservice->id);
        }
        $this->assignWebserviceToAllShops($webservice->id);

    }

    private function assignWebserviceToAllShops($idWebservice)
    {
        $values = array();
        foreach (Shop::getShops(false, null, true) as $shop) {
            $values[]  = array('id_webservice_account' => (int)$idWebservice, 'id_shop' => (int)$shop);
        }
        if (!Db::getInstance()->insert('webservice_account_shop', $values, false, true, DB::INSERT_IGNORE)) {
            throw new Exception('Error add webservice prestashop');
        }
    }

    public function getWebServiceToken()
    {
        $idWebservice = Configuration::get('FURGONETKA_ID_WEBSERVICE');
        $webservice = new WebserviceKey($idWebservice);
        return $webservice->key;
    }

    public function getShopUrl()
    {
        $this->addWebservice();
//        return  Context::getContext()->shop->getBaseURL(true);

        $idShop = Configuration::get('PS_SHOP_DEFAULT');
        $shop = new Shop($idShop);
        if($shop->id){
            return $shop->getBaseURL(true);
        }else{
//            $shops = Shop::getShops(true, null, true);
//            $idShop = current($shops);
//            $shop = new Shop((int)$idShop);
//            if($shop->id) {
//                return $shop->getBaseURL(true);
//            }
            throw new Exception('No default shop');
        }
    }


}
