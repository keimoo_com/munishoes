$(document).on('click', '#furgonetka-set-point', function(e){
    e.preventDefault();
    showMap()
});

$(document).on('change', '.delivery_option_radio', function(){
    buttonManage()
});

$(document).on('click', 'button[name = "processCarrier"]', function(e){
    e.preventDefault();
    var id_delivery = parseInt($('.delivery_option_radio input:checked').val());
    if (!$('#furgonetka-machine-' + id_delivery).length){
        $.ajax({
            type: 'POST',
            url: furgonetkaCheckMapAjax,
            async: true,
            dataType: 'json',
            data: {id_delivery : id_delivery},
            success: function(jsonData) {
                if(jsonData.status == 'error'){
                    alert(jsonData.message)
                }else{
                    $('#form').submit();
                }
            },
        });
    }else{
        $('#form').submit();
    }


});

$(document).on('click', '#HOOK_PAYMENT', function(e){
    if($(e.target).hasClass('clicked')){
        $(e.target).removeClass('clicked');
    }else{
        e.preventDefault();
        var id_delivery = parseInt($('.delivery_option_radio input:checked').val());
        if (!$('#furgonetka-machine-' + id_delivery).length){
            $.ajax({
                type: 'POST',
                url: furgonetkaCheckMapAjax,
                async: true,
                dataType: 'json',
                data: {id_delivery : id_delivery},
                success: function(jsonData) {
                    if(jsonData.status == 'error'){
                        $(e.target).data('clicked', false); // PayU module fix.
                        alert(jsonData.message);
                    }else{
                        $(e.target).addClass('clicked');
                        $(e.target).data('clicked', false); // PayU module fix.
                        $(e.target)[0].click();
                    }
                },
            });
        }else{
            $(e.target).addClass('clicked');
            $(e.target).data('clicked', false); // PayU module fix.
            $(e.target)[0].click();
        }
    }
});


function buttonManage(){
    var id_delivery = parseInt($('.delivery_option_radio input:checked').val());
    var button = $('#furgonetka-set-point');
    if (!$('#furgonetka-delivery-' + id_delivery).length) {
        button.hide();
    }else{
        button.show();
    }
    showMachine();
}

function showMap(){
    var id_delivery = parseInt($('.delivery_option_radio input:checked').val());
    var service = $('#furgonetka-delivery-' + id_delivery).val().split(',');
    var city = $('#furgonetka-city').val();
    var street = $('#furgonetka-street').val();
    if(typeof FurgonetkaBiznesMap === 'function'){
        new FurgonetkaBiznesMap({service: service,city: city, street: street, 'callback': furgonetkaMapCallback}).view();
    }
}

function showMachine(){
    var id_delivery = parseInt($('.delivery_option_radio input:checked').val());
    $('.furgonetka-machine').addClass('hidden');
    var machine_name = $('#furgonetka-machine-' + id_delivery);
    if (machine_name.length) {
        machine_name.removeClass('hidden');
    }

}

function furgonetkaMapCallback(response){
    var id_delivery = parseInt($('.delivery_option_radio input:checked').val());
    $.ajax({
        type: 'POST',
        url: furgonetkaMapAjax,
        async: true,
        dataType: 'json',
        data: {id_delivery : id_delivery, machine_code: response.code, machine_name : response.name, machine_type: response.type},
        success: function(jsonData) {
            if(jsonData.status == 'error'){
                jAlert(jsonData.message)
            }else{
                var input = '<input disabled class="furgonetka-machine" id="furgonetka-machine-' + id_delivery +'" value="'+ response.name +'">'
                $('#furgonetka-machine').html(input);
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
        }
    });
}
