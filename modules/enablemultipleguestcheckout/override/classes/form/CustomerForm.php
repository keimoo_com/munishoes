<?php
use Symfony\Component\Translation\TranslatorInterface;
class CustomerForm extends CustomerFormCore
{
    public function validate()
    {
        parent::validate(); 
        $emailField = $this->getField('email');
        $id_customer = Customer::customerExists($emailField->getValue(), true, true);
        $customer = $this->getCustomer();
        $passwordField = $this->getField('password');
        $passwordFieldValue  = $passwordField->getValue();
        
        /*
        * @ Levi - PrestaChamps
        * @ Disable existing account checking (enable multiple guest order with registered email), if i come from the order controller
        * @ 2018-10-01  
        */
        if(Tools::getValue('controller') != 'order'){
            if ($id_customer && $id_customer != $customer->id) {
                $errors = $emailField->getErrors();
                if( sizeof($errors)<1){
                    $emailField->addError($this->translator->trans(
                        'The email "%mail%" is already used, please choose another one or sign in', array('%mail%' => $emailField->getValue()), 'Shop.Notifications.Error'
                    ));
                }
            }
        } else {
            $errors = $emailField->getErrors();
            if (strpos($errors[0], ' is already used, please choose another one or sign in') !== false) {
                $emailField->setErrors([]);
            }
            if( !(empty($passwordFieldValue)) || $passwordFieldValue != ''){
                if ($id_customer && $id_customer != $customer->id) {
                    $passwordField->addError($this->translator->trans(
                        'The email "%mail%" is already used. If you want to order as guest, please leave the password field empty', array('%mail%' => $emailField->getValue()), 'Shop.Notifications.Error'
                    ));
                }
            }
        }

        $birthdayField = $this->getField('birthday');
        if (!empty($birthdayField)) {
            $birthdayValue = $birthdayField->getValue();
            if (!empty($birthdayValue)) {
                $dateBuilt = DateTime::createFromFormat(Context::getContext()->language->date_format_lite, $birthdayValue);
                if (!empty($dateBuilt)) {
                    $birthdayField->setValue($dateBuilt->format('Y-m-d'));
                }
            }
        }
        $this->validateFieldsLengths();
        $this->validateByModules();
        // return parent::validate();
        return !$this->hasErrors();
    }
     
    /*
    * module: enablemultipleguestcheckout
    * date: 2018-10-01 04:47:18
    * version: 1.0.0
    */
    private function validateByModules()
    {
        $formFieldsAssociated = array();
        foreach ($this->formFields as $formField) {
            if (!empty($formField->moduleName)) {
                $formFieldsAssociated[$formField->moduleName][] = $formField;
            }
        }
        foreach ($formFieldsAssociated as $moduleName => $formFields) {
            if ($moduleId = Module::getModuleIdByName($moduleName)) {
                $validatedCustomerFormFields = Hook::exec('validateCustomerFormFields', array('fields' => $formFields), $moduleId, true);
                if (is_array($validatedCustomerFormFields)) {
                    array_merge($this->formFields, $validatedCustomerFormFields);
                }
            }
        }
    }
}
