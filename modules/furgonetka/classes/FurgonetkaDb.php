<?php
class FurgonetkaDb
{
    public static function addAdminController($className, $module, $position = 1)
    {
        if(Tab::getIdFromClassName($className) === false){
            $sql = "INSERT INTO ". _DB_PREFIX_ ."tab
             (id_tab, id_parent, class_name, module, position, active, hide_host_mode)
              VALUES (NULL, '-1', '".pSQL($className)."', '".pSQL($module)."' , '".$position."', '1', '0')";
            return Db::getInstance()->execute($sql);
        }

        return true;

    }

    public static function createTable($table, $columns, $primary, $engine, $charset = 'utf8', $collate = 'utf8_general_ci')
    {
        $sql = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.$table.'(';
        foreach ($columns as $column => $values){
            $sql .= $column.' '.$values.' ,';
        }
        $sql .= 'PRIMARY KEY ('.implode(',',$primary ).'))';
        $sql .= 'ENGINE = '.pSQL($engine);
        $sql .= ' CHARSET = '.pSQL($charset);
        $sql .= ' COLLATE = '.pSQL($collate);

        return  DB::getInstance()->execute($sql);
    }

    public static function getCartDelivery($idCart)
    {
        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('furgonetka_cart_delivery_machine', 'fcdm');
        $sql->where('id_cart = '.(int)$idCart);

        return Db::getInstance()->getRow($sql);
    }

    public static function getMachineCodeFromOrder($idOrder)
    {
        $sql = new DbQuery();
        $sql->select('fcdm.machine_code, fcdm.machine_name, fcdm.machine_type');
        $sql->from('furgonetka_cart_delivery_machine', 'fcdm');
        $sql->innerJoin(
            'orders',
            'o',
            'o.id_cart = fcdm.id_cart AND o.id_carrier = fcdm.id_delivery AND o.id_order = '.(int)$idOrder);

        return Db::getInstance()->getRow($sql);
    }

    public static function mapTypeMachine($machine)
    {
        $mapped = array(
            'paczkomat' => 'inpost',
            'kiosk' => 'ruch',
            'placowka' => 'poczta',
            'uap' => 'ups',
            'dpd' => 'dpd',
            'parcelshop' => 'dhl',
            'fedex' => 'fedex',
        );

        return isset($mapped[$machine])? $mapped[$machine] : $machine;
    }

    public static function deliveryHasMachine($id_reference)
    {
        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('furgonetka_carrier_machine_type', 'fmt');
        $sql->where('fmt.carrier_reference = '.(int)$id_reference );
        $result = Db::getInstance()->getRow($sql);
        return !empty($result);
    }

    public static function isSetMachine($id_cart, $id_delivery)
    {
        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('furgonetka_cart_delivery_machine', 'fcdm');
        $sql->where('fcdm.id_cart = '.(int)$id_cart);
        $sql->where('fcdm.id_delivery = '.(int)$id_delivery);
        $result = Db::getInstance()->getRow($sql);
        return !empty($result);
    }

    public static function getTypes()
    {
        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('furgonetka_machine_type', 'fmt');
        $result = Db::getInstance()->executeS($sql);
        $array = array();
        foreach ($result as $row) {
            $array[$row['machine']] = $row['id_machine_type'];
        }
        return $array;
    }

    public static function clearCarrierMachine()
    {
        return Db::getInstance()->delete('furgonetka_carrier_machine_type');
    }

    public static function addCarriersType($id_type, array $carriers)
    {
        foreach ($carriers as $carrier) {
            $values[] = array('carrier_reference' => (int)$carrier, 'id_machine_type' => (int)$id_type);
        }
        return Db::getInstance()->insert('furgonetka_carrier_machine_type', $values);
    }

    public static function getTypeCarriers()
    {
        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('furgonetka_carrier_machine_type');
        $result = Db::getInstance()->executeS($sql);
        if(empty($result)){
            return array();
        }

        $array = array();
        foreach ($result as $row) {
            $array[$row['id_machine_type']][] = $row['carrier_reference'];
        }

        return $array;
    }

    public static function getCarrierTypes()
    {
        $sql = new DbQuery();
        $sql->select('fmt.machine, c.id_carrier');
        $sql->from('furgonetka_carrier_machine_type', 'fcmt');
        $sql->innerJoin('furgonetka_machine_type', 'fmt', 'fcmt.id_machine_type = fmt.id_machine_type');
        $sql->innerJoin(
            'carrier',
            'c',
            'c.id_reference = fcmt.carrier_reference AND active = 1 AND deleted = 0');
        $result = Db::getInstance()->executeS($sql);
        if(empty($result)){
            return array();
        }

        $array = array();
        foreach ($result as $row) {
            $array[$row['id_carrier']][] = $row['machine'];
        }

        foreach ($array as $key => $value) {
            $array[$key] = implode(',', $value);
        }
        return $array;
    }

    public static function resetConnection($test)
    {
        $status = $test? 'TEST_' : '';

        Configuration::updateGlobalValue($status.'FURGONETKA_REFRESH_TOKEN_EXPIRE', '');
        Configuration::updateGlobalValue($status.'FURGONETKA_CLIENT_ID', '');
        Configuration::updateGlobalValue($status.'FURGONETKA_CLIENT_SECRET', '');
        Configuration::updateGlobalValue($status.'FURGONETKA_ACCESS_TOKEN', '');
        Configuration::updateGlobalValue($status.'FURGONETKA_REFRESH_TOKEN', '');
        Configuration::updateGlobalValue($status.'FURGONETKA_SOURCE_ID', '');
    }

}
