<?php

namespace PrestaShopBundle\Command;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use IqitProductReview;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShoploReviewImportCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Connection
     */
    private $conn;
    private $output;
    private $input;

    private $errorEmail = [];


    protected function configure()
    {
        // The name of the command (the part after "bin/console")
        $this
            ->setName('shoplo:import:review')
            ->setDescription('Shoplo review import');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initDb();

        include_once __DIR__ . '/../../../modules/iqitreviews/src/IqitProductReview.php';

        $this->output = $output;
        $this->input = $input;

        $outputStyle = new OutputFormatterStyle('blue', null);
        $this->output->getFormatter()->setStyle('blue', $outputStyle);

        $this->output->writeln(sprintf('<fg=green>Start import review!</>'));

        try {
            $handle = fopen(__DIR__ . "/../../../csv_import_file/review.csv", "r");
            $count = 0;

            while (($data = fgetcsv($handle, 3500)) !== FALSE) {
                if ($count === 0) {
                    $count++;
                    continue;
                }

                $this->runMigration($data);

                $count++;
            }

            $this->output->writeln('<danger>All Export done!</danger>');
            $this->output->writeln('<error>Not save</error>');
            print_r($this->errorEmail);

        } catch (\Exception $exception) {
            var_dump($exception->getMessage());
            die;
        }

        // Here your business logic.
        $this->output->writeln('<info>All Export done!</info>');
    }


    private function runMigration(array $customerRaw): void
    {
        $position = ['lp' => 1, 'id_produktu' => 2, 'nazwa' => 3, 'data_dodania' => 4, 'email_uzytkownika' => 5, 'imie_nazwisko' => 6, 'content' => 7];

        try {
            $productComment = new IqitProductReview();

            $productComment->id_product = $customerRaw[$position['id_produktu']];
            $productComment->title = $customerRaw[$position['nazwa']];
            $productComment->comment = $customerRaw[$position['content']];

            /*
             * @todo get id by email id
             */
            $productComment->id_customer = $this->getIdCustomerByEmail($customerRaw[$position['email_uzytkownika']]);

            $productComment->date_add = $customerRaw[$position['data_dodania']];
            $productComment->status = 1;
            $productComment->rating = 5;
            $productComment->save();

            $this->output->writeln(sprintf('<fg=blue>Review %s added.</>',$customerRaw[$position['email']] ));

        } catch (\Exception $exception) {
            $this->errorEmail[] = $customerRaw[$position['email']];
            var_dump($exception->getMessage());
            $this->output->writeln(sprintf('<error>Not save customer %s </error>',$customerRaw[$position['email']] ));
            return;
        }

    }

    private function getIdCustomerByEmail(string $email) {
        /**
         * @todo example ID
         */
        return 7078;
    }

    private function initDb(): void
    {
        $container = $this->getContainer();
        $this->em = $container->get('doctrine')->getManager();
        $this->conn = $this->em->getConnection();
        $this->conn->beginTransaction();
    }

}
