<?php
require_once __DIR__.'/../../classes/FurgonetkaDb.php';

class FurgonetkaMapModuleFrontController extends ModuleFrontController
{
    public function __construct()
    {
        parent::__construct();
        $this->display_column_left = false;
        $this->display_column_right = false;
    }

    public function displayAjaxSaveMachine()
    {
        $id_delivery = Tools::getValue('id_delivery');
        $machine_code = Tools::getValue('machine_code');
        $machine_name = Tools::getValue('machine_name');
        $machine_type = Tools::getValue('machine_type');

        $machine_type = FurgonetkaDb::mapTypeMachine($machine_type);

        $id_cart = Context::getContext()->cart->id;

        if(empty($id_cart)){
            $response = array('status' => 'error', 'message' => 'Error save machine');
            die(json_encode($response));
        }

        $data = array(
          'id_cart' => $id_cart,
          'id_delivery' => $id_delivery,
          'machine_code' => $machine_code,
          'machine_name' => $machine_name,
          'machine_type' => $machine_type,
        );

        $result = Db::getInstance()->insert(
            'furgonetka_cart_delivery_machine',
            $data,
            false,
            true,
            Db::ON_DUPLICATE_KEY
            );

        if($result){
            $response = array('status' => 'ok');
            die(json_encode($response));
        }
        $response = array('status' => 'error', 'message' => 'Error save machine');
        die(json_encode($response));

    }

    public function displayAjaxValidate()
    {
        $module = Module::getInstanceByName('furgonetka');
        $id_delivery = Tools::getValue('id_delivery');

        $carrier = new Carrier((int)$id_delivery);
        if (FurgonetkaDb::deliveryHasMachine($carrier->id_reference)) {
            die(json_encode(array('status' => 'error', 'message' => $module->getTranslate('Choose machine'))));
        }
        die(json_encode(array('status' => 'ok')));


    }

}
