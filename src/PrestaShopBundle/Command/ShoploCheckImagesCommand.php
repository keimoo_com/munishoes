<?php

namespace PrestaShopBundle\Command;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShoploCheckImagesCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Connection
     */
    private $conn;
    private $output;
    private $input;

    private $errorEmail = [];


    protected function configure()
    {
        // The name of the command (the part after "bin/console")
        $this
            ->setName('shoplo:check:images')
            ->setDescription('Shoplo client import');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initDb();

        $this->output = $output;
        $this->input = $input;

        $outputStyle = new OutputFormatterStyle('blue', null);
        $this->output->getFormatter()->setStyle('blue', $outputStyle);

        $this->output->writeln(sprintf('<fg=green>Start check images!</>'));

        try {
            $products = $this->getAllProduct();

            foreach ($products as $product)
            {
                $reference = $product['reference'];
                $this->output->writeln(sprintf('<fg=green>Start check product %s!</>', $reference));

                $id = substr($reference, 0, strpos($reference, '_'));

                $api = new ShoperClientApi();

                $count = $this->getCountImagesProduct($reference);
                $countShoperImage = $api->getImagesProductsCount($id);

                if ($count === $countShoperImage) {
                    continue;
                }

                $file = __DIR__ . "/../../../csv_import_file/image_check.txt";
                $error = sprintf('Product %s, shoper has: %d images, presta has: %d images'.PHP_EOL, $reference, $countShoperImage, $count);
                file_put_contents($file, $error, FILE_APPEND | LOCK_EX);
                $this->output->writeln(sprintf('<fg=red> >>  %s</>', $error));
            }


            print_r($this->errorEmail);

        } catch (\Exception $exception) {
            var_dump($exception->getMessage());
            die;
        }


        // Here your business logic.
        $this->output->writeln('<info>All Export done!</info>');
    }

    private function getCountImagesProduct(string $code): int
    {
        $stm = $this->conn->prepare(
            '
                SELECT count(i.id_product)
                FROM  ps_product p
                JOIN ps_image_shop i ON i.id_product = p.id_product
                WHERE p.reference = :CODE        
                GROUP BY i.id_product
            '
        );

        $stm->bindValue('CODE', $code);
        $stm->execute();

        $result = $stm->fetchColumn();

        if (empty($result)) {
            return false;
        }

        return $result;
    }

    private function getAllProduct(): array
    {
        $query = $this->conn->query(
            '
                SELECT  p.id_product, p.reference
                FROM `ps_product`  p            
            '
        );

        $result = $query->fetchAll();

        if (empty($result)) {
            return [];
        }

        return $result;
    }

    private function initDb(): void
    {
        $container = $this->getContainer();
        $this->em = $container->get('doctrine')->getManager();
        $this->conn = $this->em->getConnection();
        $this->conn->beginTransaction();
    }

}
